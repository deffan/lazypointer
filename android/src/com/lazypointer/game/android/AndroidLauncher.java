package com.lazypointer.game.android;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.lazypointer.game.GLOBAL;
import com.lazypointer.game.IActivityRequestHandler;
import com.lazypointer.game.Lazypointergame;

public class AndroidLauncher extends AndroidApplication implements IActivityRequestHandler {

	protected static InterstitialAd adView;
	private static final String AD_UNIT_ID = "ca-app-pub-6704917850991364/1206962098";
    private final static int SHOW_ADS = 1;
    private final static int HIDE_ADS = 0;

    public static IActivityRequestHandler ex;
    protected static Handler handler = new Handler()
    {
        @Override
        public void handleMessage(Message msg) {
            switch(msg.what) {
                case SHOW_ADS:
                {
                	if (adView.isLoaded())
                		adView.show();
                	else {
                		System.err.println("LAZYPOINTER: Ad was not loaded.");
                		requestNewInterstitial();
                	}
                    break;
                }
                case HIDE_ADS:
                {
                    break;
                }
            }
        }
    };
	
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        //Create the layout
        RelativeLayout layout = new RelativeLayout(this);

        // Do the stuff that initialize() would do for you
       requestWindowFeature(Window.FEATURE_NO_TITLE);
       getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
       getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);

        // Create the libgdx View
       View gameView = initializeForView(new Lazypointergame(false, this, GLOBAL.ANDROID_PLATFORM));

        // Add the libgdx view
        layout.addView(gameView);
		
	    // Create an ad.
        adView = new InterstitialAd(this);
        adView.setAdUnitId(AD_UNIT_ID);
        
        
        // TEST ads
        AdRequest adRequest = new AdRequest.Builder()
        .addTestDevice("FC70B048F852C960A977BB4EC36207EA")
        .build();
        adView.loadAd(adRequest);
        
        // Real ads
       // requestNewInterstitial();
        
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                // TEST ads
                AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("FC70B048F852C960A977BB4EC36207EA")
                .build();
                adView.loadAd(adRequest);
                // real ads
                //requestNewInterstitial();
            }
        });
        setContentView(layout);
	}
	
    private static void requestNewInterstitial() {
        AdRequest interstitialAdRequest = new AdRequest.Builder().build();
        adView.loadAd(interstitialAdRequest);
    }
	
	
	
	  @Override
	  public void onResume() {
	    super.onResume();
	  }
	
	  @Override
	  public void onPause() {
	    super.onPause();
	  }
	
	  /** Called before the activity is destroyed. */
	  @Override
	  public void onDestroy() {
	    super.onDestroy();
	  }
	  
	@Override
	public void showAds(boolean show) {
		if(show)
			handler.sendEmptyMessage(SHOW_ADS);
		else
			handler.sendEmptyMessage(HIDE_ADS);
		
	}
}
