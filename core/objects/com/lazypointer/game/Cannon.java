package com.lazypointer.game;

public class Cannon extends GameObject {

	private static final long serialVersionUID = 6668020342150131964L;

	public Cannon(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
		timer = new GameTimer();
		timer.setMilliSeconds(3000);
		timer.startTimer();
		speed = 1;
	}

	public void setPlayerPos(Player pc) {
		player = pc;
	}
	
	@Override
	public void init() {
	}
	
	@Override
	public void logic() {

		// If aimbot-cannon, aim towards player all the time
		if(TYPE == GLOBAL.OBJECT_CANNON_AIMBOT) {
			sprites.elementAt(0).rotation = (Math.toDegrees(Math.atan2(player.getMiddleY() - getMiddleY(), player.getMiddleX() - getMiddleX())));
		}
		
		// If timer is completed
		if(timer.isDone()) {
			
			// What angle should the fireball go
			double angle = 0;
			if(TYPE == GLOBAL.OBJECT_CANNON_LEFT)
				angle = Math.atan2(0, (getMiddleX() - 10) - getMiddleX());
			else if(TYPE == GLOBAL.OBJECT_CANNON_RIGHT)
				angle = Math.atan2(0, (getMiddleX() + 10) - getMiddleX());
			else if(TYPE == GLOBAL.OBJECT_CANNON_UP)
				angle = Math.atan2((getMiddleY() - 10) - getMiddleY(), 0);
			else if(TYPE == GLOBAL.OBJECT_CANNON_DOWN)
				angle = Math.atan2((getMiddleY() + 10) - getMiddleY(), 0);
			else
				angle = Math.atan2(player.getMiddleY() - getMiddleY(), player.getMiddleX() - getMiddleX());
		
			event_manager.objectpool.addObject(GLOBAL.SPELL_FIREBALL, getMiddleX(), getMiddleY(), angle, TYPE);

			// Restart timer
			timer.startTimer();
		}

	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {
	}

	@Override
	public void reset() {
		timer.startTimer();
	}

	private GameTimer timer;
	private Player player;
}
