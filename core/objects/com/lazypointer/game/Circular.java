package com.lazypointer.game;

public class Circular extends GameObject {

	private static final long serialVersionUID = 9135375210914233947L;

	public Circular(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
		radius = 0;
		angle = 0;
		origin_x = 0;
		origin_y = 0;
		timer = new GameTimer();
		timer.setMilliSeconds(25);
		speedy = 0.1;
	}
	
	@Override
	public void init() {
		// The attached event-object serves as the the origin
		for(int i = 0; i < event_objects.size(); i++) {
			origin_x = event_objects.elementAt(i).coordinates.x;
			origin_y = event_objects.elementAt(i).coordinates.y;
			radius = (int)(getMiddleX() - origin_x);
			if(radius < 0)
				radius *= -1;
		}
	}

	@Override
	public void logic() {

		if(!timer.isDone())
			return;
		
		angle += speedy;
		if(angle >= 360 || angle <= -360)
			angle = 0;
		
		// If moving circular, update originx/y
		if(TYPE == GLOBAL.OBJECT_CIRCULAR_MOVING) {
			origin_x = event_objects.elementAt(0).coordinates.x;
			origin_y = event_objects.elementAt(0).coordinates.y;
		}
		
		coordinates.x = (int) ((origin_x + Math.sin(angle)*radius));
		coordinates.y = (int) ((origin_y + Math.cos(angle)*radius));
		hitbox.updateHitbox(coordinates.x, coordinates.y);
		timer.startTimer();
	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {
		
		if(what != null && what.TYPE == GLOBAL.PLAYER && GLOBAL.immortal == 0) {
			GLOBAL.playerHit(TYPE);
		}
	}

	@Override
	public void reset() {
		timer.startTimer();
	}

	private double angle;
	private double origin_x;
	private double origin_y;
	private int radius;
	private GameTimer timer;
	private double speedy;

}
