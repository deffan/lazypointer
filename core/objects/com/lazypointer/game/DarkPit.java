package com.lazypointer.game;

public class DarkPit extends GameObject {

	private static final long serialVersionUID = 341238526067825554L;

	public DarkPit(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
		timer = new GameTimer();
		timer.setMilliSeconds(5000);
	}

	@Override
	public void init() {
		sprites.elementAt(0).texture_x = 384;
		sprites.elementAt(0).texture_y = 960;
		sprites.elementAt(0).relative_x = -24;
		sprites.elementAt(0).relative_y = -24;
		hitbox.width = 16;
		hitbox.height = 16;
		coordinates.x = (coordinates.x + 24);
		coordinates.y = (coordinates.y + 24);
		hitbox.updateHitbox(coordinates.x, coordinates.y);
		timer.startTimer();
		
		if(event_objects.size() == 0)
			System.out.println("WARNING: Pit has no safespot object attatched.");
		
		if(TYPE == GLOBAL.OBJECT_DARK_PIT_FADER)
			sprites.elementAt(0).alpha = 0;
	}
	
	@Override
	public void logic() {
		
		if(TYPE == GLOBAL.OBJECT_DARK_PIT_FADER) {
			if(timer.isDone()) {
				
				if(sprites.elementAt(0).alpha > 0.09)
					sprites.elementAt(0).alpha = (sprites.elementAt(0).alpha - 0.01f);
				else 
				{
					timer.startTimer();
					
					// Remove this object
					Event ev = new Event();
					ev.action = GLOBAL.ACTION_REMOVEME;
					ev.objects.add(this);
					ev.ids.add("GENERATOR");
					event_manager.addEvent(ev);
				}
			}
			else 
			{
				if(sprites.elementAt(0).alpha < 0.99)
					sprites.elementAt(0).alpha = (sprites.elementAt(0).alpha + 0.01f);
			}
		}
		
	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {
		
		if(what != null && what.TYPE == GLOBAL.PLAYER) {
			GLOBAL.playerHit(TYPE);
			
			// Set the safespot coordinates
			for(int i = 0; i < event_objects.size(); i++) {
				GLOBAL.safespot_x = event_objects.elementAt(i).coordinates.x;
				GLOBAL.safespot_y = event_objects.elementAt(i).coordinates.y;
			}
		}
		
	}

	@Override
	public void reset() {
	}
	
	private GameTimer timer;
}
