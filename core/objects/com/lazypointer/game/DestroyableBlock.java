package com.lazypointer.game;

public class DestroyableBlock extends GameObject {

	private static final long serialVersionUID = -4659280301480433381L;
	public DestroyableBlock(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
		explode = false;
		dead = false;
		frame = 0;
		animationTimer = new GameTimer();
		animationTimer.setMilliSeconds(25);
	}

	@Override
	public void logic() {
		
		if(!dead && explode) {
			
			if(animationTimer.isDone()) {
				
				frame++;
				if(frame > 5) {
					sprites.elementAt(0).alpha = 0;
					dead = true;
				}
				sprites.elementAt(0).texture_x = 394 + (frame * 170);
				animationTimer.startTimer();
			}
		}
	}

	@Override
	public void init() {
		sprites.elementAt(0).texture_x = 640;
		sprites.elementAt(0).texture_y = 192;
	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {
		
		if(what.TYPE == GLOBAL.SPELL_FIREBALL && collision) {
			explode = true;
			sprites.elementAt(0).texture_x = 394;
			sprites.elementAt(0).texture_y = 1210;
			sprites.elementAt(0).size_x = 170;
			sprites.elementAt(0).size_y = 170;
			sprites.elementAt(0).relative_x = -54;
			sprites.elementAt(0).relative_y = -54;
			collision = false;
		}
	}

	@Override
	public void reset() {
	}

	private Boolean explode;
	private Boolean dead;
	private int frame;
	private GameTimer animationTimer;
}
