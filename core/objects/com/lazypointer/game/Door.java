package com.lazypointer.game;

public class Door extends GameObject {

	private static final long serialVersionUID = -4030418098716176858L;

	public Door(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
		opening = false;
		current_element = 0;
		original_x = (int)coordinates.x;
		original_y = (int)coordinates.y;
	}

	@Override
	public void init() {
	}
	
	@Override
	public void logic() {
		
		// If opening
		if(opening)
			move();
		else
			checkEvents();
	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {
	}
	
	private void checkEvents() {

		if(event_listener.event_queue.size() > 0) {
			for(int i = 0; i < event_listener.event_queue.size(); i++) {
				
				// Trigger door opening
				if(event_listener.event_queue.elementAt(i).action == GLOBAL.OBJECT_TRIGGER_VISUAL || event_listener.event_queue.elementAt(i).action == GLOBAL.OBJECT_TRIGGER) {
					System.out.println("Door (" + ID + ") has been triggered.");
					opening = true;
				}
			}
			event_listener.event_queue.clear();
		}
	}
	
	private void move() {
		
		// Move along the waypoint
		if(waypoints.size() > 0) {
			
			double xDistance = coordinates.x - waypoints.elementAt(current_element).coordinates.x;
			double yDistance = coordinates.y - waypoints.elementAt(current_element).coordinates.y;
			double distance = Math.sqrt(xDistance * xDistance + yDistance * yDistance);
		   
				if (distance > waypoints.elementAt(current_element).speed + 1) {

					int move_x = 0;
					int move_y = 0;
					
					if(coordinates.x < waypoints.elementAt(current_element).coordinates.x)
						move_x = waypoints.elementAt(current_element).speed;
					else if(coordinates.x > waypoints.elementAt(current_element).coordinates.x + waypoints.elementAt(current_element).speed)
						move_x = -waypoints.elementAt(current_element).speed;
					
					if(coordinates.y < waypoints.elementAt(current_element).coordinates.y)
						move_y = waypoints.elementAt(current_element).speed;
					else if(coordinates.y > waypoints.elementAt(current_element).coordinates.y + waypoints.elementAt(current_element).speed)
						move_y = -waypoints.elementAt(current_element).speed;

					coordinates.x = (coordinates.x + move_x);
					coordinates.y = (coordinates.y + move_y);
					hitbox.updateHitbox(coordinates.x, coordinates.y);
				}
		}
	}

	private Boolean opening;
	private int current_element;
	private int original_x;
	private int original_y;

	@Override
	public void reset() {
		opening = false;
		current_element = 0;
		coordinates.x = original_x;
		coordinates.y = original_y;
		hitbox.updateHitbox(original_x, original_y);
	}
}
