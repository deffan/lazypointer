package com.lazypointer.game;

public class FinishLine extends GameObject {

	private static final long serialVersionUID = -7746893120550032015L;

	public FinishLine(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
		once = true;
	}

	@Override
	public void init() {
		sprites.elementAt(0).texture_x = 512;
		sprites.elementAt(0).texture_y = 192;
	}
	
	@Override
	public void logic() {
	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {
		if(what != null && what.TYPE == GLOBAL.PLAYER && once) {
			GLOBAL.current_state = GLOBAL.STATE_SUCCESS;			// Success
			GLOBAL.current_level++;									// Increase the level number
			GLOBAL.saveCurrentProgress();							// Save progress (level and coins)
			once = false;
			if(GLOBAL.soundActive) {
				GLOBAL.music.stop();
				GLOBAL.sound_success.play(GLOBAL.sound);
			}
		}
	}

	@Override
	public void reset() {
		once = true;
	}

	private Boolean once;
}
