package com.lazypointer.game;


public class Flamethrower extends GameObject {

	private static final long serialVersionUID = -558087010021837537L;

	public Flamethrower(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
		attacking = false;
		frame = 0;
		fadeFrame = 0;
		pauseTimer = new GameTimer();
		attackTimer = new GameTimer();
		animateTimer = new GameTimer();
		fadeInTimer = new GameTimer();
		fadeOutTimer = new GameTimer();
		fadeInTimer.setMilliSeconds(150);
		fadeOutTimer.setMilliSeconds(150);
		animateTimer.setMilliSeconds(25);
		pauseTimer.setMilliSeconds(2000);
		attackTimer.setMilliSeconds(2000);
	}

	@Override
	public void logic() {
		
		if(attacking) {
			
			// Quickly "fade" in
			if(!fadeInTimer.isDone() && fadeFrame > 0) {
				if(animateTimer.isDone()) {
					sprites.elementAt(0).texture_x = (1258 - (fadeFrame * 90));
					fadeFrame--;
					animateTimer.startTimer();
				}
				return;
			}
			
			if(attackTimer.isDone()) {
				attacking = false;
				pauseTimer.startTimer();
				fadeFrame = 0;
				fadeOutTimer.startTimer();
			}
			else {
				
				if(animateTimer.isDone()) {
					frame++;
					if(frame > 6)
						frame = 0;
					sprites.elementAt(0).texture_x = ((frame * 90) + 448);
					animateTimer.startTimer();
				}

			}
			
		}
		else {
			if(pauseTimer.isDone()) {
				attacking = true;
				attackTimer.startTimer();
				fadeInTimer.startTimer();
				fadeFrame = 5;
			}
			else {
				
				// Quickly "fade" out
				if(!fadeOutTimer.isDone() && fadeFrame < 6) {
					if(animateTimer.isDone()) {
						sprites.elementAt(0).texture_x = (1078 + (fadeFrame * 90));
						fadeFrame++;
						animateTimer.startTimer();
					}
					return;
				}
			}
		}
	}

	@Override
	public void init() {
		
		hitbox.height = 200;
		hitbox.width = 90;
		
		sprites.elementAt(0).texture_x = (448);
		sprites.elementAt(0).texture_y = (960);
		sprites.elementAt(0).size_x = (90);
		sprites.elementAt(0).size_y = (200);
	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {

		// Om spelaren kolliderar med objektet n�r den �r "active" och sprutar eld...
		if(attacking && what != null && what.TYPE == GLOBAL.PLAYER) {
			Event ev = new Event();
			ev.action = GLOBAL.ACTION_PLAYER_DEATH;
			ev.ids.add("PLAYER");
			event_manager.addEvent(ev);
		}

	}

	@Override
	public void reset() {

	}
	
	private GameTimer pauseTimer;
	private GameTimer attackTimer;
	private GameTimer animateTimer;
	private GameTimer fadeInTimer;
	private GameTimer fadeOutTimer;
	private int frame;
	private int fadeFrame;
	private Boolean attacking;

}
