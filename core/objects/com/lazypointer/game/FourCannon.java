package com.lazypointer.game;

import java.util.Random;

public class FourCannon extends GameObject {

	private static final long serialVersionUID = 103021680784166473L;

	public FourCannon(Coordinate new_coordinates, Hitbox new_hitbox,
			Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
		shootPauseTimer = new GameTimer();
		shootPauseTimer.setMilliSeconds(0);
		timer = new GameTimer();
		bounceTimer = new GameTimer();
		bounceTimer.setMilliSeconds(50);
		bounceTimer.startTimer();
		timer.setMilliSeconds(2000);
		timer.startTimer();
		rotate = 0;
		speed = 1;
		rand = new Random();
		vel_x = 1;
		vel_y = 1;
	}

	@Override
	public void init() {
		// Default sprite values
		sprites.elementAt(0).texture_x = (1089);
		sprites.elementAt(0).texture_y = (1);
		sprites.elementAt(0).size_x = (114);
		sprites.elementAt(0).size_y = (113);
		sprites.elementAt(0).relative_x = (-25);
		sprites.elementAt(0).relative_y = (-25);
		hitbox.updateHitbox(coordinates.x, coordinates.y);

	}
	
	@Override
	public void logic() {

		// Rotate
		if(TYPE == GLOBAL.OBJECT_FOUR_CANNON_ROTATING && shootPauseTimer.isDone()) {
			rotate += speed;
			if(rotate >= 360 || rotate <= -360)
				rotate = 0;	
			sprites.elementAt(0).rotation = (rotate);	
		}
		else if(TYPE == GLOBAL.OBJECT_FOUR_CANNON_BOUNCE ) {
			
			// Bounce
			if(bounceTimer.isDone()) {
				coordinates.x = (coordinates.x + (int)vel_x);
				coordinates.y = (coordinates.y + (int)vel_y);
				hitbox.updateHitbox(coordinates.x, coordinates.y);
				bounceTimer.startTimer();
			}

			// Rotate
			rotate += speed;
			if(rotate >= 360 || rotate <= -360)
				rotate = 0;	
			sprites.elementAt(0).rotation = (rotate);	
		}
		
		// If timer is done
		if(timer.isDone()) 
		{
			event_manager.objectpool.addObject(GLOBAL.SPELL_FIREBALL, getMiddleX(), getMiddleY(), Math.atan2(0, (getMiddleX() - 10) - getMiddleX()) + Math.toRadians(rotate), TYPE);
			event_manager.objectpool.addObject(GLOBAL.SPELL_FIREBALL, getMiddleX(), getMiddleY(), Math.atan2(0, (getMiddleX() + 10) - getMiddleX()) + Math.toRadians(rotate), TYPE);
			event_manager.objectpool.addObject(GLOBAL.SPELL_FIREBALL, getMiddleX(), getMiddleY(), Math.atan2((getMiddleY() - 10) - getMiddleY(), 0) + Math.toRadians(rotate), TYPE);
			event_manager.objectpool.addObject(GLOBAL.SPELL_FIREBALL, getMiddleX(), getMiddleY(), Math.atan2((getMiddleY() + 10) - getMiddleY(), 0) + Math.toRadians(rotate), TYPE);
			event_manager.objectpool.addObject(GLOBAL.SPELL_FIREBALL, getMiddleX(), getMiddleY(), Math.atan2(0, (getMiddleX() + 10) - getMiddleX()) + Math.toRadians(45) + Math.toRadians(rotate), TYPE);
			event_manager.objectpool.addObject(GLOBAL.SPELL_FIREBALL, getMiddleX(), getMiddleY(), Math.atan2(0, (getMiddleX() - 10) - getMiddleX()) + Math.toRadians(45) + Math.toRadians(rotate), TYPE);
			event_manager.objectpool.addObject(GLOBAL.SPELL_FIREBALL, getMiddleX(), getMiddleY(), Math.atan2((getMiddleY() - 10) - getMiddleY(), 0) + Math.toRadians(45) + Math.toRadians(rotate), TYPE);
			event_manager.objectpool.addObject(GLOBAL.SPELL_FIREBALL, getMiddleX(), getMiddleY(), Math.atan2((getMiddleY() + 10) - getMiddleY(), 0) + Math.toRadians(45) + Math.toRadians(rotate), TYPE);

			// Restart timer
			timer.startTimer();
			shootPauseTimer.startTimer();
		}
	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {
		
		if(what == null) {
			vel_x = rand.nextDouble() + speed;
			vel_y = rand.nextDouble() + speed;
			
			if(rand.nextBoolean())
				vel_y *= -1;
			if(rand.nextBoolean())
				vel_x *= -1;
		}	
	}

	@Override
	public void reset() {
		rotate = 0;
		sprites.elementAt(0).rotation = (0);
		timer.startTimer();
	}
	
	private GameTimer timer;
	private GameTimer shootPauseTimer;
	private GameTimer bounceTimer;
	private double rotate;
	private Random rand;
	private double vel_x;
	private double vel_y;

}
