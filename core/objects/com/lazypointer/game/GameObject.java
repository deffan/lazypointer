package com.lazypointer.game;

import java.io.Serializable;
import java.util.Vector;

public abstract class GameObject implements Serializable {

	private static final long serialVersionUID = -5975313572913336192L;
	
	/**
	* GameObject
	* @param <b>new_coordinates:</b> The X and Y coordinates.
	* @param <b>new_hitbox:</b> The objects "hitbox" using a RECT.
	* @param <b>new_sprite:</b> The objects Sprite.
	* @param <b>is_collidable:</b> Decides wether you can collide with this object or not.
	*/
	public GameObject(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		coordinates = new_coordinates;
		hitbox = new_hitbox;
		collision = is_collidable;
		speed = 0;
		collision_type = GLOBAL.COLLISION_BOX;
		ID = "";
		TYPE = 0;
		collision_event = false;
		event_listener = null;
		event_manager = null;
		event_objects = new Vector<GameObject>();
		waypoints = new Vector<Waypoint>();
		sprites = new Vector<Sprite>();
		sprites.add(new_sprite);
	}
	
	// GET
	public Boolean isCollidable() { return collision; }
	public Boolean isCollidableEvent() { return collision_event; }
	public double getMiddleX() { return coordinates.x + (hitbox.width / 2); }
	public double getMiddleY() { return coordinates.y + (hitbox.height / 2); }
	public int getDirectionX() { return direction_x; }
	public int getDirectionY() { return direction_y; }
	public int getCollisionType() { return collision_type; }
	public Listener getEventListener() { return event_listener; }
	//public Boolean getJumping() { return jumping; }
	//public Boolean getFalling() { return falling; }
	//public int getVelocity() { return velocity; }
	//public Boolean hasGravity() { return has_gravity; }
	//public List<GameObject> getChildren() { return children; }
	public Vector<Waypoint> getWaypoints() { return waypoints; }
	public Vector<GameObject> getEventObjects() { return event_objects; }
	
	// SET
	public void setCollidable(Boolean collidable) { collision = collidable; }
	public void setCollidableEvent(Boolean collidable) { collision_event = collidable; }
	public void setDirectionX(int new_direction) { direction_x = new_direction; }
	public void setDirectionY(int new_direction) { direction_y = new_direction; }
	public void setCollisionType(int new_collision_type) { collision_type = new_collision_type; }
	//public void setJumping(Boolean j) { jumping = j; }
	//public void setFalling(Boolean j) { falling = j; }
	//public void setVelocity(int v) { velocity = v; }
	//public void setHasGravity(Boolean g) { has_gravity = g; }
	
	// Set this GameObject up for receiving and sending events
	public void setEventCapable(EventManager ev) { 
		event_manager = ev; 
		event_listener = new Listener(); 
		ev.addListener(ID, event_listener);
	}

	/**
	 * The logic method which will be called each frame for this object.
	 */
	public abstract void logic();
	
	/**
	 * If you need to initialize something after the constructor
	 */
	public abstract void init();
	
	/**
	 * If some other object (what) collided with this object.
	 */
	public abstract void collision(GameObject what, CollisionInformation info);
	
	/**
	 * Reset this object to "default" (as it was when initialized)
	 */
	public abstract void reset();
	
	// DATAMEMBERS
	public Coordinate coordinates;
	public int speed;
	protected int direction_x;
	protected int direction_y;
	protected int collision_type;
	public Hitbox hitbox;
	protected Boolean collision;
	//protected Boolean jumping;
	//protected Boolean falling;
	//protected Boolean has_gravity;
	//protected int velocity;
	protected Boolean collision_event;
	public int TYPE;
	public String ID;
	protected Listener event_listener;
	protected EventManager event_manager;
	public Vector<Sprite> sprites;
	protected Vector<Waypoint> waypoints;
	protected Vector<GameObject> event_objects;	// GameObjects used for "events" like Portal has a "destination object" to portal to...
	//protected List<GameObject> children;
	
}
