package com.lazypointer.game;

import java.util.List;

public class Jumper extends GameObject {

	private static final long serialVersionUID = 1970181717435892223L;
	public Jumper(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
		frame = 0;
		frameTimer = new GameTimer();
		frameTimer.setMilliSeconds(25);
		shootTimer = new GameTimer();
		shootTimer.setMilliSeconds(3000);
		forward = true;
		speed = 1;
		pathfinder = new AStar();
		onpath = false;
		current_element = 0;
		astarTimer = new GameTimer();
		astarTimer.setMilliSeconds(4000);
	}

	@Override
	public void logic() {
		
		// Animate
		if(frameTimer.isDone()) {
			if(forward) {
				frame++;
				if(frame > 9) {
					frame = 9;
					forward = false;
				}		
			}
			else {
				frame--;
				if(frame < 0) {
					frame = 0;
					forward = true;
				}	
			}
			sprites.elementAt(0).texture_x = frame * GLOBAL.TILE_SIZE;
			frameTimer.startTimer();
		}
		
		// Shoot
		if(shootTimer.isDone()) {
			double angle = Math.atan2(player.getMiddleY() - getMiddleY(), player.getMiddleX() - getMiddleX());
			event_manager.objectpool.addObject(GLOBAL.SPELL_FIREBALL, getMiddleX(), getMiddleY(), angle, TYPE);
			shootTimer.startTimer();
		}
		
		// If follower
		if(TYPE == GLOBAL.OBJECT_JUMPER_FOLLOW) {
			if(!onpath && astarTimer.isDone()) 
			{
				double tx = player.coordinates.x - getMiddleX();
				double ty = player.coordinates.y - getMiddleY();
				double distance = Math.sqrt(tx*tx+ty*ty);
				 
				if(distance > 5) {
					coordinates.x = (coordinates.x + ((tx / distance) * speed));
					coordinates.y = (coordinates.y + ((ty / distance) * speed));
					hitbox.updateHitbox((int)coordinates.x, (int)coordinates.y);
				}
			}
			else 
			{
				// Move along the waypoints
				if(waypoints.size() > 0) {

					double xDistance = coordinates.x - waypoints.elementAt(current_element).coordinates.x;
					double yDistance = coordinates.y - waypoints.elementAt(current_element).coordinates.y;
					double distance = Math.sqrt(xDistance * xDistance + yDistance * yDistance);
				   
					if (distance > waypoints.elementAt(current_element).speed + 1) {

						double move_x = 0;
						double move_y = 0;
						
						if(coordinates.x < waypoints.elementAt(current_element).coordinates.x)
							move_x = waypoints.elementAt(current_element).speed;
						else if(coordinates.x > waypoints.elementAt(current_element).coordinates.x + waypoints.elementAt(current_element).speed)
							move_x = -waypoints.elementAt(current_element).speed;
						
						if(coordinates.y < waypoints.elementAt(current_element).coordinates.y)
							move_y = waypoints.elementAt(current_element).speed;
						else if(coordinates.y > waypoints.elementAt(current_element).coordinates.y + waypoints.elementAt(current_element).speed)
							move_y = -waypoints.elementAt(current_element).speed;

						coordinates.x = (coordinates.x + move_x);
						coordinates.y = (coordinates.y + move_y);
						hitbox.updateHitbox(coordinates.x, coordinates.y);
					}
					else {
						
						if(current_element < waypoints.size() - 1)
							current_element++;
						else {
							current_element = 0;
							onpath = false;
						}
				   }
				}
			}
		}
		
	}

	@Override
	public void init() {
		sprites.elementAt(0).texture_x = 0;
		sprites.elementAt(0).texture_y = 1016;
		sprites.elementAt(0).size_y = 73;
		hitbox.width = 58;
		hitbox.height = 58;
		hitbox.updateHitbox(coordinates.x, coordinates.y);
	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {
		
		if(what == null && TYPE == GLOBAL.OBJECT_JUMPER_FOLLOW && astarTimer.isDone()) 
		{	
			onpath = true;	
			waypoints.clear();
			List<Coordinate> path = pathfinder.getPath(coordinates, player.coordinates, level);
		
			if(path == null) {
				System.out.println("No path");
				onpath = false;
			}
			else {
				for(int i = 1; i < path.size(); i++) {	// start at 1, dont include first coordinate as it makes it go backwards sometimes
					//level.world[path.get(i).x][path.get(i).y] = new Tile(level.world[path.get(i).x][path.get(i).y].coordinates, level.world[path.get(i).x][path.get(i).y].hitbox, new Sprite(64, 0, 1, 1), false);
					waypoints.add(new Waypoint((int)path.get(i).x * GLOBAL.TILE_SIZE, (int)path.get(i).y * GLOBAL.TILE_SIZE, speed, 0));
				}
			}
			current_element = 0;
			astarTimer.startTimer();
		}
		else if(what != null && what.TYPE == GLOBAL.PLAYER && GLOBAL.immortal == 0) {
			GLOBAL.playerHit(TYPE);
		}
	}

	@Override
	public void reset() {
	}

	private int frame;
	private GameTimer frameTimer;
	private GameTimer shootTimer;
	private Boolean forward;
	public Player player;
	private GameTimer astarTimer;
	private AStar pathfinder;
	public Level level;
	private Boolean onpath;
	private int current_element;
}
