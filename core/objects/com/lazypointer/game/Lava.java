package com.lazypointer.game;

import java.util.Random;
import java.util.Vector;

public class Lava extends GameObject {

	private static final long serialVersionUID = -4176541798521008291L;

	public Lava(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);

	}

	@Override
	public void logic() {
		

	}

	@Override
	public void init() {

	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {
	}

	@Override
	public void reset() {
	}

}
