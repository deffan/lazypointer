package com.lazypointer.game;

public class Mine extends GameObject {

	private static final long serialVersionUID = -1518813455251329938L;

	public Mine(Coordinate new_coordinates, Hitbox new_hitbox,
			Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
		sprites.add(new Sprite(0,0,0,0));
		turn = true;
	}

	@Override
	public void init() {
		
		hitbox.width = 20;
		hitbox.height = 20;
		hitbox.updateHitbox(hitbox.left, hitbox.top);
		
		sprites.elementAt(0).texture_x = (704);
		sprites.elementAt(0).texture_y = (256);
		sprites.elementAt(0).relative_x = (-22);	// -(tilesize - (new_width / 2))
		sprites.elementAt(0).relative_y = (-22); // -(tilesize - (new_height / 2))
		sprites.elementAt(0).alpha = (1);
		
		sprites.elementAt(1).texture_x = (sprites.elementAt(0).texture_x + GLOBAL.TILE_SIZE);
		sprites.elementAt(1).texture_y = (sprites.elementAt(0).texture_y);
		sprites.elementAt(1).relative_x = (-22);
		sprites.elementAt(1).relative_y = (-22);
		sprites.elementAt(1).alpha = (0);
	}
	
	@Override
	public void logic() {
		
		if(turn)
		{
			sprites.elementAt(0).alpha = (sprites.elementAt(0).alpha - 0.01f);
			sprites.elementAt(1).alpha = (sprites.elementAt(1).alpha + 0.01f);
			if(sprites.elementAt(0).alpha <= 0.3)
				turn = false;
		}
		else 
		{
			sprites.elementAt(1).alpha = (sprites.elementAt(1).alpha - 0.01f);
			sprites.elementAt(0).alpha = (sprites.elementAt(0).alpha + 0.01f);
			if(sprites.elementAt(1).alpha <= 0.3)
				turn = true;
		}
	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {
		
		if(what != null && what.TYPE == GLOBAL.PLAYER && GLOBAL.immortal == 0) {
			GLOBAL.playerHit(TYPE);
		}
	}

	@Override
	public void reset() {
	}

	private Boolean turn;
}
