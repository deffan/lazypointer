package com.lazypointer.game;


public class MovingBlock extends GameObject {

	private static final long serialVersionUID = 5110979908437774707L;

	MovingBlock(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
		current_element = 0;
		mov = 1;
		original_x = (int)coordinates.x;
		original_y = (int)coordinates.y;
	}

	@Override
	public void init() {
	}
	
	@Override
	public void logic() {
		
		// Move along the waypoints
		if(waypoints.size() > 0) {
			
			// This is ONLY to prevent a crash in the editor (if you remove the current waypoint during editing)
			if(current_element >= waypoints.size())
				current_element--;
			
			double xDistance = coordinates.x - waypoints.elementAt(current_element).coordinates.x;
			double yDistance = coordinates.y - waypoints.elementAt(current_element).coordinates.y;
			double distance = Math.sqrt(xDistance * xDistance + yDistance * yDistance);
		   
				if (distance > waypoints.elementAt(current_element).speed + 1) {

					int move_x = 0;
					int move_y = 0;
					
					if(coordinates.x < waypoints.elementAt(current_element).coordinates.x)
						move_x = waypoints.elementAt(current_element).speed;
					else if(coordinates.x > waypoints.elementAt(current_element).coordinates.x + waypoints.elementAt(current_element).speed)
						move_x = -waypoints.elementAt(current_element).speed;
					
					if(coordinates.y < waypoints.elementAt(current_element).coordinates.y)
						move_y = waypoints.elementAt(current_element).speed;
					else if(coordinates.y > waypoints.elementAt(current_element).coordinates.y + waypoints.elementAt(current_element).speed)
						move_y = -waypoints.elementAt(current_element).speed;

					coordinates.x = (coordinates.x + move_x);
					coordinates.y = (coordinates.y + move_y);
					hitbox.updateHitbox(coordinates.x, coordinates.y);
				}
				else {
					
					// Start the timer
					waypoints.elementAt(current_element).getTimer().startTimer();
					
					// If the timer is done
					if(waypoints.elementAt(current_element).getTimer().isDone()) 
					{
						// Move back and forth, constantly
						if(current_element == 0)
							mov = 1;
						else if(current_element == waypoints.size() - 1)
							mov = -1;
						
						current_element += mov;
					}
			   }
		}
	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {
		if(what != null && what.TYPE == GLOBAL.PLAYER && GLOBAL.immortal == 0) {
			GLOBAL.playerHit(TYPE);
		}
	}
	
	// DATAMEMBERS
	private int current_element;
	private int mov;
	private int original_x;
	private int original_y;

	@Override
	public void reset() {
		current_element = 0;
		mov = 1;
		coordinates.x = (original_x);
		coordinates.y = (original_y);
		hitbox.updateHitbox(original_x, original_y);
	}


}
