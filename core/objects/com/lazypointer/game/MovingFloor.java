package com.lazypointer.game;

public class MovingFloor extends GameObject {

	private static final long serialVersionUID = -699460817702085609L;

	public MovingFloor(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
		movePlayerTimer = new GameTimer();
		movePlayerTimer.setMilliSeconds(25);
		speed = 3;
	}

	@Override
	public void logic() {
	}

	@Override
	public void init() {
	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {
		
		if(what.TYPE == GLOBAL.PLAYER && movePlayerTimer.isDone()) {
			if(TYPE == GLOBAL.OBJECT_MOVINGFLOOR_RIGHT)
				what.coordinates.x += speed;
			else if(TYPE == GLOBAL.OBJECT_MOVINGFLOOR_LEFT)
				what.coordinates.x -= speed;
			else if(TYPE == GLOBAL.OBJECT_MOVINGFLOOR_UP)
				what.coordinates.y -= speed;
			else if(TYPE == GLOBAL.OBJECT_MOVINGFLOOR_DOWN)
				what.coordinates.y += speed;
			what.hitbox.updateHitbox(what.coordinates.x, what.coordinates.y);
			movePlayerTimer.startTimer();
		}
	}

	@Override
	public void reset() {
	}
	private GameTimer movePlayerTimer;
}
