package com.lazypointer.game;

import java.util.Random;

public class ObjectGenerator extends GameObject {

	private static final long serialVersionUID = -4178304881836780717L;

	public ObjectGenerator(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
		rand = new Random();
		generatedObjects = 0;
	}

	public void setObjectGeneratorData(Level l) {
		level = l;
	}
	
	@Override
	public void init() {
	}
	
	@Override
	public void logic() {
		
		// Listen to events that come in
		if(event_listener.event_queue.size() > 0) {
			for(int i = 0; i < event_listener.event_queue.size(); i++) {
				
				if(event_listener.event_queue.elementAt(i).action == GLOBAL.OBJECT_TRIGGER_ADDER)
					addTiles(i);
				
				if(event_listener.event_queue.elementAt(i).action == GLOBAL.OBJECT_TRIGGER_REMOVER)
					removeTiles(i);
				
				// PIT
				if(event_listener.event_queue.elementAt(i).action == GLOBAL.ACTION_PITFADE)
					createPit(i);
				
				// REMOVE ME
				if(event_listener.event_queue.elementAt(i).action == GLOBAL.ACTION_REMOVEME)
					if(event_listener.event_queue.elementAt(i).objects.size() > 0)
						removeObject(event_listener.event_queue.elementAt(i).objects.elementAt(0));
			}
			event_listener.event_queue.clear();
		}
	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {
	}

	@Override
	public void reset() {
	}
	
	private void shootFireBall(int i) {
		
		if(event_listener.event_queue.elementAt(i).objects.size() > 0) {

			// Construct a fireball
			Hitbox hitbox = new Hitbox((int)event_listener.event_queue.elementAt(i).objects.elementAt(0).getMiddleX(),
					(int)event_listener.event_queue.elementAt(i).objects.elementAt(0).getMiddleX() + 25,
					(int)event_listener.event_queue.elementAt(i).objects.elementAt(0).getMiddleY(), 
					(int)event_listener.event_queue.elementAt(i).objects.elementAt(0).getMiddleY() + 25);
			
			// The coordinate needs to be the MIDDLE of the object that shoots the fireball - HALF the size of the fireball itself.
			// Meaning, half of hitbox of a cannon minus half the hitbox of the fireball
			Coordinate coordinate = new Coordinate((int)event_listener.event_queue.elementAt(i).objects.elementAt(0).getMiddleX() - 12,
					(int)event_listener.event_queue.elementAt(i).objects.elementAt(0).getMiddleY() - 12);
			Sprite sprite = new Sprite(0, 768, 1, GLOBAL.NORMAL_LAYER);
			sprite.relative_x = (-16);
			sprite.relative_y = (-16);
			Fireball f = new Fireball(coordinate, hitbox, sprite, true);
			f.setAngle(event_listener.event_queue.elementAt(i).number_data);
			f.setEventCapable(event_manager);
			level.spells.add(f);

		}
	}
	
	private void fireExplosion(int i) {
		
		if(event_listener.event_queue.elementAt(i).objects.size() > 0) {

			// Construct an explosion
			Hitbox hitbox = new Hitbox((int)event_listener.event_queue.elementAt(i).objects.elementAt(0).getMiddleX(),
					(int)event_listener.event_queue.elementAt(i).objects.elementAt(0).getMiddleX() + 25,
					(int)event_listener.event_queue.elementAt(i).objects.elementAt(0).getMiddleY(), 
					(int)event_listener.event_queue.elementAt(i).objects.elementAt(0).getMiddleY() + 25);
			
			Coordinate coordinate = new Coordinate((int)event_listener.event_queue.elementAt(i).objects.elementAt(0).getMiddleX() - 12,(int)
					event_listener.event_queue.elementAt(i).objects.elementAt(0).getMiddleY() - 12);
			Sprite sprite = new Sprite(0, 0, 0, 0);
			FireExplosion f = new FireExplosion(coordinate, hitbox, sprite, false);
			f.init();
			level.spells.add(f);
			
		}
	}
	
	private void makeSlime(int i) {
		
		if(event_listener.event_queue.elementAt(i).objects.size() > 0) {

			// Construct a slime
			Hitbox hitbox = new Hitbox((int)event_listener.event_queue.elementAt(i).objects.elementAt(0).getMiddleX(),
					(int)event_listener.event_queue.elementAt(i).objects.elementAt(0).getMiddleX() + 25,
					(int)event_listener.event_queue.elementAt(i).objects.elementAt(0).getMiddleY(), 
					(int)event_listener.event_queue.elementAt(i).objects.elementAt(0).getMiddleY() + 25);
			
			Coordinate coordinate = new Coordinate((int)event_listener.event_queue.elementAt(i).objects.elementAt(0).coordinates.x,
					(int)event_listener.event_queue.elementAt(i).objects.elementAt(0).coordinates.y);
			Sprite sprite = new Sprite(0, 0, 1, 0);
			
			Slime s = new Slime(coordinate, hitbox, sprite, true);
			s.sprites.elementAt(0).rotation = (rand.nextInt(360));
			s.setEventCapable(event_manager);
			s.init();
			level.spells.add(s);
		}
	}
	
	private void createPit(int i) {
		
		int x = Integer.parseInt((event_listener.event_queue.elementAt(i).string_data.elementAt(0)));
		int y = Integer.parseInt((event_listener.event_queue.elementAt(i).string_data.elementAt(1)));
		Coordinate coordinate = new Coordinate(x, y);
		GameObject obj = new DarkPit(coordinate, new Hitbox(0, 0, 0, 0), new Sprite(0, 0, 1, 0), false); 
		obj.ID = ("GENERATED_PIT_" + generatedObjects);
		obj.TYPE = (GLOBAL.OBJECT_DARK_PIT_FADER);
		obj.setCollidableEvent(true);
		obj.setEventCapable(level.level_event_manager);	
		obj.init();
		level.objects.add(obj);
	}
	
	private void removeObject(GameObject o) {
      	for(int i = 0; i < level.objects.size(); i++) {
    		if(o.equals(level.objects.elementAt(i))) {
    			level.objects.remove(i);
    		}
    	}
	}
	
	private void addTiles(int i) {
		for(int n = 0; n < event_listener.event_queue.elementAt(i).objects.size(); n++) {
			int x = (int)event_listener.event_queue.elementAt(i).objects.elementAt(n).coordinates.x;
			int y = (int)event_listener.event_queue.elementAt(i).objects.elementAt(n).coordinates.y;
			Coordinate coordinate = new Coordinate(x, y);
			Sprite sprite = new Sprite(640, 256, 1, 0);
			Hitbox hitbox = new Hitbox(x, x + GLOBAL.TILE_SIZE, y, y + GLOBAL.TILE_SIZE);
			level.world[x / GLOBAL.TILE_SIZE][y / GLOBAL.TILE_SIZE] = new Tile(coordinate, hitbox, sprite, true);
		}
		
	}
	
	// "Remove" tiles by making them invisible and not collidable
	private void removeTiles(int i) {
		for(int n = 0; n < event_listener.event_queue.elementAt(i).objects.size(); n++) {
			int x = (int)event_listener.event_queue.elementAt(i).objects.elementAt(n).coordinates.x / GLOBAL.TILE_SIZE;
			int y = (int)event_listener.event_queue.elementAt(i).objects.elementAt(n).coordinates.y / GLOBAL.TILE_SIZE;
			level.world[x][y].setCollidable(false);
			level.world[x][y].sprites.elementAt(0).texture_x = 0;
			level.world[x][y].sprites.elementAt(0).texture_y = 0;
		}
	}
	
	private Level level;
	private Random rand;
	private double generatedObjects;

}
