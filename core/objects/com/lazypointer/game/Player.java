package com.lazypointer.game;


public class Player extends GameObject {

	private static final long serialVersionUID = 640532645713780628L;

	/**
	* Player
	* @param <b>new_coordinates:</b> The X and Y coordinates.
	* @param <b>new_hitbox:</b> The objects "hitbox" using a RECT.
	* @param <b>new_sprite:</b> The objects Sprite.
	* @param <b>is_collidable:</b> Decides wether you can collide with this object or not.
	*/
	Player(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
		moving = false;
		point_x = 0;
		point_y = 0;
		real_x = 0;
		real_y = 0;
		rotation = 0;
		wasHit = false;
		
		sprites.elementAt(0).texture_y = (1528);	// Feet sprite
		sprites.add(new Sprite(0, 1464, 1, 0));	// Shoulder
		sprites.add(new Sprite(0, 1400, 1, 0));	// Head sprite
		
		sprites.elementAt(0).relative_x = (-15);
		sprites.elementAt(0).relative_y = (-15);
		sprites.elementAt(1).relative_x = (-15);
		sprites.elementAt(1).relative_y = (-15);
		sprites.elementAt(2).relative_x = (-15);
		sprites.elementAt(2).relative_y = (-15);
		
		frame = 4;
		animateTimer = new GameTimer();
		immortalTimer = new GameTimer();
		blinkTimer = new GameTimer();
		immortalTimer.setMilliSeconds(1000);
		blinkTimer.setMilliSeconds(50);
		animateTimer.setMilliSeconds(25);
		forwardAnimation = false;
	}
	
	@Override
	public void init() {
	}
	
	public void moveLeft() {
		coordinates.x = (coordinates.x - (int)speed);
	}
	
	public void moveRight() {
		coordinates.x = (coordinates.x + (int)speed);
	}
	
	public void moveUp() {
		coordinates.y = (coordinates.y - (int)speed);
	}
	
	public void moveDown() {
		coordinates.y = (coordinates.y + (int)speed);
	}

	public void setMoving(Boolean moving_status, int to_x, int to_y) {
		moving = moving_status;
		rotation = Math.atan2(to_y - getMiddleY(), to_x - getMiddleX()) * (180 / Math.PI);
		point_x = to_x;
		point_y = to_y;
		sprites.elementAt(0).rotation = rotation;
		sprites.elementAt(1).rotation = rotation;
		sprites.elementAt(2).rotation = rotation;
	}
	
	private void move() {
		
		double tx = point_x - getMiddleX();
		double ty = point_y - getMiddleY();
		double distance = Math.sqrt(tx*tx + ty*ty);
		real_x = (((tx / distance) * speed));
		real_y = (((ty / distance) * speed));
		
		if(distance > 20) {
			coordinates.y += real_y;
			coordinates.x += real_x;
			hitbox.updateHitbox(coordinates.x, coordinates.y);
		}
		else
			moving = false;
	}
	
	@Override
	public void logic() {
		
		// If immortal
		if(GLOBAL.immortal > 0) {
			
			// Player has been hit...
			if(!wasHit) {
				
				// Create a "-1" animation
				event_manager.objectpool.lostlife.coordinates.x = coordinates.x + 15;
				event_manager.objectpool.lostlife.coordinates.y = coordinates.y - 20;
				event_manager.objectpool.lostlife.reset();
				
				// If the player fell down the pit, move to the safespot
				if(GLOBAL.player_hit_by == GLOBAL.OBJECT_DARK_PIT) {
					coordinates.x = GLOBAL.safespot_x;
					coordinates.y = GLOBAL.safespot_y;
					hitbox.updateHitbox(coordinates.x, coordinates.y);
					moving = false;
				}
				wasHit = true;
			}

			// Blink sprites
			if(blinkTimer.isDone()) {
				
				if(sprites.elementAt(0).alpha > 0) {
					sprites.elementAt(0).alpha = 0;
					sprites.elementAt(1).alpha = 0;
					sprites.elementAt(2).alpha = 0;
				}
				else {
					sprites.elementAt(0).alpha = 1;
					sprites.elementAt(1).alpha = 1;
					sprites.elementAt(2).alpha = 1;	
				}
				blinkTimer.startTimer();
			}
			
			// Reduce by 1 per second
			if(immortalTimer.isDone()) {
				GLOBAL.immortal--;
				if(GLOBAL.immortal == 0) {
					sprites.elementAt(0).alpha = 1;
					sprites.elementAt(1).alpha = 1;
					sprites.elementAt(2).alpha = 1;
					wasHit = false;
				}
				immortalTimer.startTimer();
			}
		}	
		
		if(moving) {
			move();
			animate();
		}
		else {
			// Default shoulder frame (standing still)
			sprites.elementAt(0).texture_x = (256);
			sprites.elementAt(1).texture_x = (256);
			frame = 4;
		}
		//checkEvents();
	}
	
	private void animate() {
		
		if(animateTimer.isDone()) {
			
			if(forwardAnimation) {
				frame++;
				if(frame > 7)
					forwardAnimation = false;
			}
			else {
				frame--;
				if(frame < 1)
					forwardAnimation = true;
			}
			sprites.elementAt(0).texture_x = (frame * GLOBAL.TILE_SIZE);
			sprites.elementAt(1).texture_x = (frame * GLOBAL.TILE_SIZE);
			animateTimer.startTimer();
		}
		
	}
	
	private void checkEvents() {

		if(event_listener.event_queue.size() > 0) {
			for(int i = 0; i < event_listener.event_queue.size(); i++) {
				
				// PLAYER DEATH - BY PIT / FALLING DOWN
				if(event_listener.event_queue.elementAt(i).action == GLOBAL.ACTION_PLAYER_DEATH) {
					moving = false;
				}
				else if(event_listener.event_queue.elementAt(i).action == GLOBAL.ACTION_TELEPORT) {
					
					for(int n = 0; n < event_listener.event_queue.elementAt(i).objects.size(); n++) {
						
						int new_x = (int)event_listener.event_queue.elementAt(i).objects.elementAt(n).coordinates.x;
						int new_y = (int)event_listener.event_queue.elementAt(i).objects.elementAt(n).coordinates.y;
						coordinates.x = new_x;
						coordinates.y = (new_y);
						hitbox.updateHitbox(new_x, new_y);
						moving = false;	
					}
				}
			}
			event_listener.event_queue.clear();
		}
	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {
		if(what.TYPE == GLOBAL.OBJECT_POWERUP_LIFE) {
			
			// Create a "+1" animation
			if(((Life) what).once == 0) {
				event_manager.objectpool.extralife.coordinates.x = coordinates.x + 15;
				event_manager.objectpool.extralife.coordinates.y = coordinates.y - 20;
				event_manager.objectpool.extralife.reset();
				((Life) what).once = 1;
			}
		}
	}
	
	private Boolean moving;
	private double point_x;
	private double point_y;
	private double real_x;
	private double real_y;
	private double rotation;
	private int frame;
	private Boolean wasHit;
	private GameTimer animateTimer;
	private Boolean forwardAnimation;
	private GameTimer immortalTimer;
	private GameTimer blinkTimer;

	@Override
	public void reset() {
		
	}
}

