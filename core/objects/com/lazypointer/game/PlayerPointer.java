package com.lazypointer.game;

public class PlayerPointer extends GameObject {

	private static final long serialVersionUID = -6206547598770323962L;

	public PlayerPointer(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
		timer = new GameTimer();
		timer.setMilliSeconds(25);
		clicked = false;
		sprites.elementAt(0).texture_x = (0);
		sprites.elementAt(0).texture_y = (1024);
		frame = 0;
	}

	public void Click(int x, int y) {
		coordinates.x = (x);
		coordinates.y = (y);
		clicked = true;
		sprites.elementAt(0).alpha = (1);
		frame = -1;	
	}
	
	@Override
	public void logic() {
		
		if(clicked) {
			if(timer.isDone()) {
				
				if(sprites.elementAt(0).alpha <= 0)
					clicked = false;
				else
					sprites.elementAt(0).alpha = (sprites.elementAt(0).alpha - 0.05f);
				
				timer.startTimer();
				
				
				if(frame == 9)
					return;
				frame++;
				sprites.elementAt(0).texture_x = (frame * GLOBAL.TILE_SIZE);
			}
		}
	}

	@Override
	public void init() {
	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {
	}

	@Override
	public void reset() {
	}

	private GameTimer timer;
	private Boolean clicked;
	private int frame;
	
}
