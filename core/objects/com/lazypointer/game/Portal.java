package com.lazypointer.game;

import java.util.Random;
import java.util.Vector;

public class Portal extends GameObject {

	private static final long serialVersionUID = -3235098367518502480L;

	public Portal(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);

	}
	
	@Override
	public void init() {
		

	}

	@Override
	public void logic() {
		

	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {

	}

	@Override
	public void reset() {
	}

}
