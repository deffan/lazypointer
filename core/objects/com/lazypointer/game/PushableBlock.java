package com.lazypointer.game;

public class PushableBlock extends GameObject {

	private static final long serialVersionUID = -4753661438974408021L;
	public PushableBlock(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
	}

	@Override
	public void logic() {
	}

	@Override
	public void init() {
		sprites.elementAt(0).texture_x = 576;
		sprites.elementAt(0).texture_y = 192;
	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {

		if(what == null) {
			// v�gg, stanna.
		}
		else if(what.TYPE == GLOBAL.PLAYER) {
			
			Boolean ABOVE = what.hitbox.bottom - 10 < hitbox.top;
			Boolean BELOW = what.hitbox.top + 10 > hitbox.bottom;
			Boolean RIGHT = what.hitbox.right + 10 >  hitbox.left && what.hitbox.left < hitbox.left;
			Boolean LEFT = what.hitbox.left - 10 < hitbox.right && what.hitbox.right > hitbox.right;
			
			// LEFT
			if(LEFT && !BELOW && !ABOVE) {
				coordinates.x--;
			}
	 		
			// RIGHT
			if(RIGHT && !BELOW && !ABOVE) {
				coordinates.x += 2;
			}
			
			// DOWN
	 		if(ABOVE) {
	 			coordinates.y++;
	 		}
	 		
			// UP
	 		if(BELOW) {
	 			coordinates.y--;
	 		}
			hitbox.updateHitbox(coordinates.x, coordinates.y);
		}
		
	}

	@Override
	public void reset() {

	}

}
