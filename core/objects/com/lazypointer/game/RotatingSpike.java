package com.lazypointer.game;

import java.util.Random;

public class RotatingSpike extends GameObject {

	private static final long serialVersionUID = -6335530487160304671L;

	public RotatingSpike(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
		rotate = 0;
		movement = 0;
		original_x = (int)coordinates.x;
		original_y = (int)coordinates.y;
		dir = 1;
		rotating = true;
		rand = new Random();
		speed = 1;
		vel_x = 0;
		vel_y = 0;
	}
	
	@Override
	public void init() {
	}
	
	public void setRotatingSpikeMovement(int mov, Boolean rotate, int speeed) 
	{ 
		movement = mov;			// 0 = not moving
		rotating = rotate;		// default TRUE, meaning it rotates.
		speed = speeed;
		vel_x = rand.nextDouble() + speed;
		vel_y = rand.nextDouble() + speed;
	}

	@Override
	public void logic() {

		if(movement != 0) {
			if(movement == GLOBAL.OBJECT_ROTATING_SPIKE_RANDOM)
				BounceRandomly();
			else if(movement == GLOBAL.OBJECT_ROTATING_SPIKE_VER)
				BounceVertical();
			else if(movement == GLOBAL.OBJECT_ROTATING_SPIKE_HOR)
				BounceHorizontal();		
		}

		// Rotate
		if(rotating) {
			rotate += speed;
			if(rotate >= 360 || rotate <= -360)
				rotate = 0;	
			sprites.elementAt(0).rotation = (rotate);	
		}
	}
	
	private void BounceRandomly() {
		coordinates.x = (coordinates.x + (int)vel_x);
		coordinates.y = (coordinates.y + (int)vel_y);
		hitbox.updateHitbox(coordinates.x, coordinates.y);	
	}
	
	private void BounceVertical() {
		coordinates.y = (coordinates.y + ((int)speed * dir));
		hitbox.updateHitbox(coordinates.x, coordinates.y);
	}
	
	private void BounceHorizontal() {
		coordinates.x = (coordinates.x + ((int)speed * dir));
		hitbox.updateHitbox(coordinates.x, coordinates.y);
	}
	
	private void changeDirection(CollisionInformation lastCollision) {
		
		if(movement != 0 && movement == GLOBAL.OBJECT_ROTATING_SPIKE_RANDOM) {
			
			vel_x = rand.nextDouble() + speed;
			vel_y = rand.nextDouble() + speed;
			
			if(rand.nextBoolean())
				vel_y *= -1;
			if(rand.nextBoolean())
				vel_x *= -1;
		}
		else {
			if(dir == 1)
				dir = -1;
			else
				dir = 1;
		}
		
	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {

		// Collision with the world
		if(what == null) {
			changeDirection(info);
		}
		else {
			if(what.TYPE == GLOBAL.PLAYER && GLOBAL.immortal == 0) {
				GLOBAL.playerHit(TYPE);
			}
		}
	}

	@Override
	public void reset() {
		vel_x = rand.nextDouble() + speed;
		vel_y = rand.nextDouble() + speed;
	}
	
	private double rotate;
	private int original_x;
	private int original_y;
	private int movement;
	private int dir;
	private Boolean rotating;
	private Random rand;
	private double vel_x;
	private double vel_y;

}
