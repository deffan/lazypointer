package com.lazypointer.game;

public class SkullGhost extends GameObject {

	private static final long serialVersionUID = 8431384276506472190L;

	public SkullGhost(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
		speed = 1;
	}
	
	public void setPlayerCoord(Coordinate pc) { player_coord = pc; }

	@Override
	public void logic() {
		
		double tx = player_coord.x - getMiddleX();
		double ty = player_coord.y - getMiddleY();
		double distance = Math.sqrt(tx*tx+ty*ty);
		 
		if(distance > 5) {
			coordinates.x = (coordinates.x + ((tx / distance) * speed));
			coordinates.y = (coordinates.y + ((ty / distance) * speed));
			hitbox.updateHitbox(coordinates.x, coordinates.y);
		}
		
		// If player is nearby, the ghost becomes visible!
		// 200x200 pixels counts as nearby.
		if(player_coord.x - 200 > coordinates.x || player_coord.x + 200 < coordinates.x ||
			player_coord.y - 200 > coordinates.y || player_coord.y + 200 < coordinates.y) {
			// Fade out
			if(sprites.elementAt(0).alpha > 0.01)
				sprites.elementAt(0).alpha = (sprites.elementAt(0).alpha - 0.01f);
		}
		else 
		{
			// Fade in
			if(sprites.elementAt(0).alpha < 0.99)
				sprites.elementAt(0).alpha = (sprites.elementAt(0).alpha + 0.01f);
		}
		
	}

	@Override
	public void init() {
		hitbox.width = 35;
		hitbox.height = 35;
		sprites.elementAt(0).texture_x = (805);
		sprites.elementAt(0).texture_y = (420);
		sprites.elementAt(0).size_x = (70);
		sprites.elementAt(0).size_y = (70);
		sprites.elementAt(0).relative_x = (-20);
		sprites.elementAt(0).relative_y = (-15);
	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {
		if(what != null && what.TYPE == GLOBAL.PLAYER && GLOBAL.immortal == 0) {
			GLOBAL.playerHit(TYPE);
		}
	}

	@Override
	public void reset() {
	}

	private Coordinate player_coord;
}
