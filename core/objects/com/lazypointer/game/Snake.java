package com.lazypointer.game;

import java.util.Random;

public class Snake extends GameObject {

	private static final long serialVersionUID = -935527195969694794L;

	public Snake(Coordinate new_coordinates, Hitbox new_hitbox,Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
		isHead = false;
		direction = GLOBAL.RIGHT;
		rand = new Random();
		speed = 2;
		waitTimer = new GameTimer();
		fireballTimer = new GameTimer();
		fireballTimer.setMilliSeconds((rand.nextInt(3) + 2) * 1000);
		max_distance = 0;
	}
	
	public void setParent(Snake o) { parent = o; }
	public void setData(int msec, int dist) { waitTimer.setMilliSeconds(msec); waitTimer.startTimer(); max_distance = dist; }
	public int getSnakeDirection() { return direction; }
	public void setPlayer(Player pc) { player = pc; }
	
	@Override
	public void logic() {
		
		// If BODY, then check the distance to head and pause if too close.
		if(!isHead) {
			if(parent.getSnakeDirection() == direction) {
				double xDistance = coordinates.x - parent.coordinates.x;
				double yDistance = coordinates.y - parent.coordinates.y;
				double distance = Math.sqrt(xDistance * xDistance + yDistance * yDistance);
				if (distance < max_distance)
					waitTimer.startTimer();
			}
		}
		
		// Move
		if(waitTimer.isDone()) 
		{
			// move head, logic
			if(direction == GLOBAL.RIGHT)
				coordinates.x = (coordinates.x + (int)speed);
			else if(direction == GLOBAL.LEFT)
				coordinates.x = (coordinates.x - (int)speed);
			else if(direction == GLOBAL.UP)
				coordinates.y = (coordinates.y - (int)speed);
			else
				coordinates.y = (coordinates.y + (int)speed);
			
			hitbox.updateHitbox(coordinates.x, coordinates.y);
			
			// FIRESNAKE: Create fireball event randomly and shoot towards player if close enough
			if(TYPE == GLOBAL.OBJECT_FIRESNAKE && fireballTimer.isDone()) {
				if(player.coordinates.x - 640 > coordinates.x || player.coordinates.x + 640 < coordinates.x ||
						player.coordinates.y - 640 > coordinates.y || player.coordinates.y + 640 < coordinates.y)
					return;
				else {
					event_manager.objectpool.addObject(GLOBAL.SPELL_FIREBALL, getMiddleX(), getMiddleY(), Math.atan2(player.getMiddleY() - getMiddleY(), player.getMiddleX() - getMiddleX()), TYPE);
					fireballTimer.startTimer();
				}
			}
		}

	}

	@Override
	public void init() {
		
		// If this is the head, set all parents/bodies
		if(event_objects.size() > 0) {
			
			// Set HEAD sprite
			isHead = true;
			sprites.elementAt(0).texture_x = (384);
			sprites.elementAt(0).texture_y = (704);
			
			// Set parents for all bodies
			for(int i = 0; i < event_objects.size(); i++) {
				if(i == 0) {
					((Snake) event_objects.elementAt(i)).setParent(this);
					((Snake) event_objects.elementAt(i)).setData(10, 32);
				}
				else {
					((Snake) event_objects.elementAt(i)).setParent((Snake)event_objects.elementAt(i - 1));
					((Snake) event_objects.elementAt(i)).setData(10, 32);
				}
			}
		}

	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {
		
		if(what == null) {
			
			// On collision, head decides the direction for all bodies too
			if(isHead) {
				int newDir = direction;
				while(newDir == direction) {
					direction = rand.nextInt(4);
				}
			}
			else {
				
				// Body direction 
				direction = parent.getSnakeDirection();
				
				// The head hitbox may be on a different x/y position depending on how the collision happened, so adjust.
				if((direction == GLOBAL.LEFT || direction == GLOBAL.RIGHT) && parent.hitbox.top != hitbox.top)
					coordinates.y = (parent.hitbox.top);
				else if((direction == GLOBAL.UP || direction == GLOBAL.DOWN) && parent.hitbox.left != hitbox.left)
					coordinates.x = (parent.hitbox.left);
			}
		}
		else if(what.TYPE == GLOBAL.PLAYER && GLOBAL.immortal == 0) {
			GLOBAL.playerHit(TYPE);
		}

	}

	@Override
	public void reset() {

	}
	private Boolean isHead;
	private Snake parent;
	private int direction;
	private Random rand;
	private GameTimer waitTimer;
	private GameTimer fireballTimer;
	private Player player;
	private double max_distance;
}
