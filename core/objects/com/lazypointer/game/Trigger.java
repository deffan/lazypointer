package com.lazypointer.game;

public class Trigger extends GameObject {

	private static final long serialVersionUID = 3949940162779024607L;

	public Trigger(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
		active = true;
		visual = false;
	}
	
	@Override
	public void init() {
		
		if(TYPE == GLOBAL.OBJECT_TRIGGER_VISUAL)
			visual = true;
	}
	
	@Override
	public void logic() {
		checkEvents();
	}
	
	private void checkEvents() {

		if(event_listener.event_queue.size() > 0) {
			for(int i = 0; i < event_listener.event_queue.size(); i++) {
				
				if(event_listener.event_queue.elementAt(i).action == GLOBAL.OBJECT_TRIGGER_ACTIVATOR)
				{
					reset();
				}
			}
			event_listener.event_queue.clear();
		}
	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {
		
		if(what != null && active && what.TYPE == GLOBAL.PLAYER) {
			
			// Send "trigger" action to all objects that are to be affected by this trigger
			if(TYPE == GLOBAL.OBJECT_TRIGGER_VISUAL || TYPE == GLOBAL.OBJECT_TRIGGER || TYPE == GLOBAL.OBJECT_TRIGGER_ACTIVATOR) {
				Event ev = new Event();
				ev.action = TYPE;
				for(int i = 0; i < event_objects.size(); i++) {
					ev.ids.add(event_objects.elementAt(i).ID);
				}
				event_manager.addEvent(ev);
			}
			else if(TYPE == GLOBAL.OBJECT_TRIGGER_ADDER || TYPE == GLOBAL.OBJECT_TRIGGER_REMOVER) {
				// Send "trigger" to add or remove tiles where the objects are specified.
				Event ev = new Event();
				ev.action = TYPE;
				ev.ids.add("GENERATOR");
				ev.objects.addAll(event_objects);
				event_manager.addEvent(ev);
			}
			active = false;
			
			// If this trigger is visual, change the texture by +1 tilesize on the x-axis
			if(visual)
				sprites.elementAt(0).texture_x = (sprites.elementAt(0).texture_x + GLOBAL.TILE_SIZE);
		}
	}

	@Override
	public void reset() {
		
		if(!active) {
			if(visual)
				sprites.elementAt(0).texture_x = (sprites.elementAt(0).texture_x - GLOBAL.TILE_SIZE);
			active = true;
		}
	}
	
	private Boolean active;
	private Boolean visual;
}
