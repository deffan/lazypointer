package com.lazypointer.game;

import java.util.Random;

public class Virus extends GameObject {

	private static final long serialVersionUID = -5467743866095702326L;

	public Virus(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
		idleTimer = new GameTimer();
		fadeTimer = new GameTimer();
		randTextureTimer = new GameTimer();
		idleTimer.setMilliSeconds(4000);
		fadeTimer.setMilliSeconds(2000);
		randTextureTimer.setMilliSeconds(500);
		idleTimer.startTimer();
		rand = new Random();
		newx = 0;
		newy = 0;
		idle = true;
		fadeOut = false;
		fadeIn = false;
		moving = false;
	}
	
	public void setWorld(Level l) { level = l; }

	@Override
	public void logic() {

		if(idle) {
			
			// randomize texture of virus 0-5
			if(randTextureTimer.isDone()) {
				sprites.elementAt(0).texture_x = (rand.nextInt(6) * GLOBAL.TILE_SIZE);
				randTextureTimer.startTimer();
			}

			// If idle is done, start moving process
			if(idleTimer.isDone()) {
				idle = false;
				moving = true;
			}
		}
		else {

			// Prepare to move
			if(moving) 
			{
				int tries = 0;
				while(true) {

					// Randomize new coordinates (1-6)
					int randx = rand.nextInt(5) + 1;
					int randy = rand.nextInt(5) + 1;
					
					// Sometimes they are negative
					if(rand.nextBoolean())
						randx *= -1;
					if(rand.nextBoolean())
						randy *= -1;
					
					// Add current coordinates (divided by tilesize to get element numbers)
					// Which means new coordinates will be plus/minus 1-6 coordinates from the current ones
					randx += (coordinates.x / GLOBAL.TILE_SIZE);
					randy += (coordinates.y / GLOBAL.TILE_SIZE);
					
					// Check that these numbers are within the world
					if(randx > 0 && randx < level.world_size_x && randy > 0 && randy < level.world_size_y) 
					{
						// We cant go to a collidable position
						if(!level.world[randx][randy].isCollidable()) 
						{
							newx = randx * GLOBAL.TILE_SIZE;
							newy = randy * GLOBAL.TILE_SIZE;
							moving = false;
							fadeOut = true;
							fadeTimer.startTimer();
							
							// Send event
							Event ev = new Event();
							ev.action = GLOBAL.ACTION_PITFADE;
							ev.ids.add("GENERATOR");
							ev.string_data.add(Integer.toString((int)coordinates.x));
							ev.string_data.add(Integer.toString((int)coordinates.y));
							event_manager.addEvent(ev);
							
							break;
						}
					}
					// If for some reason we within 10 tries fail to find new coordinates, cancel.
					tries++;
					if(tries > 10) {
						
						// Remove this object
						Event ev = new Event();
						ev.action = GLOBAL.ACTION_REMOVEME;
						ev.objects.add(this);
						ev.ids.add("GENERATOR");
						event_manager.addEvent(ev);
						break;
					}
						
				}
			}
			else if(fadeOut) {
				
				// If done, fadeIn
				if(fadeTimer.isDone()) {
					fadeOut = false;
					fadeIn = true;
					fadeTimer.startTimer();
	
					// Move now!
					coordinates.x = (newx);
					coordinates.y = (newy);
					hitbox.updateHitbox(newx, newy);
				}
				
				// shrink texture Y size
				if(sprites.elementAt(0).size_y > 0)
					sprites.elementAt(0).size_y = (sprites.elementAt(0).size_y - 1);
				
			}
			else if(fadeIn) {
				
				// If done, go Idle
				if(fadeTimer.isDone()) {
					fadeIn = false;
					idle = true;
					idleTimer.startTimer();
					
				}
				
				// increase texture Y size
				if(sprites.elementAt(0).size_y < GLOBAL.TILE_SIZE)
					sprites.elementAt(0).size_y = (sprites.elementAt(0).size_y + 1);
			}
		}

	}

	@Override
	public void init() {
		sprites.elementAt(0).texture_x = (0);
		sprites.elementAt(0).texture_y = (960);

	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {
		
	}

	@Override
	public void reset() {

	}

	private Level level;
	private GameTimer idleTimer;
	private GameTimer fadeTimer;
	private GameTimer randTextureTimer;
	private Random rand;
	private int newx;
	private int newy;
	private Boolean idle;
	private Boolean fadeOut;
	private Boolean fadeIn;
	private Boolean moving;
}
