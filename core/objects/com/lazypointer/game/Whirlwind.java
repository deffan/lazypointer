package com.lazypointer.game;

import java.util.Random;
import java.util.Vector;

public class Whirlwind extends GameObject {

	private static final long serialVersionUID = 2392002374613086626L;

	public Whirlwind(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);

		fader = new Vector<Boolean>();
		
		fader.add(true);
		fader.add(true);
		fader.add(true);
		fader.add(true);
		fader.add(true);
		
		sprites.add(new Sprite(0, 0, 0, 0));
		sprites.add(new Sprite(0, 0, 0, 0));
		sprites.add(new Sprite(0, 0, 0, 0));
		sprites.add(new Sprite(0, 0, 0, 0));
		
		rand = new Random();
		
		speed = 1;
		vel_x = speed;
		vel_y = speed;
	}

	@Override
	public void logic() {
		
		// Fade in / out constantly
		for(int i = 0; i < fader.size(); i++) {
			if(fader.elementAt(i)) 
			{
				sprites.elementAt(i).alpha = (sprites.elementAt(i).alpha + 0.01f);
				if(sprites.elementAt(i).alpha >= 0.7)
					fader.setElementAt(false, i);
			}
			else
			{
				sprites.elementAt(i).alpha = (sprites.elementAt(i).alpha - 0.01f);
				if(sprites.elementAt(i).alpha <= 0.1)
					fader.setElementAt(true, i);
			}
			
			// Rotate
			if(i >= (fader.size() / 2))
				sprites.elementAt(i).rotation = (sprites.elementAt(i).rotation + (rand.nextInt(3) + 1));
			else
				sprites.elementAt(i).rotation = (sprites.elementAt(i).rotation - (rand.nextInt(3) + 1));
				
			if(sprites.elementAt(i).rotation >= 360 || sprites.elementAt(i).rotation <= -360)
				sprites.elementAt(i).rotation = (0);
			
		}
		coordinates.x = (coordinates.x + (int)vel_x);
		coordinates.y = (coordinates.y + (int)vel_y);
		hitbox.updateHitbox(coordinates.x, coordinates.y);	
	}

	@Override
	public void init() {
		
		// Set default coordinates
		sprites.elementAt(0).texture_y = (1220);
		sprites.elementAt(0).size_x = (120);
		sprites.elementAt(0).size_y = (120);
		sprites.elementAt(0).relative_x = (-25);
		sprites.elementAt(0).relative_y = (-25);
		
		sprites.elementAt(1).texture_y = (sprites.elementAt(0).texture_y);
		sprites.elementAt(2).texture_y = (sprites.elementAt(0).texture_y);
		sprites.elementAt(3).texture_y = (sprites.elementAt(0).texture_y);
		sprites.elementAt(4).texture_y = (sprites.elementAt(0).texture_y);
		
		sprites.elementAt(1).size_x = (sprites.elementAt(0).size_x);
		sprites.elementAt(1).size_y = (sprites.elementAt(0).size_y);
		sprites.elementAt(2).size_x = (sprites.elementAt(0).size_x);
		sprites.elementAt(2).size_y = (sprites.elementAt(0).size_y);
		sprites.elementAt(3).size_x = (sprites.elementAt(0).size_x);
		sprites.elementAt(3).size_y = (sprites.elementAt(0).size_y);
		sprites.elementAt(4).size_x = (sprites.elementAt(0).size_x);
		sprites.elementAt(4).size_y = (sprites.elementAt(0).size_y);
		
		sprites.elementAt(1).texture_x = (sprites.elementAt(0).texture_x + 120);
		sprites.elementAt(2).texture_x = (sprites.elementAt(1).texture_x + 120);
		sprites.elementAt(3).texture_x = (sprites.elementAt(0).texture_x);
		sprites.elementAt(4).texture_x = (sprites.elementAt(0).texture_x + 120);
		
		sprites.elementAt(1).alpha = (0.45f);
		sprites.elementAt(2).alpha = (0.20f);
		sprites.elementAt(3).alpha = (0.15f);
		sprites.elementAt(4).alpha = (0.70f);
		
		sprites.elementAt(1).rotation = (33);
		sprites.elementAt(2).rotation = (128);
		sprites.elementAt(3).rotation = (300);
		sprites.elementAt(4).rotation = (220);
		
		sprites.elementAt(1).scale_x = (1.2f);
		sprites.elementAt(1).scale_y = (1.2f);
		sprites.elementAt(2).scale_x = (1.3f);
		sprites.elementAt(2).scale_y = (1.3f);
		sprites.elementAt(3).scale_x = (0.9f);
		sprites.elementAt(3).scale_y = (0.9f);
		sprites.elementAt(4).scale_x = (0.8f);
		sprites.elementAt(4).scale_y = (0.8f);
		
		sprites.elementAt(1).relative_x = (sprites.elementAt(0).relative_x);
		sprites.elementAt(1).relative_y = (sprites.elementAt(0).relative_y);
		sprites.elementAt(2).relative_x = (sprites.elementAt(0).relative_x);
		sprites.elementAt(2).relative_y = (sprites.elementAt(0).relative_y);
		sprites.elementAt(3).relative_x = (sprites.elementAt(0).relative_x);
		sprites.elementAt(3).relative_y = (sprites.elementAt(0).relative_y);
		sprites.elementAt(4).relative_x = (sprites.elementAt(0).relative_x);
		sprites.elementAt(4).relative_y = (sprites.elementAt(0).relative_y);

	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {
		
		if(what == null) {
			vel_x = rand.nextInt(1) + speed;
			vel_y = rand.nextInt(1) + speed;
			
			if(rand.nextBoolean())
				vel_y *= -1;
			if(rand.nextBoolean())
				vel_x *= -1;
		}
		else if(what != null && what.TYPE == GLOBAL.PLAYER && GLOBAL.immortal == 0) {
			GLOBAL.playerHit(TYPE);
		}
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub

	}
	
	private Vector<Boolean> fader;
	private Random rand;
	private double vel_x;
	private double vel_y;
}
