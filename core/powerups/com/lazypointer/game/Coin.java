package com.lazypointer.game;

public class Coin extends Powerup {

	private static final long serialVersionUID = -4308377262093894755L;

	public Coin(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
		picked = false;
		active = true;
		fade = false;
		animationTimer = new GameTimer();
		animationTimer.setMilliSeconds(25);
		move_y = 0;
		frame = 0;
		animate = true;
		sprites.add(new Sprite(0, 0, 1, 0));
	}

	@Override
	public void logic() {
		
			if(active && picked) {
				
				if(animationTimer.isDone()) 
				{
					if(animate) {
						frame++;
						if(frame > 3) {
							animate = false;
							sprites.elementAt(1).alpha = 0;
						}
						sprites.elementAt(1).texture_x = frame * GLOBAL.TILE_SIZE;
					}
					
					// Move 50 pixels up
					if(!fade) {
						move_y++;
						coordinates.y--;
						if(move_y > 50) {
							fade = true;
							move_y = 0;
						}	
					}
					else 
					{	// Fade out
						if(sprites.elementAt(0).alpha > 0.1)
							sprites.elementAt(0).alpha = (sprites.elementAt(0).alpha - 0.1f);
						else {
							sprites.elementAt(0).alpha = 0;
							active = false;
						}
					}
					animationTimer.startTimer();
				}
			}
	}

	@Override
	public void init() {
		sprites.elementAt(0).texture_x = 0;
		sprites.elementAt(0).texture_y = 192;
		sprites.elementAt(0).relative_x = -20;
		sprites.elementAt(0).relative_y = -20;
		sprites.elementAt(1).relative_x = -20;
		sprites.elementAt(1).relative_y = -20;
		hitbox.width = 25;
		hitbox.height = 25;
		hitbox.updateHitbox(coordinates.x, coordinates.y);
		
	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {
		if(active && !picked) {
			animationTimer.startTimer();
			picked = true;
			GLOBAL.coins++;
			GLOBAL.updateCoinString();
			sprites.elementAt(0).texture_x = 0;
			sprites.elementAt(0).texture_y = 0;
			sprites.elementAt(0).size_x = 0;
			sprites.elementAt(0).size_y = 0;
			sprites.elementAt(1).texture_x = 0;
			sprites.elementAt(1).texture_y = 1086;
			if(GLOBAL.soundActive)
				GLOBAL.sound_coin.play(GLOBAL.sound);
		}
	}

	@Override
	public void reset() {
		picked = false;
		active = true;
		fade = false;
		sprites.elementAt(0).texture_x = 0;
		sprites.elementAt(0).texture_y = 192;
		sprites.elementAt(0).size_x = GLOBAL.TILE_SIZE;
		sprites.elementAt(0).size_y = GLOBAL.TILE_SIZE;
		sprites.elementAt(0).alpha = 1;
		move_y = 0;
		frame = 0;
		sprites.elementAt(1).texture_x = 0;
		sprites.elementAt(1).texture_y = 0;
		sprites.elementAt(1).alpha = 1;
		sprites.elementAt(0).relative_x = -20;
		sprites.elementAt(0).relative_y = -20;
		animate = true;
	}
	
	public Boolean picked;
	public Boolean fade;
	public Boolean animate;
	public GameTimer animationTimer;
	public int move_y;
	public int frame;
}
