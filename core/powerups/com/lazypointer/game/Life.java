package com.lazypointer.game;

import java.util.Random;

public class Life extends Powerup {

	private static final long serialVersionUID = -6260362094023040819L;

	public Life(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
		
		sprites.add(new Sprite(0,0,1,0));
		sprites.add(new Sprite(0,0,1,0));
		sprites.add(new Sprite(0,0,1,0));
		sprites.add(new Sprite(0,0,1,0));
		sprites.add(new Sprite(0,0,1,0));
		sprites.add(new Sprite(0,0,1,0));
		endPos = new int[6];
		flowTimer = new GameTimer();
		flowTimer.setMilliSeconds(25);
		rand = new Random();
		picked = false;
		active = true;
		animate = false;
		frame = 0;
		once = 0;
	}

	@Override
	public void logic() {
		
		// If picked:
		// DONT reset relative_y, just fade out and when all are alpha == 0, set inactive.
		if(active) {
			if(flowTimer.isDone()) {
				
				if(animate) {
					frame++;
					if(frame > 3) {
						animate = false;
						sprites.elementAt(6).alpha = 0;
					}
					sprites.elementAt(6).texture_x = 256 + (frame * GLOBAL.TILE_SIZE);
				}
				
				for(int i = 0; i < sprites.size() - 1; i++) {
					
					// Move up
					sprites.elementAt(i).relative_y--;
					if(sprites.elementAt(i).relative_y < endPos[i]) // Once reached top position...
					{
						// Fade out & Reset when faded
						sprites.elementAt(i).alpha -= 0.05;
						if(sprites.elementAt(i).alpha <= 0.05) 
						{
							if(picked) {
								sprites.elementAt(i).alpha = 0;
							}
							else {
								sprites.elementAt(i).alpha = 0.5f;
								sprites.elementAt(i).relative_y = 0;
								sprites.elementAt(i).relative_x = rand.nextInt(15);	
							}
						}
						
						// Check if all sprites have faded out before setting inactive
						if(picked) {
							Boolean all_done = true;
							for(int n = 0; n < sprites.size() - 1; n++) {
								if(sprites.elementAt(n).alpha != 0) {
									all_done = false;
									break;
								}
							}
							if(all_done) {
								active = false;
								return;
							}
								
						}
					}
				}
				flowTimer.startTimer();
			}
		}
	}

	@Override
	public void init() {
		
		for(int i = 0; i < sprites.size() - 1; i++) {
			sprites.elementAt(i).size_x = 20;
			sprites.elementAt(i).size_y = 20;
			sprites.elementAt(i).relative_x = rand.nextInt(15);
			sprites.elementAt(i).alpha = 0.5f;
		}
		
		sprites.elementAt(0).texture_x = 1856;
		sprites.elementAt(0).texture_y = 704;
		
		sprites.elementAt(1).texture_x = 1876;
		sprites.elementAt(1).texture_y = 704;
		
		sprites.elementAt(2).texture_x = 1896;
		sprites.elementAt(2).texture_y = 704;
		
		sprites.elementAt(3).texture_x = 1876;
		sprites.elementAt(3).texture_y = 704;
		
		sprites.elementAt(4).texture_x = 196;
		sprites.elementAt(4).texture_y = 704;
		
		sprites.elementAt(5).texture_x = 1876;
		sprites.elementAt(5).texture_y = 704;
		
		endPos[0] = -50;
		endPos[1] = -37;
		endPos[2] = -60;
		endPos[3] = -35;
		endPos[4] = -40;
		endPos[5] = -55;

		// Main sprite
		sprites.elementAt(6).texture_x = 128;
		sprites.elementAt(6).texture_y = 192;
		sprites.elementAt(6).relative_x = -15;
		sprites.elementAt(6).relative_y = -20;
		
		hitbox.width = 25;
		hitbox.height = 25;
		hitbox.updateHitbox(coordinates.x, coordinates.y);
	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {
		if(what.TYPE == GLOBAL.PLAYER && !picked && active) {
			GLOBAL.life++;
			GLOBAL.updateLifeString();
			picked = true;
			animate = true;
			sprites.elementAt(6).texture_x = 256;
			sprites.elementAt(6).texture_y = 1085;
			if(GLOBAL.soundActive)
				GLOBAL.sound_life.play(GLOBAL.sound);
		}
	}

	@Override
	public void reset() {
		for(int i = 0; i < sprites.size() - 1; i++) {
			sprites.elementAt(i).relative_x = rand.nextInt(15);
			sprites.elementAt(i).alpha = 0.5f;
			sprites.elementAt(i).relative_y = 0;
		}
		sprites.elementAt(6).texture_x = 128;
		sprites.elementAt(6).texture_y = 192;
		active = true;
		picked = false;
		animate = false;
	}

	private int endPos[];
	private GameTimer flowTimer;
	private Random rand;
	private Boolean picked;
	private Boolean animate;
	private int frame;
	public int once;
}
