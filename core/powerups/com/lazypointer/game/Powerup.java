package com.lazypointer.game;
import com.lazypointer.game.Coordinate;
import com.lazypointer.game.GameObject;
import com.lazypointer.game.Hitbox;
import com.lazypointer.game.Sprite;


public abstract class Powerup extends GameObject {

	private static final long serialVersionUID = -5330018990279650484L;

	public Powerup(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
		active = true;
	}

	public Boolean active;
}
