package com.lazypointer.game;

public class Time extends Powerup {

	private static final long serialVersionUID = -2896553005610153154L;

	public Time(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
		picked = false;
		active = true;
		fade = false;
		animationTimer = new GameTimer();
		animationTimer.setMilliSeconds(25);
		move_y = 0;
		frame = 0;
		my_time = 0;
		tpos = 0;
		animate = true;
		sprites.add(new Sprite(0, 0, 1, 0));
	}

	@Override
	public void logic() {
		
		if(active && picked) {
			
			if(animationTimer.isDone()) 
			{
				if(animate) {
					frame++;
					if(frame > 3) {
						animate = false;
						sprites.elementAt(1).alpha = 0;
					}
					sprites.elementAt(1).texture_x = frame * GLOBAL.TILE_SIZE;
				}
				
				// Move 50 pixels up
				if(!fade) {
					move_y++;
					coordinates.y--;
					if(move_y > 50) {
						fade = true;
						move_y = 0;
					}	
				}
				else 
				{	// Fade out
					if(sprites.elementAt(0).alpha > 0.1)
						sprites.elementAt(0).alpha = (sprites.elementAt(0).alpha - 0.1f);
					else {
						sprites.elementAt(0).alpha = 0;
						active = false;
					}
				}
				animationTimer.startTimer();
			}
		}
	}

	@Override
	public void init() {
		sprites.elementAt(0).texture_x = 64;
		sprites.elementAt(0).texture_y = 192;
		
		sprites.elementAt(0).relative_x = -20;
		sprites.elementAt(0).relative_y = -20;
		sprites.elementAt(1).relative_x = -20;
		sprites.elementAt(1).relative_y = -20;
		
		hitbox.width = 25;
		hitbox.height = 25;
		hitbox.updateHitbox(coordinates.x, coordinates.y);
		
		// Set time
		switch(TYPE) 
		{
			case GLOBAL.OBJECT_POWERUP_TIME_10: my_time = 10; tpos = 1; break;
			case GLOBAL.OBJECT_POWERUP_TIME_20: my_time = 20; tpos = 2; break;
			case GLOBAL.OBJECT_POWERUP_TIME_30: my_time = 30; tpos = 3; break;
			case GLOBAL.OBJECT_POWERUP_TIME_40: my_time = 40; tpos = 4; break;
			case GLOBAL.OBJECT_POWERUP_TIME_50: my_time = 50; tpos = 5; break;
			case GLOBAL.OBJECT_POWERUP_TIME_60: my_time = 60; tpos = 6; break;
			case GLOBAL.OBJECT_POWERUP_TIME_120: my_time = 120; tpos = 7; break;
			case GLOBAL.OBJECT_POWERUP_TIME_180: my_time = 180; tpos = 8; break;
			default: my_time = 10;
		}

	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {
		if(active && !picked) {
			animationTimer.startTimer();
			picked = true;
			GLOBAL.time += my_time;
			sprites.elementAt(0).texture_x = 1960;		
			sprites.elementAt(0).texture_y = 709 + (tpos * 31);
			sprites.elementAt(0).size_x = 87;
			sprites.elementAt(0).size_y = 28;
			sprites.elementAt(0).relative_x = -30;
			sprites.elementAt(1).texture_x = 0;
			sprites.elementAt(1).texture_y = 1150;
			if(GLOBAL.soundActive)
				GLOBAL.sound_time.play(GLOBAL.sound);
		}
	}

	@Override
	public void reset() {
		picked = false;
		active = true;
		fade = false;
		sprites.elementAt(0).texture_x = 64;
		sprites.elementAt(0).texture_y = 192;
		sprites.elementAt(0).size_x = GLOBAL.TILE_SIZE;
		sprites.elementAt(0).size_y = GLOBAL.TILE_SIZE;
		sprites.elementAt(0).alpha = 1;
		move_y = 0;
		frame = 0;
		sprites.elementAt(1).texture_x = 0;
		sprites.elementAt(1).texture_y = 0;
		sprites.elementAt(1).alpha = 1;
		animate = true;
	}
	
	public Boolean picked;
	public Boolean fade;
	public Boolean animate;
	public GameTimer animationTimer;
	public int move_y;
	public int frame;
	public int my_time;
	public int tpos;
}
