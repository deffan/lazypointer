package com.lazypointer.game;

import java.util.Vector;

public class FireExplosion extends Spell {

	private static final long serialVersionUID = 1L;

	public FireExplosion(Coordinate new_coordinates, Hitbox new_hitbox,
			Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
		
		// Create particles
		Vector<Particle> p = new Vector<Particle>(15);
		for(int i = 0; i < 15; i++) {
			p.add(new FireExplosionParticle((int)coordinates.x, (int)coordinates.y));
		}
		emitter = new ParticleEmitter(p);
		emitter.repeat = false;

		fadeTimer = new GameTimer();
		fadeTimer.setMilliSeconds(300);
		fadeTimer.startTimer();
	}
	
	@Override
	public void init() {
		
	}

	@Override
	public void logic() {
		
		if(alive) {
			emitter.update((int)coordinates.x, (int)coordinates.y);
			if(fadeTimer.isDone())
				setDead();
		}
	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {
	}

	@Override
	public void reset() {
		emitter.resetAll((int)point_x, (int)point_y);
		alive = true;
		fadeTimer.startTimer();
	}
	
	private GameTimer fadeTimer;
	
}
