package com.lazypointer.game;

public class FireExplosionParticle extends Particle {

	private static final long serialVersionUID = -6121000833412955305L;

	FireExplosionParticle(int xpos, int ypos) {
		sprite.texture_x = (210 + (rand.nextInt(1) * 65));
		sprite.texture_y = (720);
		
		life.setMilliSeconds(rand.nextInt(300) + 100);
		sprite.alpha = (1);
		life.startTimer();
		alive = true;
		
		x = xpos;
		y = ypos;
		angle = Math.atan2(0, (xpos - 10) - ypos) + Math.toRadians(rand.nextInt(360));
	}
	
	@Override
	public void update() {
		
		if(life.isDone()) {
			alive = false;
			sprite.alpha = (0);
		}
		else {
			if(sprite.alpha > 0.1)
				sprite.alpha = (sprite.alpha - 0.1f);
			
			double vx = 1.3*Math.cos(angle);
			double vy = 1.3*Math.sin(angle);
			double dx = vx*1.3;
			double dy = vy*1.3;
			x += dx; 
			y += dy;
		}
	}

	@Override
	public void reset(int xpos, int ypos) {
		x = xpos;
		y = ypos;
		life.setMilliSeconds(rand.nextInt(300) + 100);
		sprite.texture_x = (210 + (rand.nextInt(1) * 65));
		angle = Math.atan2(0, (xpos - 10) - ypos) + Math.toRadians(rand.nextInt(360));
		sprite.alpha = 1;
		life.startTimer();
		alive = true;
	}
	
	private double angle;

}
