package com.lazypointer.game;

import java.util.Vector;

public class Fireball extends Spell {

	private static final long serialVersionUID = 8376326280724543634L;

	public Fireball(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
		
		// Create particles
		Vector<Particle> p = new Vector<Particle>(25);
		for(int i = 0; i < 25; i++)
			p.add(new FireballParticle());
		
		emitter = new ParticleEmitter(p);
	}
	
	@Override
	public void init() {
	}

	@Override
	public void logic() {
		
		if(alive) {
			point_x += (speed*Math.cos(angle))*speed;
			point_y += (speed*Math.sin(angle))*speed;

			coordinates.x = ((int)point_x);
			coordinates.y = ((int)point_y);
			hitbox.updateHitbox(coordinates.x, coordinates.y);
			
			emitter.update((int)point_x, (int)point_y);
		}
	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {
		
		if(what == null && alive) 
		{
			event_manager.objectpool.addObject(GLOBAL.SPELL_FIRE_EXPLOSION, getMiddleX(), getMiddleY(), 0, 0);
			alive = false;
		}
		else if(what != null && what.TYPE == GLOBAL.PLAYER && alive) {
			GLOBAL.playerHit(TYPE);
			alive = false;
			event_manager.objectpool.addObject(GLOBAL.SPELL_FIRE_EXPLOSION, getMiddleX(), getMiddleY(), 0, 0);
		}
	}

	@Override
	public void reset() {
		alive = true;
		emitter.resetAll((int)point_x, (int)point_y);
	}
	
}
