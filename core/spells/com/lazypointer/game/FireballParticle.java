package com.lazypointer.game;

public class FireballParticle extends Particle {
	
	private static final long serialVersionUID = -63528441092033485L;

	FireballParticle() {
		sprite.texture_x = (210);
		sprite.texture_y = (720);
	}

	public void update() {
		
		if(life.isDone()) {
			alive = false;
			sprite.alpha = (0);
		}
		else {
			sprite.alpha = (sprite.alpha - 0.01f);
		}
	}
	
	public void reset(int xpos, int ypos) {

		life.setMilliSeconds(rand.nextInt(150) + 100);
		sprite.alpha = (1);
		life.startTimer();
		alive = true;
		
		int xpositive = rand.nextInt(2);
		int ypositive = rand.nextInt(2);
		
		x = xpos;
		y = ypos;
		
		if(xpositive == 0)
			x = xpos + rand.nextInt(5);
		else
			x = xpos - rand.nextInt(5);
		
		if(ypositive == 0)
			y = ypos + rand.nextInt(5);
		else
			y = ypos - rand.nextInt(5);
	}
	
}
