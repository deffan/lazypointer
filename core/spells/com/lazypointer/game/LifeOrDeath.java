package com.lazypointer.game;

public class LifeOrDeath extends Spell {

	private static final long serialVersionUID = -9028041828747213013L;

	public LifeOrDeath(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);

		animationTimer = new GameTimer();
		animationTimer.setMilliSeconds(25);
		active = true;
		fade = false;
		move_y = 0;
	}

	@Override
	public void logic() {
		
		if(active) {
			if(animationTimer.isDone()) 
			{
				// Move 50 pixels up
				if(!fade) {
					move_y++;
					coordinates.y--;
					if(move_y > 50) {
						fade = true;
						move_y = 0;
					}	
				}
				else 
				{	// Fade out
					if(sprites.elementAt(0).alpha > 0.1)
						sprites.elementAt(0).alpha = (sprites.elementAt(0).alpha - 0.1f);
					else {
						sprites.elementAt(0).alpha = 0;
						active = false;
						alive = false;
					}
				}
				animationTimer.startTimer();
			}
		}

	}

	@Override
	public void init() {
		sprites.elementAt(0).size_x = 50;
		sprites.elementAt(0).size_y = 30;
	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {

	}
	
	public void reset() {
		active = true;
		fade = false;
		alive = true;
		move_y = 0;
		sprites.elementAt(0).alpha = 1;
		sprites.elementAt(0).relative_x = -15;
	}

	private GameTimer animationTimer;
	private Boolean active;
	private Boolean fade;
	private int move_y;
}
