package com.lazypointer.game;


import com.lazypointer.game.CollisionInformation;
import com.lazypointer.game.Coordinate;
import com.lazypointer.game.GLOBAL;
import com.lazypointer.game.GameObject;
import com.lazypointer.game.GameTimer;
import com.lazypointer.game.Hitbox;
import com.lazypointer.game.Sprite;

public class MovingFloorAnimation extends GameObject {

	private static final long serialVersionUID = -3774727474706563769L;
	public MovingFloorAnimation(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
		floorTimer = new GameTimer();
		floorTimer.setMilliSeconds(50);
		x_mov = 0;
		y_mov = 0;
	}

	@Override
	public void logic() {
		if(floorTimer.isDone()) {
			
			if(TYPE == GLOBAL.OBJECT_MOVINGFLOOR_RIGHT_A) {
				x_mov--;
				sprites.elementAt(0).texture_x--;
				if(x_mov == -63) {
					x_mov = 0;
					sprites.elementAt(0).texture_x = 64;
				}
			}
			else if(TYPE == GLOBAL.OBJECT_MOVINGFLOOR_LEFT_A) {
				x_mov++;
				sprites.elementAt(0).texture_x++;
				if(x_mov == 64) {
					x_mov = 0;
					sprites.elementAt(0).texture_x = 0;
				}
				
			}	
			else if(TYPE == GLOBAL.OBJECT_MOVINGFLOOR_UP_A) {
				y_mov++;
				sprites.elementAt(0).texture_y++;
				if(y_mov == 63) {
					y_mov = 0;
					sprites.elementAt(0).texture_y = 384;
				}
			}
			else if(TYPE == GLOBAL.OBJECT_MOVINGFLOOR_DOWN_A){
				y_mov--;
				sprites.elementAt(0).texture_y--;
				if(y_mov == -63) {
					y_mov = 0;
					sprites.elementAt(0).texture_y = 448;
				}
			}
			floorTimer.startTimer();
		}
	}

	@Override
	public void init() {
		
		if(TYPE == GLOBAL.OBJECT_MOVINGFLOOR_RIGHT_A) {
			sprites.elementAt(0).texture_x = 64;
			sprites.elementAt(0).texture_y = 448;
		}
		else if(TYPE == GLOBAL.OBJECT_MOVINGFLOOR_LEFT_A) {
			sprites.elementAt(0).texture_x = 0;
			sprites.elementAt(0).texture_y = 448;
		}	
		else if(TYPE == GLOBAL.OBJECT_MOVINGFLOOR_UP_A) {
			sprites.elementAt(0).texture_y = 384;
			sprites.elementAt(0).texture_x = 0;
		}
		else {
			sprites.elementAt(0).texture_x = 0;
			sprites.elementAt(0).texture_y = 448;
		}
	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {
	}

	@Override
	public void reset() {
	}

	private int x_mov;
	private int y_mov;
	private GameTimer floorTimer;

}
