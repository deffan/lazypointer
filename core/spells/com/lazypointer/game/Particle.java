package com.lazypointer.game;

import java.io.Serializable;
import java.util.Random;


public abstract class Particle implements Serializable {
	
	private static final long serialVersionUID = 6435973957799286818L;
	
	Particle() {
		sprite = new Sprite(0, 0, 1, GLOBAL.NORMAL_LAYER);
		life = new GameTimer();
		alive = false;			// Particle always starts off being DEAD
		x = 0;
		y = 0;
		particle_size_x = 35;
		particle_size_y = 35;
		rand = new Random();
		life.setMilliSeconds(rand.nextInt(120));
		life.startTimer();
	}
	
	public abstract void update();
	public abstract void reset(int xpos, int ypos);
	
	public Boolean getAlive() { return alive; }
	public int getX() { return x; }
	public int getY() { return y; }
	public Sprite getSprite() { return sprite; }
	public int getParticleSizeX() { return particle_size_x; }
	public int getParticleSizeY() { return particle_size_y; }

	protected Sprite sprite;
	protected GameTimer life;
	protected Boolean alive;
	protected int x;
	protected int y;
	protected int particle_size_x;
	protected int particle_size_y;
	protected Random rand;
	
}
