package com.lazypointer.game;

import java.io.Serializable;
import java.util.Vector;

public class ParticleEmitter implements Serializable {

	private static final long serialVersionUID = -3699619394240751682L;
	
	ParticleEmitter(Vector<Particle> p) {
		particles = new Vector<Particle>();
		particles = p;
		repeat = true;
	}
	
	public void update(int x, int y) {
		for(int i = 0; i < particles.size(); i++) {
			if(particles.get(i).getAlive()) 
				particles.get(i).update();
			else {
				if(repeat)
					particles.get(i).reset(x, y);
			}
				
		}
	}
	
	public void resetAll(int x, int y) {
		for(int i = 0; i < particles.size(); i++) {
			particles.get(i).reset(x, y);
		}	
	}
	
	Vector<Particle> getParticles() { return particles; }
	private Vector<Particle> particles;
	public Boolean repeat;
}
