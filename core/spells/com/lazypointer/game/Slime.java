package com.lazypointer.game;

import java.util.Random;

public class Slime extends Spell {

	private static final long serialVersionUID = -2787964154625573651L;

	public Slime(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
		timer = new GameTimer();
		timer.setMilliSeconds(7000);
		timer.startTimer();
		fade = false;
		alive = false;
		death_delay.setMilliSeconds(0);
	}

	@Override
	public void logic() {
		
		if(alive) {
			if(timer.isDone()) {
				fade = true;
				timer.startTimer();
			}
				
			if(fade) {
				if(sprites.elementAt(0).alpha > 0.01)
					sprites.elementAt(0).alpha = (sprites.elementAt(0).alpha - 0.01f);
				else
					alive = false;
			}	
		}
	}

	@Override
	public void init() {
		
		Random rand = new Random();
		int xpos = rand.nextInt(7);
		sprites.elementAt(0).texture_x = (xpos * GLOBAL.TILE_SIZE);
		sprites.elementAt(0).texture_y = (896);
		sprites.elementAt(0).size_y = (63);
	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {
		if(what != null && what.TYPE == GLOBAL.PLAYER && alive) {
			GLOBAL.playerHit(TYPE);
			alive = false;
		}
	}

	@Override
	public void reset() {
		alive = true;
		timer.startTimer();
		fade = false;
		sprites.elementAt(0).alpha = 1;
	}

	private GameTimer timer;
	private Boolean fade;
}
