package com.lazypointer.game;

import java.util.Vector;

public abstract class Spell extends GameObject {

	private static final long serialVersionUID = 1L;

	public Spell(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
		point_x = coordinates.x;
		point_y = coordinates.y;
		alive = true;
		speed = 2;
		death_delay = new GameTimer();
		death_delay.setMilliSeconds(200);
		emitter = new ParticleEmitter(new Vector<Particle>());	// empty vector for particleemitter
		from_object = 0;
	}

	public void setAngle(double ang) {
		angle = ang;
	}
	
	public void updateParticles() {
		emitter.update((int)point_x, (int)point_y);
	}
	
	public void setDead() {
		alive = false;
	}
	
	public void reset() {
		
	}
	
	public ParticleEmitter getEmitter() { return emitter; }
	public Boolean is_alive() { return alive; }
	
	public Boolean alive;
	public double angle;
	public double point_x;
	public double point_y;
	public int from_object;
	protected ParticleEmitter emitter;
	protected GameTimer death_delay;

}
