package com.lazypointer.game;

public class Spike extends Spell {

	private static final long serialVersionUID = -4462260915586801308L;

	public Spike(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
		death_delay.setMilliSeconds(0);
		player_coord = new Coordinate(0, 0);
		active = false;
		alive = true;
		angle = 0;
	}
	
	public void setPlayerCoord(Coordinate pc) { player_coord = pc; }

	@Override
	public void logic() {
		
		// If player is nearby, otherwise we stand and do nothing.
		// 200 pixels counts as nearby.
		if(!active) {
			if(player_coord.x - 100 > coordinates.x || player_coord.x + 100 < coordinates.x ||
				player_coord.y - 100 > coordinates.y || player_coord.y + 100 < coordinates.y)
				return;
			else
				active = true;
		}
		
		// Move
		if(alive) {
			point_x += (speed*Math.cos(angle))*speed;
			point_y += (speed*Math.sin(angle))*speed;

			coordinates.x = ((int)point_x);
			coordinates.y = ((int)point_y);
			hitbox.updateHitbox(coordinates.x, coordinates.y);
		}
		
	}

	@Override
	public void init() {
		
		point_x = coordinates.x;
		point_y = coordinates.y;
		hitbox.width = 20;
		hitbox.height = 20;
		hitbox.updateHitbox(coordinates.x, coordinates.y);
		
		if(TYPE == GLOBAL.OBJECT_SPIKE_LEFT)
			angle = Math.atan2(0, (getMiddleX() - 10) - getMiddleX());
		else if(TYPE == GLOBAL.OBJECT_SPIKE_RIGHT)
			angle = Math.atan2(0, (getMiddleX() + 10) - getMiddleX());
		else if(TYPE == GLOBAL.OBJECT_SPIKE_UP)
			angle = Math.atan2((getMiddleY() - 10) - getMiddleY(), 0);
		else if(TYPE == GLOBAL.OBJECT_SPIKE_DOWN)
			angle = Math.atan2((getMiddleY() + 10) - getMiddleY(), 0);
		
		sprites.elementAt(0).texture_x = (470);
		sprites.elementAt(0).texture_y = (460);
		sprites.elementAt(0).size_x = (35);
		sprites.elementAt(0).size_y = (35);
		sprites.elementAt(0).rotation = (Math.toDegrees(angle));
	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {
		if(what == null && alive) {
			alive = false;
		}
		else if(what != null && what.TYPE == GLOBAL.PLAYER) {
			GLOBAL.playerHit(TYPE);
			alive = false;
		}
	}

	@Override
	public void reset() {
	}

	private Coordinate player_coord;
	private Boolean active;
}
