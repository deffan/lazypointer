package com.lazypointer.game;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AStar implements Serializable {

	private static final long serialVersionUID = 5154529782077221740L;
	/**
	* AStar (A*) path-finding algorithm
	*/
	AStar() {
		level = null;
		end = null;
		open = new ArrayList<Node>();
		closed = new ArrayList<Node>();
	}
	
	/**
	* getPath
	* @param <b>start_o:</b> Start coordinate.
	* @param <b>end_o:</b> Goal coordinate.
	* @param <b>map[][]:</b> 2D array (world map).
	* @return Node object, with parents. Iterate through parents to get path (reversed).
	*/
	public List<Coordinate> getPath(Coordinate start_o, Coordinate end_o, Level l) {
		
		// Clear previous search
		open.clear();
		closed.clear();

		// Set map
		level = l;
		
		// End node
		end = new Node();
		end.coordinate = new Coordinate((int)end_o.x / GLOBAL.TILE_SIZE, (int)end_o.y / GLOBAL.TILE_SIZE);
		
		// Start node
		Node start = new Node();
		start.coordinate = new Coordinate((int)start_o.x / GLOBAL.TILE_SIZE, (int)start_o.y / GLOBAL.TILE_SIZE);
		start.G = 0;
		start.H = calc_H_ret(start, end);
		start.F = start.G + start.H;
		
		// Add to open list
		open.add(start);
		
		// Calculate path and return result
		return constructPath(calculate());
		
	}
	
	/**
	* constructPath
	* @param <b>path:</b> The final node with parents.
	* @return List with Coordinate objects or NULL.
	*/
	private List<Coordinate> constructPath(Node path) {
		
		if(path == null)
			return null;
		else {
			
			List<Coordinate> thepath = new ArrayList<Coordinate>();
			while(path != null) {
				thepath.add(0, path.coordinate);
				path = path.parent;
			}
			
			return thepath;
		}
	}
	
	/**
	* Calculates the path.
	* @return Returns the final Node with parents, or NULL.
	*/	
	private Node calculate() {
		
		int max = 2000;
		while(true) {
			
			// Failed to find goal
			if(open.size() == 0)
				return null;

			// Look for lowest F cost in the open-list
			int current_f = 5000;
			int element = 0;
			for(int i = 0; i < open.size(); i++) {
				
				if(open.get(i).F < current_f) {
					current_f = open.get(i).F;
					element = i;
				}
			}
			
			// Remove the Node with lowest F score from open list, and add to closed
			current = open.remove(element);
			closed.add(current);
			
			// We have fount it?
			if((int)current.coordinate.x == (int)end.coordinate.x && (int)current.coordinate.y == (int)end.coordinate.y) {
				return current;
			}
	
			// Just get coordinates
			int xPos = (int)current.coordinate.x;
			int yPos = (int)current.coordinate.y;
			
			// This is just to prevent infinite loop (for some weird reason, should never occur though).
			max--;
			if(max <= 0) {
				return null;
			}

			// Check all 8 squares around the current_square.
			if(xPos > 0 && xPos < level.world_size_x && yPos > 0 && yPos < level.world_size_y) {
				
				Boolean down = false;
				Boolean up = false;
				Boolean left = false;
				Boolean right = false;

				up = checkNode(xPos, yPos - 1, false);			// TOP
				down = checkNode(xPos, yPos + 1, false);		// BOTTOM
				left = checkNode(xPos - 1, yPos, false);		// LEFT
				right = checkNode(xPos + 1, yPos, false);		// RIGHT
				
				if(!down || !up || right) {
						checkNode(xPos - 1, yPos + 1, true);	// BOTTOM LEFT
						checkNode(xPos + 1, yPos + 1, true);	// BOTTOM RIGHT
				}
				
				if(!up && !down || left) {
						checkNode(xPos - 1, yPos - 1, true);	// TOP LEFT
						checkNode(xPos + 1, yPos - 1, true);	// TOP RIGHT
				}	

			}

		}
	}
	
	/**
	* checkNode
	* @param <b>x:</b> x coordinate.
	* @param <b>y:</b> y coordinate.
	* @param <b>diagonal:</b> if this is diagonal or not (relative to current).
	*/
	private Boolean checkNode(int x, int y, Boolean diagonal) {
		
		if(!level.world[x][y].isCollidable()) {	// Ignore if this is collidable (not walkable)
			
			// Construct a new Node with the coordinates of this tile
			Node n = new Node();
			n.coordinate = new Coordinate((int)level.world[x][y].coordinates.x / GLOBAL.TILE_SIZE, (int)level.world[x][y].coordinates.y / GLOBAL.TILE_SIZE);
			
			// Does it already exist in the closed list?
			Boolean exist_closed = false;
			for(int i = 0; i < closed.size(); i++)
				if((int)closed.get(i).coordinate.x == (int)n.coordinate.x && (int)closed.get(i).coordinate.y == (int)n.coordinate.y) {
					exist_closed = true;
					break;
				}
			
			// If it dosent exist in the closed list:
			if(!exist_closed) {
				
				// Does it exist in the open list?
				Boolean exist_open = false;
				for(int i = 0; i < open.size(); i++)
					if((int)open.get(i).coordinate.x == (int)n.coordinate.x && (int)open.get(i).coordinate.y == (int)n.coordinate.y) {
						n = open.get(i);
						exist_open = true;
						break;
					}
				
				// The G score of this node
				n.G = 10;
				if(diagonal)
					n.G = 14;

				// Calculate H
				n.H = calc_H_ret(end, n);
				n.F = n.G + n.H;
				
				// The tenative G score...
				int tentative_g_score = n.G + n.H;
				
				if(!exist_open || tentative_g_score < n.G) {	
					
					n.parent = current;	// Make the current square the parent of this square.
					n.G = tentative_g_score;
					n.H = calc_H_ret(n, end);
					n.F = n.G + n.H;
					
					// Add to open_list if not already there
					if(!exist_open)
						open.add(n); 		
				}
			}
			return true;
		}
		return false;
	}
	
	/**
	* calc_H_ret - Calculates the heuristics...
	* @param <b>from:</b> Node from.
	* @param <b>to:</b> Node to.
	* @return the result (int).
	*/	
	private int calc_H_ret(Node from, Node to) {
		return 10 * (Math.abs((int)from.coordinate.x - (int)to.coordinate.x) + Math.abs((int)from.coordinate.y - (int)to.coordinate.y));
	}
	
	private Level level;
	private Node end;
	private List<Node> open;
	private List<Node> closed;
	private Node current;
	
}



