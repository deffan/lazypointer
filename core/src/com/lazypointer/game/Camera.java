package com.lazypointer.game;

import java.util.Vector;

public class Camera extends GameObject {

	private static final long serialVersionUID = 2785183329964163181L;

	public Camera(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
	}

	@Override
	public void init() {
	}
	
	@Override
	public void logic() {
		
	}
	
	
	private void cameraWaypointLogic() {
	}

	@Override
	public void collision(GameObject what, CollisionInformation info) {
	}
	
	public void setWaypoints(Vector<Waypoint> w) { 
	}
	
	public void addWaypoint(Waypoint w) { 
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		
	}
}
