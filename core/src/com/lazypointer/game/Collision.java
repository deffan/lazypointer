package com.lazypointer.game;

import java.io.Serializable;

public class Collision implements Serializable {
	
	private static final long serialVersionUID = 1L;
	/**
	* Collision
	* @param <b>world_array[][]</b> The 2D world_array
	*/
	Collision(GameObject world_array[][]) {
		world = world_array;
		ci_camera = new CollisionInformation();
		ci_world = new CollisionInformation();
		ci_object = new CollisionInformation();
	}
	
	//------------------------------------------------------------------------------
	// collision_left()
	//------------------------------------------------------------------------------	
	private Boolean collision_left(GameObject a, GameObject b) {
		
		Boolean LEFT = a.hitbox.left - a.speed < b.hitbox.right && a.hitbox.right > b.hitbox.right;
		Boolean BELOW = a.hitbox.top + a.speed >= b.hitbox.bottom;
		Boolean ABOVE = a.hitbox.bottom - a.speed <= b.hitbox.top;
		
		if(LEFT && !BELOW && !ABOVE) {
			a.coordinates.x = b.hitbox.right;
			a.hitbox.updateHitbox(a.coordinates.x, a.coordinates.y);
			return true;
		}
		return false;

	}
	
	//------------------------------------------------------------------------------
	// collision_right()
	//------------------------------------------------------------------------------	
	private Boolean collision_right(GameObject a, GameObject b) {

		Boolean RIGHT = a.hitbox.right + a.speed > b.hitbox.left && a.hitbox.left < b.hitbox.left;
		Boolean BELOW = a.hitbox.top + a.speed >= b.hitbox.bottom;
		Boolean ABOVE = a.hitbox.bottom - a.speed <= b.hitbox.top;
		
		if(RIGHT && !BELOW && !ABOVE) {
			a.coordinates.x = (b.hitbox.left - a.hitbox.width - 1);
			a.hitbox.updateHitbox(a.coordinates.x, a.coordinates.y);
			return true;
		}
		return false;

	}
	
	//------------------------------------------------------------------------------
	// collision_up()
	//------------------------------------------------------------------------------		
	private Boolean collision_up(GameObject a, GameObject b) {

		Boolean BELOW = a.hitbox.top + a.speed >= b.hitbox.bottom;
		
 		if(BELOW && a.hitbox.top < b.hitbox.bottom) {
 			a.coordinates.y = b.hitbox.bottom;
 			a.hitbox.updateHitbox(a.coordinates.x, a.coordinates.y);
 			return true;
 		}
 		return false;
	}
	
	//------------------------------------------------------------------------------
	// collision_down()
	//------------------------------------------------------------------------------	
	private Boolean collision_down(GameObject a, GameObject b) {

		Boolean ABOVE = a.hitbox.bottom - a.speed <= b.hitbox.top;
		
 		if(ABOVE && a.hitbox.bottom > b.hitbox.top) {
 			
 			int newy = b.hitbox.top - a.hitbox.height - 1;
 			if(a.TYPE == GLOBAL.PLAYER)
 				newy++;
 				
 			a.coordinates.y = newy;
 			a.hitbox.updateHitbox(a.coordinates.x, a.coordinates.y);
 			return true;
 		}
 		return false;
	}
	
	//------------------------------------------------------------------------------
	// PEEK collision_down()
	//------------------------------------------------------------------------------
	private Boolean peekCollisionDown(GameObject a, GameObject b) {
		Boolean ABOVE = a.hitbox.bottom - a.speed <= b.hitbox.top;
 		Boolean DOWN = a.hitbox.bottom > b.hitbox.top;

 		if(DOWN && ABOVE)
 			return true;
 		return false;	
	}
	
	//------------------------------------------------------------------------------
	// PEEK collision_up()
	//------------------------------------------------------------------------------
	private Boolean peekCollisionUp(GameObject a, GameObject b) {
		Boolean BELOW = a.hitbox.top + a.speed >= b.hitbox.bottom;
 		Boolean UP = a.hitbox.top < b.hitbox.bottom;

 		if(UP && BELOW)
 			return true;
 		return false;
	}
	
	//------------------------------------------------------------------------------
	// PEEK collision_left()
	//------------------------------------------------------------------------------
	private Boolean peekCollisionLeft(GameObject a, GameObject b) {
		Boolean LEFT = a.hitbox.left - a.speed < b.hitbox.right && a.hitbox.right > b.hitbox.right;
		Boolean BELOW = a.hitbox.top + a.speed >= b.hitbox.bottom;
		Boolean ABOVE = a.hitbox.bottom - a.speed <= b.hitbox.top;
		
		if(LEFT && !BELOW && !ABOVE)
			return true;
		return false;
	}
	
	//------------------------------------------------------------------------------
	// PEEK collision_right()
	//------------------------------------------------------------------------------
	private Boolean peekCollisionRight(GameObject a, GameObject b) {
		Boolean RIGHT = a.hitbox.right + a.speed > b.hitbox.left && a.hitbox.left < b.hitbox.left;
		Boolean BELOW = a.hitbox.top + a.speed >= b.hitbox.bottom;
		Boolean ABOVE = a.hitbox.bottom - a.speed <= b.hitbox.top;
		
		if(RIGHT && !BELOW && !ABOVE)
			return true;
		return false;
	}
	
	//---------------------------------------------
	// collision_object() - GameObject vs GameObject in all directions
	//---------------------------------------------	
	public CollisionInformation collision_object(GameObject a, GameObject b) {

		// LEFT COLLISION
		ci_object.left = collision_left(a, b);
		
		// RIGHT COLLISION
		ci_object.right = collision_right(a, b);
		
		// UP COLLISION
		ci_object.up = collision_up(a, b);

		// DOWN COLLISION
		ci_object.down = collision_down(a, b);
	
		// Return result
		return ci_object;
	}
	
	//---------------------------------------------
	// collision_object_peek() - GameObject vs GameObject in all directions
	//---------------------------------------------	
	public CollisionInformation collision_object_peek(GameObject a, GameObject b) {

		// LEFT COLLISION
		ci_object.left = peekCollisionLeft(a, b);
		
		// RIGHT COLLISION
		ci_object.right = peekCollisionRight(a, b);
		
		// UP COLLISION
		ci_object.up = peekCollisionUp(a, b);

		// DOWN COLLISION
		ci_object.down = peekCollisionDown(a, b);
	
		// Return result
		return ci_object;
	}
	
	//---------------------------------------------
	// collision_world() - GameObject vs World[][] collision in all directions
	//---------------------------------------------	
	public CollisionInformation collision_world(GameObject o) {

		ci_world.left = false;
		ci_world.right = false;
		ci_world.up = false;
		ci_world.down = false;
		
		// LEFT COLLISION
		if(world[(int)o.coordinates.x / GLOBAL.TILE_SIZE][(int)o.coordinates.y / GLOBAL.TILE_SIZE].isCollidable())
			ci_world.left = collision_left(o, world[(int)o.coordinates.x / GLOBAL.TILE_SIZE][(int)o.coordinates.y / GLOBAL.TILE_SIZE]);
		
		if(world[(int)o.coordinates.x / GLOBAL.TILE_SIZE][(int)(o.coordinates.y + o.hitbox.height) / GLOBAL.TILE_SIZE].isCollidable())
			ci_world.left = collision_left(o, world[(int)o.coordinates.x / GLOBAL.TILE_SIZE][(int)(o.coordinates.y + o.hitbox.width) / GLOBAL.TILE_SIZE]);
		
		// RIGHT COLLISION
		if(world[(int)(o.coordinates.x + o.hitbox.width) / GLOBAL.TILE_SIZE][(int)o.coordinates.y / GLOBAL.TILE_SIZE].isCollidable())
			ci_world.right = collision_right(o, world[(int)(o.coordinates.x + o.hitbox.width) / GLOBAL.TILE_SIZE][(int)o.coordinates.y / GLOBAL.TILE_SIZE]);
		
		if(world[(int)(o.coordinates.x + o.hitbox.width) / GLOBAL.TILE_SIZE][(int)(o.coordinates.y + o.hitbox.height) / GLOBAL.TILE_SIZE].isCollidable())
			ci_world.right = collision_right(o, world[(int)(o.coordinates.x + o.hitbox.width) / GLOBAL.TILE_SIZE][(int)(o.coordinates.y + o.hitbox.width) / GLOBAL.TILE_SIZE]);

		// UP COLLISION
		if(world[(int)o.coordinates.x / GLOBAL.TILE_SIZE][(int)o.coordinates.y / GLOBAL.TILE_SIZE].isCollidable() && world[(int)o.coordinates.x / GLOBAL.TILE_SIZE][(int)o.coordinates.y / GLOBAL.TILE_SIZE].getCollisionType() == GLOBAL.COLLISION_BOX)
			ci_world.up = collision_up(o, world[(int)o.coordinates.x / GLOBAL.TILE_SIZE][(int)o.coordinates.y / GLOBAL.TILE_SIZE]);
		
		if(world[(int)(o.coordinates.x + o.hitbox.width) / GLOBAL.TILE_SIZE][(int)o.coordinates.y / GLOBAL.TILE_SIZE].isCollidable() && world[(int)(o.coordinates.x + o.hitbox.width) / GLOBAL.TILE_SIZE][(int)o.coordinates.y / GLOBAL.TILE_SIZE].getCollisionType() == GLOBAL.COLLISION_BOX)
			ci_world.up = collision_up(o, world[(int)(o.coordinates.x + o.hitbox.width) / GLOBAL.TILE_SIZE][(int)o.coordinates.y / GLOBAL.TILE_SIZE]);

		// DOWN COLLISION
		if(world[(int)o.coordinates.x / GLOBAL.TILE_SIZE][(int)(o.coordinates.y + o.hitbox.height) / GLOBAL.TILE_SIZE].isCollidable() && world[(int)o.coordinates.x / GLOBAL.TILE_SIZE][(int)(o.coordinates.y + o.hitbox.height) / GLOBAL.TILE_SIZE].getCollisionType() == GLOBAL.COLLISION_BOX)
			ci_world.down = collision_down(o, world[(int)o.coordinates.x / GLOBAL.TILE_SIZE][(int)(o.coordinates.y + o.hitbox.height) / GLOBAL.TILE_SIZE]);
		
		if(world[(int)(o.coordinates.x + o.hitbox.width) / GLOBAL.TILE_SIZE][(int)(o.coordinates.y + o.hitbox.height) / GLOBAL.TILE_SIZE].isCollidable() && world[(int)(o.coordinates.x + o.hitbox.width) / GLOBAL.TILE_SIZE][(int)(o.coordinates.y + o.hitbox.height) / GLOBAL.TILE_SIZE].getCollisionType() == GLOBAL.COLLISION_BOX)
			ci_world.down = collision_down(o, world[(int)(o.coordinates.x + o.hitbox.width) / GLOBAL.TILE_SIZE][(int)(o.coordinates.y + o.hitbox.height) / GLOBAL.TILE_SIZE]);
			
		// Return result
		return ci_world;
	}
	

	//---------------------------------------------
	// collision_world_quick() - GameObject vs World[][] collision in all directions
	//---------------------------------------------	
	public Boolean collision_world_quick(GameObject o) {
		
		if(world[(int)o.coordinates.x / GLOBAL.TILE_SIZE][(int)o.coordinates.y / GLOBAL.TILE_SIZE].isCollidable())
			if(box_collision(o, world[(int)o.coordinates.x / GLOBAL.TILE_SIZE][(int)o.coordinates.y / GLOBAL.TILE_SIZE]))
				return true;

		if(world[(int)o.coordinates.x / GLOBAL.TILE_SIZE][(int)o.coordinates.y / GLOBAL.TILE_SIZE].isCollidable())
			if(box_collision(o, world[(int)o.coordinates.x / GLOBAL.TILE_SIZE][(int)o.coordinates.y / GLOBAL.TILE_SIZE]))
				return true;
		
		if(world[(int)o.coordinates.x / GLOBAL.TILE_SIZE][(int)(o.coordinates.y + o.hitbox.height) / GLOBAL.TILE_SIZE].isCollidable())
			if(box_collision(o, world[(int)o.coordinates.x / GLOBAL.TILE_SIZE][(int)(o.coordinates.y + o.hitbox.width) / GLOBAL.TILE_SIZE]))
				return true;
		
		if(world[(int)(o.coordinates.x + o.hitbox.width) / GLOBAL.TILE_SIZE][(int)o.coordinates.y / GLOBAL.TILE_SIZE].isCollidable())
			if(box_collision(o, world[(int)(o.coordinates.x + o.hitbox.width) / GLOBAL.TILE_SIZE][(int)o.coordinates.y / GLOBAL.TILE_SIZE]))
				return true;
		
		if(world[(int)(o.coordinates.x + o.hitbox.width) / GLOBAL.TILE_SIZE][(int)(o.coordinates.y + o.hitbox.height) / GLOBAL.TILE_SIZE].isCollidable())
			if(box_collision(o, world[(int)(o.coordinates.x + o.hitbox.width) / GLOBAL.TILE_SIZE][(int)(o.coordinates.y + o.hitbox.width) / GLOBAL.TILE_SIZE]))
				return true;
		
		if(world[(int)o.coordinates.x / GLOBAL.TILE_SIZE][(int)o.coordinates.y / GLOBAL.TILE_SIZE].isCollidable() && world[(int)o.coordinates.x / GLOBAL.TILE_SIZE][(int)o.coordinates.y / GLOBAL.TILE_SIZE].getCollisionType() == GLOBAL.COLLISION_BOX)
			if(box_collision(o, world[(int)o.coordinates.x / GLOBAL.TILE_SIZE][(int)o.coordinates.y / GLOBAL.TILE_SIZE]))
				return true;
		
		if(world[(int)(o.coordinates.x + o.hitbox.width) / GLOBAL.TILE_SIZE][(int)o.coordinates.y / GLOBAL.TILE_SIZE].isCollidable() && world[(int)(o.coordinates.x + o.hitbox.width) / GLOBAL.TILE_SIZE][(int)o.coordinates.y / GLOBAL.TILE_SIZE].getCollisionType() == GLOBAL.COLLISION_BOX)
			if(box_collision(o, world[(int)(o.coordinates.x + o.hitbox.width) / GLOBAL.TILE_SIZE][(int)o.coordinates.y / GLOBAL.TILE_SIZE]))
				return true;
		
		if(world[(int)o.coordinates.x / GLOBAL.TILE_SIZE][(int)(o.coordinates.y + o.hitbox.height) / GLOBAL.TILE_SIZE].isCollidable() && world[(int)o.coordinates.x / GLOBAL.TILE_SIZE][(int)(o.coordinates.y + o.hitbox.height) / GLOBAL.TILE_SIZE].getCollisionType() == GLOBAL.COLLISION_BOX)
			if(box_collision(o, world[(int)o.coordinates.x / GLOBAL.TILE_SIZE][(int)(o.coordinates.y + o.hitbox.height) / GLOBAL.TILE_SIZE]))
				return true;
		
		if(world[(int)(o.coordinates.x + o.hitbox.width) / GLOBAL.TILE_SIZE][(int)(o.coordinates.y + o.hitbox.height) / GLOBAL.TILE_SIZE].isCollidable() && world[(int)(o.coordinates.x + o.hitbox.width) / GLOBAL.TILE_SIZE][(int)(o.coordinates.y + o.hitbox.height) / GLOBAL.TILE_SIZE].getCollisionType() == GLOBAL.COLLISION_BOX)
			if(box_collision(o, world[(int)(o.coordinates.x + o.hitbox.width) / GLOBAL.TILE_SIZE][(int)(o.coordinates.y + o.hitbox.height) / GLOBAL.TILE_SIZE]))
				return true;
		return false;
	}
	
	//---------------------------------------------
	// box_collision() - With game-objects
	//---------------------------------------------	
	public Boolean box_collision(GameObject a, GameObject b) {
		return box_collision(a.hitbox, b.hitbox);
	}

	//------------------------------------------------------------------------------
	// box_collision() - With hitboxes
	//------------------------------------------------------------------------------
	private Boolean box_collision(Hitbox a, Hitbox b) {
		if(a.right < b.left || a.left > b.right || a.bottom < b.top || a.top > b.bottom)
			return false;
		return true;
	}
	
	//------------------------------------------------------------------------------
	// is_within() - With hitboxes
	//------------------------------------------------------------------------------
	public Boolean is_within(Hitbox a, Hitbox b, int extended) {
		if(a.left > b.left - extended && a.right < b.right + extended && a.top > b.top - extended && a.bottom < b.bottom + extended)
			return true;
		return false;
	}
	
	//------------------------------------------------------------------------------
	// is_within() - With game-objects
	//------------------------------------------------------------------------------
	public Boolean is_within(GameObject a, GameObject b) {
		return is_within(a.hitbox, b.hitbox, 0);
	}
	
	//------------------------------------------------------------------------------
	// triangle_right_bottom()
	//------------------------------------------------------------------------------
	private Boolean triangle_right_bottom(GameObject a, GameObject b) {
		
		if(a.hitbox.right > b.hitbox.left) {
			int x = a.hitbox.bottom - b.hitbox.bottom;
			
			if(x >= 0) {
				a.coordinates.x = b.hitbox.left - a.hitbox.width - 1;
				a.coordinates.y = a.coordinates.y - 1;
				a.hitbox.updateHitbox(a.coordinates.x, a.coordinates.y);
			}
			
			if(x < 0 && a.hitbox.right + x > b.hitbox.left) {
				a.coordinates.x = b.hitbox.left - x - a.hitbox.width;
				a.coordinates.y = a.coordinates.y - 1;
				a.hitbox.updateHitbox(a.coordinates.x, a.coordinates.y);
				return true;
			}
		}
		return false;
	}
	
	//------------------------------------------------------------------------------
	// triangle_right_top()
	//------------------------------------------------------------------------------
	private Boolean triangle_right_top(GameObject a, GameObject b) {
		
		if(a.hitbox.right > b.hitbox.left) {
			int x = b.hitbox.top - a.hitbox.top;
			
			if(x >= 0) {
				a.coordinates.x = b.hitbox.right;
				a.coordinates.y = a.coordinates.y + 1;
				a.hitbox.updateHitbox(a.coordinates.x, a.coordinates.y);
			}

			if(a.hitbox.right + x > b.hitbox.left) {
				a.coordinates.x = b.hitbox.left - x - a.hitbox.width;
				a.coordinates.y = a.coordinates.y + 1;
				a.hitbox.updateHitbox(a.coordinates.x, a.coordinates.y);
				return true;
			}
		}
		return false;
	}
	
	//------------------------------------------------------------------------------
	// triangle_left_bottom()
	//------------------------------------------------------------------------------
	private Boolean triangle_left_bottom(GameObject a, GameObject b) {
		
		if(a.hitbox.left < b.hitbox.right) {
			int x = a.hitbox.bottom - b.hitbox.bottom;
			
			if(x >= 0) {
				a.coordinates.x = b.hitbox.right;
				a.coordinates.y = a.coordinates.y - 1;
				a.hitbox.updateHitbox(a.coordinates.x, a.coordinates.y);
			}
			
			if(x < 0 && a.hitbox.left - x < b.hitbox.right) {
				a.coordinates.x = b.hitbox.right + x;
				a.coordinates.y = a.coordinates.y - 1;
				a.hitbox.updateHitbox(a.coordinates.x, a.coordinates.y);
				return true;
			}
		}
		return false;
	}
	
	//------------------------------------------------------------------------------
	// triangle_left_top()
	//------------------------------------------------------------------------------
	private Boolean triangle_left_top(GameObject a, GameObject b) {
		
		if(a.hitbox.left < b.hitbox.right) {
			int x = b.hitbox.top - a.hitbox.top;
			
			if(x >= 0) {
				a.coordinates.x = b.hitbox.right;
				a.coordinates.y = a.coordinates.y + 1;
				a.hitbox.updateHitbox(a.coordinates.x, a.coordinates.y);
			}
			
			if(x < 0 && a.hitbox.left - x < b.hitbox.right) {
				a.coordinates.x = b.hitbox.right + x + 1;
				a.coordinates.y = a.coordinates.y + 1;
				a.hitbox.updateHitbox(a.coordinates.x, a.coordinates.y);
				return true;
			}
		}
		return false;
	}
	
	//------------------------------------------------------------------------------
	// collision_camera() - If any object collides with the camera boundaries (from the inside)
	//------------------------------------------------------------------------------	
	public CollisionInformation collision_camera(GameObject o, Hitbox camera) {
		
		// LEFT
		if(o.hitbox.left < camera.left) {
			o.coordinates.x = camera.left;
			ci_camera.left = true;
		}
		else
			ci_camera.left = false;
		
		// RIGHT
		if(o.hitbox.right > camera.right) {
			o.coordinates.x = camera.right - o.hitbox.width;
			ci_camera.right = true;
		}
		else
			ci_camera.right = false;
		
		// UP
		if(o.hitbox.top < camera.top) {
			o.coordinates.y = camera.top;
			ci_camera.up = true;
		}
		else
			ci_camera.up = false;
		
		// DOWN
		if(o.hitbox.bottom > camera.bottom) {
			o.coordinates.y = camera.bottom - o.hitbox.height;
			ci_camera.down = true;
		}
		else
			ci_camera.down = false;
		
		o.hitbox.updateHitbox(o.coordinates.x, o.coordinates.y);
		
		return ci_camera;
	}

	// DATAMEMBERS
	public GameObject world[][];
	private CollisionInformation ci_camera;
	private CollisionInformation ci_world;
	private CollisionInformation ci_object;
}
