package com.lazypointer.game;

import java.io.Serializable;

public class CollisionInformation implements Serializable {

	private static final long serialVersionUID = 2668019736284474601L;
	CollisionInformation() {
		up = false;
		down = false;
		right = false;
		left = false;
	}
	
	public Boolean up;
	public Boolean down;
	public Boolean right;
	public Boolean left;
	
}
