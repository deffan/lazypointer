package com.lazypointer.game;

import java.io.Serializable;

public class Coordinate implements Serializable {

	private static final long serialVersionUID = -7363584091307630976L;
	
	/**
	* Coordinate
	* @param <b>x:</b> The X coordinate.
	* @param <b>y:</b> The Y coordinate.
	*/
	public Coordinate(int new_x, int new_y) {
		x = new_x;
		y = new_y;
	}
	
	public double x;
	public double y;
	
}
