package com.lazypointer.game;

import java.io.Serializable;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

public class Draw implements Serializable {

	private static final long serialVersionUID = 5908912521043708923L;
	//------------------------------------------------------------------------------
	// constructor
	//------------------------------------------------------------------------------	
	Draw(Level l, Hitbox c) {
		level = l;
		camera_rect = c;
		grid_active = false;
		showing_hitboxes = false;
		screen_width = Gdx.graphics.getWidth();
		screen_height = Gdx.graphics.getHeight();
	}
	
	public void setSelected(GameObject sel) {
		selected_editor_object = sel;
	}

	//------------------------------------------------------------------------------
	// initDraw()
	//------------------------------------------------------------------------------	
	public Boolean initDraw() {
		
		try {
			batch = new SpriteBatch();
			//texture = new Texture(Gdx.files.internal("data/lptiles_large.png"));
			texture = new Texture(Gdx.files.internal(GLOBAL.main_texture));
			region = new TextureRegion(texture, 0, 0, GLOBAL.TILE_SIZE, GLOBAL.TILE_SIZE); 	// Texture part, will be used to draw tiles
			shapeRenderer = new ShapeRenderer();
			shapeRenderer.setColor(0, 1, 0, 1);
			
			// Camera (flip Y so its 0,0 in the TOP-LEFT corner)
			camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
			camera.setToOrtho(true);
			camera.update();
			batch.setProjectionMatrix(camera.combined);
			shapeRenderer.setProjectionMatrix(camera.combined);
			
			timefont = new BitmapFont(Gdx.files.internal("data/fonts/buxton50orange.fnt"), true);
			levelfont = new BitmapFont(Gdx.files.internal("data/fonts/buxton32white.fnt"), true);
			
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
			return false;
		}
		
		/*
		ShaderProgram.pedantic = false;
		defaultShader = new ShaderProgram(vertexShader, defaultPixelShader);
		lightShader = new ShaderProgram(vertexShader, lightPixelShader);
		ambientShader = new ShaderProgram(vertexShader, ambientPixelShader);
		finalShader = new ShaderProgram(vertexShader, finalPixelShader);
		batch.setShader(defaultShader);
	
		ambientShader.begin();
		ambientShader.setUniformf("ambientColor", ambientColor.x, ambientColor.y, ambientColor.z, ambientIntensity);
		ambientShader.end();
		
		lightShader.begin();
		lightShader.setUniformf("resolution", Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		lightShader.end();
		
		lightShader.begin();
		lightShader.setUniformi("u_lightmap", 1);
		lightShader.end();
		
		finalShader.begin();
		finalShader.setUniformi("u_lightmap", 1);
		finalShader.setUniformf("ambientColor", ambientColor.x, ambientColor.y, ambientColor.z, ambientIntensity);
		finalShader.end();
		
		finalShader.begin();
		finalShader.setUniformf("resolution", Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		finalShader.end();
		*/
		return true;
	}
	
	//------------------------------------------------------------------------------
	// editor_draw()
	//------------------------------------------------------------------------------	
	public void editor_draw(float alpha_normal, float alpha_background, float alpha_foreground, float alpha_object) {

		draw();
		
		/*
		
		drawLights();
		batch.setShader(finalShader);
		
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		drawWorld(level.background, alpha_background);
		drawWorld(level.world, alpha_normal);
		
		drawPlayer();
		 
		drawObjects();
		
		//drawWorld(level.foreground, alpha_foreground);
		drawWorldOutside(level);
		
		
		batch.end();
		
		if(grid_active)
			drawGrid();
		
		if(showing_hitboxes)
			drawObjectHitboxes();
		
		// draw other stuff, enemies, objects.
		// draw animations,spells,etc

		 */
	
	}
	
	public void showTexture() {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		
		region.setRegion(0, 0, texture.getWidth(), texture.getHeight());
		region.flip(false, true);
		batch.setColor(1, 1, 1, 1);
		batch.draw(region, 0, 0);

		batch.end();
	}
	
	public void draw_failure(float global_alpha, int button1x, int button1y, int button2x, int button2y) {
		
		int center_w = (screen_width / 2) - 192;
		int center_h = (screen_height / 2) - 195;

		// rita ut world(global_alpha)
		draw(global_alpha);
		
		// rita ut "grid"
		batch.begin();
		batch.setColor(1, 1, 1, global_alpha);
		drawTransGrid();
		
		// rita ut fail
		region.setRegion(388, 1650, 386, 390);
		region.flip(false, true);
		batch.draw(region, center_w, center_h);
		batch.end();
	}
	
	public void draw_success(float global_alpha, int button1x, int button1y, int button2x, int button2y) {
		
		int center_w = (screen_width / 2) - 192;
		int center_h = (screen_height / 2) - 195;

		// rita ut world(global_alpha)
		draw(global_alpha);
		
		// rita ut "grid"
		batch.begin();
		batch.setColor(1, 1, 1, global_alpha);
		drawTransGrid();
		
		// rita ut success
		region.setRegion(0, 1650, 386, 390);
		region.flip(false, true);
		batch.draw(region, center_w, center_h);
		batch.end();
	}
	
	public void draw_pause(float global_alpha, int button1x, int button1y, int button2x, int button2y) {
		
		int center_w = (screen_width / 2) - 192;
		int center_h = (screen_height / 2) - 195;

		// rita ut world(global_alpha)
		draw(global_alpha);
		
		// rita ut "grid"
		batch.begin();
		batch.setColor(1, 1, 1, global_alpha);
		drawTransGrid();
		
		// rita ut success
		region.setRegion(1480, 1365, 386, 206);
		region.flip(false, true);
		batch.draw(region, center_w, center_h);
		batch.end();
	}
	
	public void draw_menu(float global_alpha, int button1x, int button1y, int button2x, int button2y) {
		
		int center_w = (screen_width / 2) - 195;
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		batch.setColor(1, 1, 1, global_alpha);
		
		// draw menu
		region.setRegion(1091, 1650, 391, 383);
		region.flip(false, true);
		batch.draw(region, center_w, 0);
		center_w = (screen_width / 2) - 186;

		// draw play-button
		region.setRegion(button1x, button1y, 373, 80);
		region.flip(false, true);
		batch.draw(region, center_w, 240);
		
		// draw exit-button
		region.setRegion(button2x, button2y, 373, 80);
		region.flip(false, true);
		batch.draw(region, center_w, 325);

		batch.end();
		
	}
	
	public void draw() {

		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();	// remove THIS when lights are turned on

		drawWorld(level.background, 1);
		drawWorldOutside(level, 1);
		drawBackOBjects();
		drawObjects();
		drawPlayer();
		drawPowerups();
		drawWorld(level.foreground, 1);
		drawInGameUI(1);

		batch.end();
		
		if(grid_active)
			drawGrid();
		
		if(showing_hitboxes)
			drawObjectHitboxes();
		
		drawSelected();
	
	}
	
	public void drawTransGrid() {
		
		// Set start position
		int start_map_x = camera_rect.left / GLOBAL.TILE_SIZE;
		int start_map_y = camera_rect.top / GLOBAL.TILE_SIZE;

		// Calculate end of map
		int end_map_x = start_map_x + (screen_width / GLOBAL.TILE_SIZE);
		int end_map_y = start_map_y + (screen_height / GLOBAL.TILE_SIZE);
		end_map_x++;
		end_map_y++;

		// Offset
		int offset_x =- (camera_rect.left % GLOBAL.TILE_SIZE);
		int offset_y =- (camera_rect.top % GLOBAL.TILE_SIZE);
		int xPos = offset_x;
		int yPos = offset_y;
		
		for(int y = start_map_y; y <= end_map_y; y++) {
			for(int x = start_map_x; x <= end_map_x; x++) {
				region.setRegion(768, 64, GLOBAL.TILE_SIZE, GLOBAL.TILE_SIZE);
				batch.draw(region, xPos, yPos);
				xPos += GLOBAL.TILE_SIZE;
			} 
			xPos = offset_x;
			yPos += GLOBAL.TILE_SIZE;
		}
	}

	public void draw(float global_alpha) {
		
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();	// remove THIS when lights are turned on

		drawWorld(level.background, global_alpha);
		drawWorldOutside(level, global_alpha);
		drawBackOBjects(global_alpha);
		drawObjects(global_alpha);
		drawPlayer(global_alpha);
		drawPowerups(global_alpha);
		drawWorld(level.foreground, global_alpha);
		drawInGameUI(global_alpha);
	
		batch.end();
	}
	
	
	private void drawBackOBjects(float global_alpha) {
		
		int objsize = level.backObjects.size();
		for(int i = 0; i < objsize; i++) {
			for(int n = 0; n < level.backObjects.elementAt(i).sprites.size(); n++) {
				
				region.setRegion(level.backObjects.elementAt(i).sprites.elementAt(n).texture_x, 
						level.backObjects.elementAt(i).sprites.elementAt(n).texture_y, 
						level.backObjects.elementAt(i).sprites.elementAt(n).size_x, 
						level.backObjects.elementAt(i).sprites.elementAt(n).size_y);
				
				region.flip(false, true);
				batch.setColor(1, 1, 1, global_alpha);
				
				batch.draw(region,
						(int)level.backObjects.elementAt(i).coordinates.x - camera_rect.left + level.backObjects.elementAt(i).sprites.elementAt(n).relative_x, 
						(int)level.backObjects.elementAt(i).coordinates.y - camera_rect.top + level.backObjects.elementAt(i).sprites.elementAt(n).relative_y, 
						level.backObjects.elementAt(i).sprites.elementAt(n).size_x / 2, 
						level.backObjects.elementAt(i).sprites.elementAt(n).size_y / 2, 
						region.getRegionWidth(), 
						region.getRegionHeight(), 
						level.backObjects.elementAt(i).sprites.elementAt(n).scale_x, 
						level.backObjects.elementAt(i).sprites.elementAt(n).scale_y, 
						(float) level.backObjects.elementAt(i).sprites.elementAt(n).rotation);
			}
		}
	}
	
	private void drawBackOBjects() {
		
		int objsize = level.backObjects.size();
		for(int i = 0; i < objsize; i++) {
			for(int n = 0; n < level.backObjects.elementAt(i).sprites.size(); n++) {
				
				region.setRegion(level.backObjects.elementAt(i).sprites.elementAt(n).texture_x, 
						level.backObjects.elementAt(i).sprites.elementAt(n).texture_y, 
						level.backObjects.elementAt(i).sprites.elementAt(n).size_x, 
						level.backObjects.elementAt(i).sprites.elementAt(n).size_y);
				
				region.flip(false, true);
				batch.setColor(1, 1, 1, level.backObjects.elementAt(i).sprites.elementAt(n).alpha);
				
				batch.draw(region,
						(int)level.backObjects.elementAt(i).coordinates.x - camera_rect.left + level.backObjects.elementAt(i).sprites.elementAt(n).relative_x, 
						(int)level.backObjects.elementAt(i).coordinates.y - camera_rect.top + level.backObjects.elementAt(i).sprites.elementAt(n).relative_y, 
						level.backObjects.elementAt(i).sprites.elementAt(n).size_x / 2, 
						level.backObjects.elementAt(i).sprites.elementAt(n).size_y / 2, 
						region.getRegionWidth(), 
						region.getRegionHeight(), 
						level.backObjects.elementAt(i).sprites.elementAt(n).scale_x, 
						level.backObjects.elementAt(i).sprites.elementAt(n).scale_y, 
						(float) level.backObjects.elementAt(i).sprites.elementAt(n).rotation);
			}
		}
	}

	private void drawObjects(float global_alpha) {
		
		// Spells
		int spellsize = level.spells.size();
		for(int i = 0; i < spellsize; i++) {
			
			if(!level.spells.elementAt(i).is_alive())
				continue;
			
			// draw the spells particles
			for(int n = 0; n < level.spells.elementAt(i).getEmitter().getParticles().size(); n++) {
				region.setRegion(level.spells.elementAt(i).getEmitter().getParticles().get(n).getSprite().texture_x, level.spells.elementAt(i).getEmitter().getParticles().get(n).getSprite().texture_y, level.spells.elementAt(i).getEmitter().getParticles().get(n).getParticleSizeX(), level.spells.elementAt(i).getEmitter().getParticles().get(n).getParticleSizeY());
				batch.setColor(1, 1, 1, global_alpha);
				batch.draw(region, level.spells.elementAt(i).getEmitter().getParticles().get(n).getX() - camera_rect.left, level.spells.elementAt(i).getEmitter().getParticles().get(n).getY() - camera_rect.top);
			}
			
			int spellspritesize = level.spells.elementAt(i).sprites.size();
			for(int n = 0; n < spellspritesize; n++) {
				
				region.setRegion(level.spells.elementAt(i).sprites.elementAt(n).texture_x, 
						level.spells.elementAt(i).sprites.elementAt(n).texture_y, 
						level.spells.elementAt(i).sprites.elementAt(n).size_x, 
						level.spells.elementAt(i).sprites.elementAt(n).size_y);
				
				region.flip(false, true);
				batch.setColor(1, 1, 1, global_alpha);
				
				batch.draw(region,
						(int)level.spells.elementAt(i).coordinates.x - camera_rect.left + level.spells.elementAt(i).sprites.elementAt(n).relative_x, 
						(int)level.spells.elementAt(i).coordinates.y - camera_rect.top + level.spells.elementAt(i).sprites.elementAt(n).relative_y, 
						level.spells.elementAt(i).sprites.elementAt(n).size_x / 2, 
						level.spells.elementAt(i).sprites.elementAt(n).size_y / 2, 
						region.getRegionWidth(), 
						region.getRegionHeight(), 
						level.spells.elementAt(i).sprites.elementAt(n).scale_x, 
						level.spells.elementAt(i).sprites.elementAt(n).scale_y, 
						(float) level.spells.elementAt(i).sprites.elementAt(n).rotation);
			}

		}
		
		// Draw object sprite(s)
		int objsize = level.objects.size();
		for(int i = 0; i < objsize; i++) {
			for(int n = 0; n < level.objects.elementAt(i).sprites.size(); n++) {
				
				region.setRegion(level.objects.elementAt(i).sprites.elementAt(n).texture_x, 
						level.objects.elementAt(i).sprites.elementAt(n).texture_y, 
						level.objects.elementAt(i).sprites.elementAt(n).size_x, 
						level.objects.elementAt(i).sprites.elementAt(n).size_y);
				
				region.flip(false, true);
				batch.setColor(1, 1, 1, global_alpha);
				
				batch.draw(region,
						(int)level.objects.elementAt(i).coordinates.x - camera_rect.left + level.objects.elementAt(i).sprites.elementAt(n).relative_x, 
						(int)level.objects.elementAt(i).coordinates.y - camera_rect.top + level.objects.elementAt(i).sprites.elementAt(n).relative_y, 
						level.objects.elementAt(i).sprites.elementAt(n).size_x / 2, 
						level.objects.elementAt(i).sprites.elementAt(n).size_y / 2, 
						region.getRegionWidth(), 
						region.getRegionHeight(), 
						level.objects.elementAt(i).sprites.elementAt(n).scale_x, 
						level.objects.elementAt(i).sprites.elementAt(n).scale_y, 
						(float) level.objects.elementAt(i).sprites.elementAt(n).rotation);
			}
		}
		


	}
	
	private void drawObjects() {
		
		// Spells
		int spellsize = level.spells.size();
		for(int i = 0; i < spellsize; i++) {
			
			if(!level.spells.elementAt(i).is_alive())
				continue;
			
			// draw the spells particles
			for(int n = 0; n < level.spells.elementAt(i).getEmitter().getParticles().size(); n++) {
				region.setRegion(level.spells.elementAt(i).getEmitter().getParticles().get(n).getSprite().texture_x, level.spells.elementAt(i).getEmitter().getParticles().get(n).getSprite().texture_y, level.spells.elementAt(i).getEmitter().getParticles().get(n).getParticleSizeX(), level.spells.elementAt(i).getEmitter().getParticles().get(n).getParticleSizeY());
				batch.setColor(1, 1, 1, level.spells.elementAt(i).getEmitter().getParticles().get(n).getSprite().alpha);
				batch.draw(region, level.spells.elementAt(i).getEmitter().getParticles().get(n).getX() - camera_rect.left, level.spells.elementAt(i).getEmitter().getParticles().get(n).getY() - camera_rect.top);
			}
			
			int spellspritesize = level.spells.elementAt(i).sprites.size();
			for(int n = 0; n < spellspritesize; n++) {
				
				region.setRegion(level.spells.elementAt(i).sprites.elementAt(n).texture_x, 
						level.spells.elementAt(i).sprites.elementAt(n).texture_y, 
						level.spells.elementAt(i).sprites.elementAt(n).size_x, 
						level.spells.elementAt(i).sprites.elementAt(n).size_y);
				
				region.flip(false, true);
				batch.setColor(1, 1, 1, level.spells.elementAt(i).sprites.elementAt(n).alpha);
				
				batch.draw(region,
						(int)level.spells.elementAt(i).coordinates.x - camera_rect.left + level.spells.elementAt(i).sprites.elementAt(n).relative_x, 
						(int)level.spells.elementAt(i).coordinates.y - camera_rect.top + level.spells.elementAt(i).sprites.elementAt(n).relative_y, 
						level.spells.elementAt(i).sprites.elementAt(n).size_x / 2, 
						level.spells.elementAt(i).sprites.elementAt(n).size_y / 2, 
						region.getRegionWidth(), 
						region.getRegionHeight(), 
						level.spells.elementAt(i).sprites.elementAt(n).scale_x, 
						level.spells.elementAt(i).sprites.elementAt(n).scale_y, 
						(float) level.spells.elementAt(i).sprites.elementAt(n).rotation);
			}

		}
		
		// Draw object sprite(s)
		int objsize = level.objects.size();
		for(int i = 0; i < objsize; i++) {
			for(int n = 0; n < level.objects.elementAt(i).sprites.size(); n++) {
				
				region.setRegion(level.objects.elementAt(i).sprites.elementAt(n).texture_x, 
						level.objects.elementAt(i).sprites.elementAt(n).texture_y, 
						level.objects.elementAt(i).sprites.elementAt(n).size_x, 
						level.objects.elementAt(i).sprites.elementAt(n).size_y);
				
				region.flip(false, true);
				batch.setColor(1, 1, 1, level.objects.elementAt(i).sprites.elementAt(n).alpha);
				
				batch.draw(region,
						(int)level.objects.elementAt(i).coordinates.x - camera_rect.left + level.objects.elementAt(i).sprites.elementAt(n).relative_x, 
						(int)level.objects.elementAt(i).coordinates.y - camera_rect.top + level.objects.elementAt(i).sprites.elementAt(n).relative_y, 
						level.objects.elementAt(i).sprites.elementAt(n).size_x / 2, 
						level.objects.elementAt(i).sprites.elementAt(n).size_y / 2, 
						region.getRegionWidth(), 
						region.getRegionHeight(), 
						level.objects.elementAt(i).sprites.elementAt(n).scale_x, 
						level.objects.elementAt(i).sprites.elementAt(n).scale_y, 
						(float) level.objects.elementAt(i).sprites.elementAt(n).rotation);
			}
		}


	}

	public void drawPowerups(float global_alpha) {
		
		// Draw powerups
		int powerupsize = level.powerups.size();
		for(int i = 0; i < powerupsize; i++) {
			for(int n = 0; n < level.powerups.elementAt(i).sprites.size(); n++) {
				
				if(!level.powerups.elementAt(i).active)
					continue;
				
				region.setRegion(level.powerups.elementAt(i).sprites.elementAt(n).texture_x, 
						level.powerups.elementAt(i).sprites.elementAt(n).texture_y, 
						level.powerups.elementAt(i).sprites.elementAt(n).size_x, 
						level.powerups.elementAt(i).sprites.elementAt(n).size_y);
				
				region.flip(false, true);
				batch.setColor(1, 1, 1, global_alpha);
				
				batch.draw(region,
						(int)level.powerups.elementAt(i).coordinates.x - camera_rect.left + level.powerups.elementAt(i).sprites.elementAt(n).relative_x, 
						(int)level.powerups.elementAt(i).coordinates.y - camera_rect.top + level.powerups.elementAt(i).sprites.elementAt(n).relative_y, 
						level.powerups.elementAt(i).sprites.elementAt(n).size_x / 2, 
						level.powerups.elementAt(i).sprites.elementAt(n).size_y / 2, 
						region.getRegionWidth(), 
						region.getRegionHeight(), 
						1, 
						1, 
						1);
			}
		}
	}

	public void drawPowerups() {
		// Draw powerups
		int powerupsize = level.powerups.size();
		for(int i = 0; i < powerupsize; i++) {
			for(int n = 0; n < level.powerups.elementAt(i).sprites.size(); n++) {
				
				region.setRegion(level.powerups.elementAt(i).sprites.elementAt(n).texture_x, 
						level.powerups.elementAt(i).sprites.elementAt(n).texture_y, 
						level.powerups.elementAt(i).sprites.elementAt(n).size_x, 
						level.powerups.elementAt(i).sprites.elementAt(n).size_y);
				
				region.flip(false, true);
				batch.setColor(1, 1, 1, level.powerups.elementAt(i).sprites.elementAt(n).alpha);
				
				batch.draw(region,
						(int)level.powerups.elementAt(i).coordinates.x - camera_rect.left + level.powerups.elementAt(i).sprites.elementAt(n).relative_x, 
						(int)level.powerups.elementAt(i).coordinates.y - camera_rect.top + level.powerups.elementAt(i).sprites.elementAt(n).relative_y, 
						level.powerups.elementAt(i).sprites.elementAt(n).size_x / 2, 
						level.powerups.elementAt(i).sprites.elementAt(n).size_y / 2, 
						region.getRegionWidth(), 
						region.getRegionHeight(), 
						1, 
						1, 
						1);
			}
		}
	}
	
	public void changeGridColor(int r, int g, int b) {
		shapeRenderer.setColor(r, g, b, 1);
	}
	
	public void showGrid(Boolean onoff) {
		grid_active = onoff;
	}
	
	public void showHitboxes(Boolean onoff) {
		showing_hitboxes = onoff;
	}
	
	public Boolean isGridActive() { return grid_active; }
	public Boolean isHitboxesActive() { return showing_hitboxes; }
	
	public void drawSelected() {
		
		// Begin shaperenderer
		shapeRenderer.begin(ShapeType.Line);

		shapeRenderer.line(
				selected_editor_object.hitbox.left - camera_rect.left, 
				selected_editor_object.hitbox.top - camera_rect.top, 
				selected_editor_object.hitbox.left + 10 - camera_rect.left, 
				selected_editor_object.hitbox.top - camera_rect.top);
		
		shapeRenderer.line(
				selected_editor_object.hitbox.right - 10 - camera_rect.left,
				selected_editor_object.hitbox.top - camera_rect.top,
				selected_editor_object.hitbox.right - camera_rect.left,
				selected_editor_object.hitbox.top - camera_rect.top);
		
		shapeRenderer.line(
				selected_editor_object.hitbox.left - camera_rect.left,
				selected_editor_object.hitbox.bottom - camera_rect.top,
				selected_editor_object.hitbox.left + 10 - camera_rect.left,
				selected_editor_object.hitbox.bottom - camera_rect.top);
		
		shapeRenderer.line(
				selected_editor_object.hitbox.right - 10 - camera_rect.left,
				selected_editor_object.hitbox.bottom - camera_rect.top,
				selected_editor_object.hitbox.right - camera_rect.left,
				selected_editor_object.hitbox.bottom - camera_rect.top);
		
		shapeRenderer.line(
				selected_editor_object.hitbox.left - camera_rect.left, 
				selected_editor_object.hitbox.top + 10 - camera_rect.top, 
				selected_editor_object.hitbox.left - camera_rect.left, 
				selected_editor_object.hitbox.top - camera_rect.top);
		
		shapeRenderer.line(
				selected_editor_object.hitbox.right - camera_rect.left,
				selected_editor_object.hitbox.top - camera_rect.top,
				selected_editor_object.hitbox.right - camera_rect.left,
				selected_editor_object.hitbox.top + 10 - camera_rect.top);
		
		shapeRenderer.line(
				selected_editor_object.hitbox.left - camera_rect.left,
				selected_editor_object.hitbox.bottom - camera_rect.top,
				selected_editor_object.hitbox.left - camera_rect.left,
				selected_editor_object.hitbox.bottom - 10 - camera_rect.top);
		
		shapeRenderer.line(
				selected_editor_object.hitbox.right - camera_rect.left,
				selected_editor_object.hitbox.bottom - 10 - camera_rect.top,
				selected_editor_object.hitbox.right - camera_rect.left,
				selected_editor_object.hitbox.bottom - camera_rect.top);
		
		shapeRenderer.end();
	}
	
	public void drawGrid() {
		
		// Begin shaperenderer
		shapeRenderer.begin(ShapeType.Line);

		// Set start position
		int start_map_x = camera_rect.left / GLOBAL.TILE_SIZE;
		int start_map_y = camera_rect.top / GLOBAL.TILE_SIZE;

		// Calculate end of map
		int end_map_x = start_map_x + (screen_width / GLOBAL.TILE_SIZE);
		int end_map_y = start_map_y + (screen_height / GLOBAL.TILE_SIZE);
		end_map_x++;
		end_map_y++;

		// Offset
		int offset_x =- (camera_rect.left % GLOBAL.TILE_SIZE);
		int offset_y =- (camera_rect.top % GLOBAL.TILE_SIZE);
		int xPos = offset_x;
		int yPos = offset_y;
		
		// H�r ritas allt ut
		for(int y = start_map_y; y <= end_map_y; y++) {
			for(int x = start_map_x; x <= end_map_x; x++) {

				// Draw this tile
				if(x > level.world_size_x - 1 || y > level.world_size_y - 1 || x < 0 || y < 0) {
					xPos += GLOBAL.TILE_SIZE;
				}
				else {
					shapeRenderer.rect(xPos, yPos, GLOBAL.TILE_SIZE, GLOBAL.TILE_SIZE);
					xPos += GLOBAL.TILE_SIZE;
				}
			} 

			// Nollst�ll X, uppdatera Y
			xPos = offset_x;
			yPos += GLOBAL.TILE_SIZE;
		}
		shapeRenderer.end();
	}

	//------------------------------------------------------------------------------
	// drawLights()
	//------------------------------------------------------------------------------	
	private void drawLights() {
/*
		//draw the light to the FBO
		fbo.begin();
		batch.setShader(defaultShader);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		batch.draw(light, player.coordinates.x - camera_rect.left - 230, player.coordinates.y - camera_rect.top - 230, 500, 500);
		batch.end();
		fbo.end();

		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.setShader(finalShader);
		batch.begin();
		
		fbo.getColorBufferTexture().bind(1); //this is important! bind the FBO to the 2nd texture unit
		light.bind(0);//we force the binding of a texture on first texture unit to avoid artefacts
		   //this is because our default and ambiant shader dont use multi texturing...
		   //youc can basically bind anything, it doesnt matter
*/
	
	}
	
	//------------------------------------------------------------------------------
	// drawObjectHitboxes() - Draws the object hitboxes
	//------------------------------------------------------------------------------	
	public void drawObjectHitboxes() {
		
		// Begin shaperenderer
		shapeRenderer.begin(ShapeType.Line);
		
		// Draw object hitboxes
		shapeRenderer.rect(level.player.hitbox.left - camera_rect.left, level.player.hitbox.top - camera_rect.top, level.player.hitbox.width, level.player.hitbox.height);
		for(int i = 0; i < level.objects.size(); i++) {
			shapeRenderer.rect(level.objects.elementAt(i).hitbox.left - camera_rect.left, level.objects.elementAt(i).hitbox.top - camera_rect.top, level.objects.elementAt(i).hitbox.width, level.objects.elementAt(i).hitbox.height);
		}
		
		// Draw spells hitboxes
		for(int i = 0; i < level.spells.size(); i++) {
			shapeRenderer.rect(level.spells.elementAt(i).hitbox.left - camera_rect.left, level.spells.elementAt(i).hitbox.top - camera_rect.top, level.spells.elementAt(i).hitbox.width, level.spells.elementAt(i).hitbox.height);
		}
		
		// Draw powerup hitboxes
		for(int i = 0; i < level.powerups.size(); i++) {
			shapeRenderer.rect(level.powerups.elementAt(i).hitbox.left - camera_rect.left, level.powerups.elementAt(i).hitbox.top - camera_rect.top, level.powerups.elementAt(i).hitbox.width, level.powerups.elementAt(i).hitbox.height);
		}
		shapeRenderer.end();
	}

	//------------------------------------------------------------------------------
	// drawWorld()
	//------------------------------------------------------------------------------	
	private void drawWorld(GameObject arr[][], float alpha) {
		
		// Set start position
		int start_map_x = camera_rect.left / GLOBAL.TILE_SIZE;
		int start_map_y = camera_rect.top / GLOBAL.TILE_SIZE;

		// Calculate end of map
		int end_map_x = start_map_x + (screen_width / GLOBAL.TILE_SIZE);
		int end_map_y = start_map_y + (screen_height / GLOBAL.TILE_SIZE);
		end_map_x++;
		end_map_y++;

		// Offset
		int offset_x =- (camera_rect.left % GLOBAL.TILE_SIZE);
		int offset_y =- (camera_rect.top % GLOBAL.TILE_SIZE);
		int xPos = offset_x;
		int yPos = offset_y;

		// H�r ritas allt ut
		for(int y = start_map_y; y <= end_map_y; y++) {
			for(int x = start_map_x; x <= end_map_x; x++) {

				// Draw this tile
				if(x > level.world_size_x - 1 || y > level.world_size_y - 1 || x < 0 || y < 0 || arr[x][y].sprites == null) {
					xPos += GLOBAL.TILE_SIZE;
				}
				else {
					region.setRegion(arr[x][y].sprites.elementAt(0).texture_x, arr[x][y].sprites.elementAt(0).texture_y, GLOBAL.TILE_SIZE, GLOBAL.TILE_SIZE);
					region.flip(false, true);
					batch.setColor(1, 1, 1, alpha);
					batch.draw(region, xPos, yPos);
					xPos += GLOBAL.TILE_SIZE;
				}
			} 

			// Nollst�ll X, uppdatera Y
			xPos = offset_x;
			yPos += GLOBAL.TILE_SIZE;
		}
	}
	
	private void drawWorldOutside(Level lev, float alpha) {
		
		// Set start position
		int start_map_x = camera_rect.left / GLOBAL.TILE_SIZE;
		int start_map_y = camera_rect.top / GLOBAL.TILE_SIZE;

		// Calculate end of map
		int end_map_x = start_map_x + (screen_width / GLOBAL.TILE_SIZE);
		int end_map_y = start_map_y + (screen_height / GLOBAL.TILE_SIZE);
		end_map_x++;
		end_map_y++;

		// Offset
		int offset_x =- (camera_rect.left % GLOBAL.TILE_SIZE);
		int offset_y =- (camera_rect.top % GLOBAL.TILE_SIZE);
		int xPos = offset_x;
		int yPos = offset_y;

		// H�r ritas allt ut
		for(int y = start_map_y; y <= end_map_y; y++) {
			for(int x = start_map_x; x <= end_map_x; x++) {

				// Draw this tile
				if(x > level.world_size_x - 1 || y > level.world_size_y - 1 || x < 0 || y < 0) {
					region.setRegion(lev.null_sprite.texture_x, lev.null_sprite.texture_y, GLOBAL.TILE_SIZE, GLOBAL.TILE_SIZE);
					region.flip(false, true);
					batch.setColor(1, 1, 1, alpha);
					batch.draw(region, xPos, yPos);
					xPos += GLOBAL.TILE_SIZE;
				}
				else {
					region.setRegion(lev.world[x][y].sprites.elementAt(0).texture_x, lev.world[x][y].sprites.elementAt(0).texture_y, GLOBAL.TILE_SIZE, GLOBAL.TILE_SIZE);
					region.flip(false, true);
					batch.setColor(1, 1, 1, alpha);
					batch.draw(region, xPos, yPos);
					xPos += GLOBAL.TILE_SIZE;
				}
			} 

			// Nollst�ll X, uppdatera Y
			xPos = offset_x;
			yPos += GLOBAL.TILE_SIZE;
		}
	}
	
	//------------------------------------------------------------------------------
	// drawPlayer()
	//------------------------------------------------------------------------------	
	private void drawPlayer() {

		// The player
		for(int n = 0; n < level.player.sprites.size(); n++) {
			
			region.setRegion(level.player.sprites.elementAt(n).texture_x, 
					level.player.sprites.elementAt(n).texture_y, 
					level.player.sprites.elementAt(n).size_x, 
					level.player.sprites.elementAt(n).size_y);
			
			region.flip(false, true);
			batch.setColor(1, 1, 1, level.player.sprites.elementAt(n).alpha);
			
			batch.draw(region,
					(int)level.player.coordinates.x - camera_rect.left + level.player.sprites.elementAt(n).relative_x, 
					(int)level.player.coordinates.y - camera_rect.top + level.player.sprites.elementAt(n).relative_y, 
					level.player.sprites.elementAt(n).size_x / 2, 
					level.player.sprites.elementAt(n).size_y / 2, 
					region.getRegionWidth(), 
					region.getRegionHeight(), 
					level.player.sprites.elementAt(n).scale_x, 
					level.player.sprites.elementAt(n).scale_y, 
					(float) level.player.sprites.elementAt(n).rotation);
		}
	
	}
	
	//------------------------------------------------------------------------------
	// drawPlayer() - Med global alpha, s� man kan k�ra fade in/out
	//------------------------------------------------------------------------------	
	private void drawPlayer(float global_alpha) {

		// The player
		for(int n = 0; n < level.player.sprites.size(); n++) {
			
			region.setRegion(level.player.sprites.elementAt(n).texture_x, 
					level.player.sprites.elementAt(n).texture_y, 
					level.player.sprites.elementAt(n).size_x, 
					level.player.sprites.elementAt(n).size_y);
			
			region.flip(false, true);
			batch.setColor(1, 1, 1, global_alpha);
			
			batch.draw(region,
					(int)level.player.coordinates.x - camera_rect.left + level.player.sprites.elementAt(n).relative_x, 
					(int)level.player.coordinates.y - camera_rect.top + level.player.sprites.elementAt(n).relative_y, 
					level.player.sprites.elementAt(n).size_x / 2, 
					level.player.sprites.elementAt(n).size_y / 2, 
					region.getRegionWidth(), 
					region.getRegionHeight(), 
					level.player.sprites.elementAt(n).scale_x, 
					level.player.sprites.elementAt(n).scale_y, 
					(float) level.player.sprites.elementAt(n).rotation);
		}
	
	}
	
	//------------------------------------------------------------------------------
	// dispose()
	//------------------------------------------------------------------------------	
	public void dispose() {
		batch.dispose();
		texture.dispose();
		shapeRenderer.dispose();
		timefont.dispose();
		levelfont.dispose();
	//	light.dispose();
	//	lightShader.dispose();
	//	defaultShader.dispose();
	//	ambientShader.dispose();
	//	finalShader.dispose();
	//	fbo.dispose();
	}
	
	public void resize(int h, int w) {
		screen_width = Gdx.graphics.getWidth();
		screen_height = Gdx.graphics.getHeight();
		camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		camera.setToOrtho(true);
		camera.update();
		batch.setProjectionMatrix(camera.combined);
		shapeRenderer.setProjectionMatrix(camera.combined);
	}
	
	public void drawInGameUI(float global_alpha) {
		
		// Draw UI background
		region.setRegion(775, 1650, 315, 100);
		region.flip(false, true);
		batch.setColor(1, 1, 1, global_alpha);
		batch.draw(region, 0, 0);
		
		// Draw sound UI icon, bottom left
		if(GLOBAL.sound == 1)
			region.setRegion(192, 192, GLOBAL.TILE_SIZE, GLOBAL.TILE_SIZE);
		else
			region.setRegion(256, 192, GLOBAL.TILE_SIZE, GLOBAL.TILE_SIZE);
		region.flip(false, true);
		batch.setColor(1, 1, 1, global_alpha);
		batch.draw(region, 0, screen_height - GLOBAL.TILE_SIZE);
		
		// Draw Text
		levelfont.setColor(1, 1, 1, global_alpha);
		timefont.setColor(1, level.flashing, level.flashing, global_alpha);
		levelfont.draw(batch, level.currlev, 5, 5);
		levelfont.draw(batch, GLOBAL.coins_string, 30, 40);
		levelfont.draw(batch, GLOBAL.life_string, 30, 63);
        timefont.draw(batch, level.timestring, 145, 10);
	}
	
	// LEVEL
	public Level level;
	
	// CAMERA
	public Hitbox camera_rect;
	
	// DRAWING AND TEXTURE
	private Texture texture;
	private SpriteBatch batch;
	private TextureRegion region;
	private OrthographicCamera camera;
	private ShapeRenderer shapeRenderer;
	
	// FONTS
	private BitmapFont timefont;
	private BitmapFont levelfont;
	
	
	// EDITOR
	private Boolean grid_active;
	private Boolean showing_hitboxes;
	private GameObject selected_editor_object;
	
	public int screen_width;
	public int screen_height;


	
//	private Texture light = new Texture("data/light.png");
//	private FrameBuffer fbo = new FrameBuffer(Format.RGBA8888, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false);
//	private ShaderProgram lightShader;
//	private ShaderProgram defaultShader;
//	private ShaderProgram ambientShader;
//	private ShaderProgram finalShader;
	
//	final String vertexShader = new FileHandle("data/vertexShader.glsl").readString();
//	final String defaultPixelShader = new FileHandle("data/defaultPixelShader.glsl").readString();
//	final String lightPixelShader =  new FileHandle("data/lightPixelShader.glsl").readString();
//	final String ambientPixelShader = new FileHandle("data/ambientPixelShader.glsl").readString();
//	final String finalPixelShader =  new FileHandle("data/pixelShader.glsl").readString();
	
//	public static final float ambientIntensity = 0;
//	public static final Vector3 ambientColor = new Vector3(0.3f, 0.3f, 0.7f);
	


}
