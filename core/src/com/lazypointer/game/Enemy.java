package com.lazypointer.game;

public abstract class Enemy extends GameObject {

	private static final long serialVersionUID = 6674725972503694777L;

	Enemy(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
	}
	
}
