package com.lazypointer.game;

import java.io.Serializable;
import java.util.Vector;

public class Event implements Serializable {

	private static final long serialVersionUID = 7821163641499401371L;
	Event() {
		ids = new Vector<String>();
		action = 0;
		objects = new Vector<GameObject>();
		waypoints = new Vector<Waypoint>();
		string_data =  new Vector<String>();
		number_data = 0;
	}
	
	public Vector<String> ids;					// Object(s) that should recieve this event
	public int action;							// The action(s) to preform
	public Vector<String> string_data;				// If any additional string data
	public double number_data;					// If any additional number data
	public Vector<GameObject> objects;			// If the action involves some game-object(s).
	public Vector<Waypoint> waypoints;			// If waypoints are needed.
}

