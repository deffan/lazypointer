package com.lazypointer.game;

import java.io.Serializable;
import java.util.Vector;

public class EventManager implements Serializable {
	
	private static final long serialVersionUID = 4043439707158763174L;
	//------------------------------------------------------------------------------
	// EventManager() - Constructor
	//------------------------------------------------------------------------------
	EventManager(Vector<Spell> spells) {
		events = new Vector<Event>();
		receivers = new Vector<EventReceiver>();
		objectpool = new ObjectPool(this, spells);
	}
	
	//------------------------------------------------------------------------------
	// addEvent()
	//------------------------------------------------------------------------------
	public void addEvent(Event event) {
		events.add(event);
	}
	
	//------------------------------------------------------------------------------
	// sendEvents() - Sends the current event(s) to the object(s) that should receieve it/them
	//------------------------------------------------------------------------------
	public void sendEvents() {
		
		if(events.size() > 0) {
			for(int i = 0; i < events.size(); i++) {
				for(int j = 0; j < receivers.size(); j++) {
					for(int n = 0; n < events.elementAt(i).ids.size(); n++) {
						if(receivers.elementAt(j).id.equals(events.elementAt(i).ids.elementAt(n))) {
							receivers.elementAt(j).listener.event_queue.add(events.elementAt(i));
						}
					}
				}
			}
			events.clear();
		}
	}
	
	//------------------------------------------------------------------------------
	// addListener()
	//------------------------------------------------------------------------------	
	public void addListener(String id, Listener listener) {
		receivers.add(new EventReceiver(id, listener));
	}

	// DATAMEMBERS
	private Vector<Event> events;
	private Vector<EventReceiver> receivers;
	
	// ObjectPool
	public ObjectPool objectpool;
	
}
