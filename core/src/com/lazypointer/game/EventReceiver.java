package com.lazypointer.game;

import java.io.Serializable;

public class EventReceiver implements Serializable {

	private static final long serialVersionUID = -960496734181609479L;
	public EventReceiver(String id2, Listener listener2) {
		id = id2;
		listener = listener2;
	}
	public String id;
	public Listener listener;
}
