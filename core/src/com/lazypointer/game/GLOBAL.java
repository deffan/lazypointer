package com.lazypointer.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;

public class GLOBAL {
	
	// Initiate and load sounds
	public static void init_sound() {
	
		try 
		{
			sound_fail = Gdx.audio.newSound(Gdx.files.internal("data/sound/fail.wav"));
			sound_coin = Gdx.audio.newSound(Gdx.files.internal("data/sound/coin.wav"));
			sound_success = Gdx.audio.newSound(Gdx.files.internal("data/sound/success.wav"));
			sound_life = Gdx.audio.newSound(Gdx.files.internal("data/sound/life.wav"));
			sound_damage = Gdx.audio.newSound(Gdx.files.internal("data/sound/damage.wav"));
			sound_time = Gdx.audio.newSound(Gdx.files.internal("data/sound/time.wav"));
			music = Gdx.audio.newMusic(Gdx.files.internal("data/sound/happy.mp3"));
			music.setLooping(true);
			music.setVolume(0.7f);
			soundActive = true;
		}
		catch(Exception e) {
			soundActive = false;
			e.printStackTrace();
		}

	}
	
	// Unload sounds
	public static void dispose() {
		if(soundActive) {
			sound_fail.dispose();
			sound_success.dispose();
			sound_damage.dispose();
			sound_coin.dispose();
			sound_life.dispose();
			sound_time.dispose();
			music.dispose();
		}
	}
	
	// SOUNDS
	public static Sound sound_fail;
	public static Sound sound_success;
	public static Sound sound_damage;
	public static Sound sound_coin;
	public static Sound sound_life;
	public static Sound sound_time;
	public static Music music;
	public static Boolean soundActive;
	
	// FILES
	public final static String main_texture = "data/boxdev.png";
	public final static String objectlist_file = "editordata/objectlist.txt";
	public final static String testlevel_file = "data/level/5.lvl";
	public final static int DESKTOP_PLATFORM = 0;
	public final static int ANDROID_PLATFORM = 1;
	
	// GAME STATE / GAME STATISTICS / STATIC GAME DATAMEMBERS
	public static int current_level = 0;
	public static int current_state = 0;
	public static int current_platform = 0;
	public static int coins = 0;
	public static String coins_string = "0";
	public static int time = 0;
	public static int immortal = 0;
	public static int life = 1;
	public static String life_string = "1";
	public static double safespot_x = 0;
	public static double safespot_y = 0;
	public static int player_hit_by = 0;
	public static int sound = 1;
	
	// GLOBALS FOR THE GAME SCREEN / CAMERA / TILE SIZE etc..
	public final static int TILE_SIZE = 64;
	public final static int MAX_SPEED = 7;
	public final static int GRAVITY = 1;
	
	// DIRECTIONS
	public final static int LEFT = 0;
	public final static int RIGHT = 1;
	public final static int UP = 2;
	public final static int DOWN = 3;
	public final static int JUMP = 4;
	public final static int VERTICAL = 5;
	public final static int HORIZONTAL = 6;
	public final static int RANDOMLY = 7;
	
	// LOG MESSAGETYPES
	public final static int MSG_DEFAULT = 0;
	public final static int MSG_DEFAULT_BOLD = 1;
	public final static int MSG_ERROR = 2;
	public final static int MSG_INFORMATION = 3;
	public final static int MSG_WARNING = 4;
	public final static int MSG_EVENT = 5;
	
	// UI
	public final static int UI_LEFT = 0;
	public final static int UI_RIGHT = 1;
	public final static int UI_TOP = 2;
	public final static int UI_BOTTOM = 3;
	public final static int UI_CENTER_VERTICAL = 4;
	public final static int UI_CENTER_HORIZONTAL = 5;
	public final static int UI_LIST = 6;
	public final static int UI_GRID = 7;
	
	// LAYER POSITIONS (for drawing on the screen)
	public final static int BACKGROUND_LAYER = 0;
	public final static int FOREGROUND_LAYER = 1;
	public final static int NORMAL_LAYER = 2;
	
	// COLLISION TYPES
	public final static int COLLISION_BOX = 0;
	public final static int COLLISION_TRIANGLE_LEFT_BOTTOM = 1;
	public final static int COLLISION_TRIANGLE_LEFT_TOP = 2;
	public final static int COLLISION_TRIANGLE_RIGHT_BOTTOM  = 3;
	public final static int COLLISION_TRIANGLE_RIGHT_TOP  = 4;
	public final static int COLLISION_CIRCLE  = 5;

	// GAME/EDITOR STATES
	public final static int STATE_PLAY = 0;
	public final static int STATE_EDITOR_NORMAL = 1;
	public final static int STATE_EDITOR_FOREGROUND = 2;
	public final static int STATE_EDITOR_BACKGROUND = 3;
	public final static int STATE_EDITOR_OBJECT = 4;
	public final static int STATE_MAINMENU = 5;
	public final static int STATE_SUCCESS = 6;
	public final static int STATE_FAILURE = 7;
	public final static int STATE_FADEOUT = 8;
	public final static int STATE_FADEIN = 9;
	public final static int STATE_PAUSE = 10;
	
	// OBJECTS
	public final static int PLAYER = 1337;
	public final static int OBJECT_LIFEGAIN = -3;
	public final static int OBJECT_LIFELOSS = -2;
	public final static int OBJECT_GENERATOR = -1;
	public final static int OBJECT_DEFAULT = 0;
	public final static int OBJECT_MOVING_BLOCK = 1;
	public final static int OBJECT_DARK_PIT = 2;
	public final static int OBJECT_PORTAL = 3;
	public final static int OBJECT_TRIGGER = 4;
	public final static int OBJECT_TRIGGER_VISUAL = 5;
	public final static int OBJECT_DOOR = 6;
	public final static int OBJECT_ROTATING_SPIKE = 7;
	public final static int OBJECT_ROTATING_SPIKE_VER = 8;
	public final static int OBJECT_ROTATING_SPIKE_HOR = 9;
	public final static int OBJECT_ROTATING_SPIKE_RANDOM = 10;
	public final static int OBJECT_MINE = 11;
	public final static int OBJECT_CANNON_LEFT = 12;
	public final static int OBJECT_CANNON_RIGHT = 13;
	public final static int OBJECT_CANNON_UP = 14;
	public final static int OBJECT_CANNON_DOWN = 15;
	public final static int OBJECT_CANNON_AIMBOT = 16;
	public final static int OBJECT_FOUR_CANNON = 17;
	public final static int OBJECT_FOUR_CANNON_ROTATING = 18;
	public final static int OBJECT_FOUR_CANNON_BOUNCE = 19;
	public final static int OBJECT_CIRCULAR_STATIC = 20;
	public final static int OBJECT_CIRCULAR_MOVING = 21;
	public final static int OBJECT_LAVA = 22;
	public final static int OBJECT_WHIRLWIND = 23;
	public final static int OBJECT_SLIMEBALL = 24;
	public final static int OBJECT_SPIKE_LEFT = 25;
	public final static int OBJECT_SPIKE_RIGHT = 26;
	public final static int OBJECT_SPIKE_UP = 27;
	public final static int OBJECT_SPIKE_DOWN = 28;
	public final static int OBJECT_SKULLGHOST = 29;
	public final static int OBJECT_SNAKE = 30;
	public final static int OBJECT_FIRESNAKE = 31;
	public final static int OBJECT_FLAMETHROWER = 32;
	public final static int OBJECT_VIRUS = 33;
	public final static int OBJECT_DARK_PIT_FADER = 34;
	public final static int OBJECT_FINISHLINE = 35;
	public final static int OBJECT_TRIGGER_ADDER = 36;
	public final static int OBJECT_TRIGGER_REMOVER = 37;
	public final static int OBJECT_TRIGGER_ACTIVATOR = 38;
	public final static int OBJECT_POWERUP_COIN = 39;
	public final static int OBJECT_POWERUP_TIME_10 = 40;
	public final static int OBJECT_POWERUP_TIME_20 = 41;
	public final static int OBJECT_POWERUP_TIME_30 = 42;
	public final static int OBJECT_POWERUP_TIME_40 = 43;
	public final static int OBJECT_POWERUP_TIME_50 = 44;
	public final static int OBJECT_POWERUP_TIME_60 = 45;
	public final static int OBJECT_POWERUP_TIME_120 = 46;
	public final static int OBJECT_POWERUP_TIME_180 = 47;
	public final static int OBJECT_POWERUP_LIFE = 48;
	public final static int OBJECT_MOVINGFLOOR_LEFT = 49;
	public final static int OBJECT_MOVINGFLOOR_RIGHT = 50;
	public final static int OBJECT_MOVINGFLOOR_UP = 51;
	public final static int OBJECT_MOVINGFLOOR_DOWN = 52;
	public final static int OBJECT_MOVINGFLOOR_LEFT_A = 53;
	public final static int OBJECT_MOVINGFLOOR_RIGHT_A = 54;
	public final static int OBJECT_MOVINGFLOOR_UP_A = 55;
	public final static int OBJECT_MOVINGFLOOR_DOWN_A = 56;
	public final static int OBJECT_JUMPER = 57;
	public final static int OBJECT_DESTROYABLE = 58;
	public final static int OBJECT_PUSHABLE = 59;
	public final static int OBJECT_JUMPER_FOLLOW = 60;
	
	// SPELLS
	public final static int SPELL_FIREBALL = 2000;
	public final static int SPELL_FIRE_EXPLOSION = 2001;
	public final static int SPELL_SLIME = 2002;
	
	// ACTIONS
	public final static int ACTION_MOVE = 0;
	public final static int ACTION_ATTACK = 1;
	public final static int ACTION_DESTROY = 2;
	public final static int ACTION_OPEN = 3;
	public final static int ACTION_PLAYER_DEATH = 7;
	public final static int ACTION_TELEPORT = 8;
	public final static int ACTION_TRIGGER = 9;
	public final static int ACTION_FIREBALL = 10;
	public final static int ACTION_FIREBALL_EXPLOSION = 11;
	public final static int ACTION_SLIME = 12;
	public final static int ACTION_PITFADE = 13;
	public final static int ACTION_REMOVEME = 14;
	
	public static void saveCurrentProgress() {
		
		if(current_platform == ANDROID_PLATFORM) {
			Preferences prefs = Gdx.app.getPreferences("lazypointerprogress");
			prefs.putInteger("level", current_level);
			prefs.putInteger("coins", coins);
			prefs.putInteger("life", life);
			prefs.flush();
		}
		else {
			try {
			    FileHandle progressfile = Gdx.files.local("data/progress.lp");
			    String t = Integer.toString(current_level) + ";" + Integer.toString(coins) + ";" + Integer.toString(life);
			    progressfile.writeString(t, false);

			}
			catch(Exception e) {}
		}
	}
	
	public static void updateCoinString() {
		
		if(coins > 99) {
			life++;
			coins = 0;
			updateLifeString();
		}
		coins_string = Integer.toString(coins);
	}
	
	public static void updateLifeString() {
		life_string = Integer.toString(life);
	}
	
	public static void playerHit(int hit_by) {
		
		// If not currently immortal, we take damage
		if(immortal == 0) 
		{
			immortal = 3;
			player_hit_by = hit_by;
			life--;
			updateLifeString();

			// We died
			if(life <= 0) {
				current_state = STATE_FAILURE;
				if(soundActive) {
					music.stop();
					sound_fail.play(sound);
				}
			}
			else {	// Took damage
				if(soundActive)
					sound_damage.play(sound);
			}
				
		}
	}
}
