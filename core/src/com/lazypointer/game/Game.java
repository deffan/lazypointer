package com.lazypointer.game;

import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Vector;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;


public class Game implements Serializable {

	private static final long serialVersionUID = 8954498820756043406L;

	/**
	* Game constructor
	*/
	Game(int platform) {
		
		// Initiate various
		GLOBAL.current_state = GLOBAL.STATE_MAINMENU;
		prev_state = GLOBAL.STATE_PLAY;
		next_state = GLOBAL.STATE_PLAY;
		editor_snap = false;
		mouse_x = 0;
		mouse_y = 0;
		menuposplay = 1654;
		menuposexit = 1735;
		camera_size_x = Gdx.graphics.getWidth();
		camera_size_y = Gdx.graphics.getHeight();
		messages = new Vector<String>();
		adOn = false;
		GLOBAL.current_platform = platform;
		
		// Selected & "default" object(s) [EDITOR ONLY]
		selected_editor_object_default = new Tile(new Coordinate(0,0), new Hitbox(0,0,0,0), new Sprite(0,0,1,0), false);
		selected_editor_object_default.ID = "DEFAULT";
		selected_editor_object = selected_editor_object_default;
		selected_editor_object.ID = "DEFAULT";
		
		// CollisionInformation for each possible way the player can collide and get "squished"
		player_world_collision = new CollisionInformation();
		
		// Initiate camera 
		camera_rect = new Hitbox(0, camera_size_x, 0, camera_size_y);

		if(false) {
			level = new Level();
			level.newEmptyLevel(10, 10);
			level.null_sprite.texture_x = 0;
			level.null_sprite.texture_y = 0;
		//	level.null_sprite.texture_x = 1024;
		//	level.null_sprite.texture_y = 128;
			GLOBAL.time = 181;
			level.start_time = GLOBAL.time;
		}
		else
			loadLevel(GLOBAL.testlevel_file);
		
		// The collision object
		collision = new Collision(level.world);
			
		// Drawing object
		draw = new Draw(level, camera_rect);
		draw.setSelected(selected_editor_object);
		
		click = false;
		global_alpha = 1;
		
		// Show some information in the log
		messages.add(GLOBAL.MSG_INFORMATION + "Tiles(x,y):" + level.world_size_x + "," + level.world_size_y);
		messages.add(GLOBAL.MSG_INFORMATION + "Pixels(x,y): " + level.world_size_x * GLOBAL.TILE_SIZE + "," + level.world_size_y * GLOBAL.TILE_SIZE);
		messages.add(GLOBAL.MSG_INFORMATION + "Display(x,y): " + camera_size_x + "," + camera_size_y);
		
		// Sound
		GLOBAL.init_sound();
	}
	
	public Boolean loadLevel(String lvl) {
		
		try {
			
			// ObjectInputStream
			ObjectInputStream in = new ObjectInputStream(Gdx.files.internal(lvl).read());
			
			// Try loading in the level
			try {
				level = (Level) in.readObject();
			} catch (Exception e) {
				in.close();
				e.printStackTrace();
				System.err.println("Serialization error in loadLevel().");
				messages.add(GLOBAL.MSG_ERROR + "ERROR IN game.loadLevel(), inner try");
				return false;
			}
			in.close();
			
			
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Serialization error in loadLevel().");
			messages.add(GLOBAL.MSG_ERROR + "ERROR IN game.loadLevel(), outer try");
			return false;
		}
		return true;
	}
	
	public void loadLevel(int lvlnumber) {
		
		try {
			// ObjectInputStream
			ObjectInputStream in = new ObjectInputStream(Gdx.files.internal("data/level/" + Integer.toString(lvlnumber) + ".lvl").read());
			
			// Try loading in the level
			try {
				level = (Level) in.readObject();
			} catch (Exception e) {
				in.close();
				e.printStackTrace();
				GLOBAL.current_state = GLOBAL.STATE_MAINMENU;
				global_alpha = 1;
				menuposplay = 1654;
				menuposexit = 1735;
			}
			in.close();
			
		} catch (Exception e) {
			e.printStackTrace();
			GLOBAL.current_state = GLOBAL.STATE_MAINMENU;
			global_alpha = 1;
			menuposplay = 1654;
			menuposexit = 1735;
		}
		
		// Set/Reset some stuff
		level.reset();
		draw.level = level;
		collision.world = level.world;
		level.player.coordinates.x = level.player_start_x;
		level.player.coordinates.y = level.player_start_y;
		camera_rect.left = (int)level.player.coordinates.x - (draw.screen_width / 2);
		camera_rect.top = (int)level.player.coordinates.y - (draw.screen_height / 2);
		if(camera_rect.left <= 0)
			camera_rect.left = 0;
		if(camera_rect.top <= 0)
			camera_rect.top = 0;
		GLOBAL.updateCoinString();
		GLOBAL.updateLifeString();
		GLOBAL.safespot_x = level.player_start_x;
		GLOBAL.safespot_y = level.player_start_y;
		level.player.setMoving(false, -100, -100);
		GLOBAL.immortal = 0;
	}
	
	public void getCurrentProgress() {
		
		if(GLOBAL.current_platform == GLOBAL.ANDROID_PLATFORM) {
			Preferences prefs = Gdx.app.getPreferences("lazypointerprogress");
			GLOBAL.current_level = prefs.getInteger("level");
			GLOBAL.coins = prefs.getInteger("coins");
			GLOBAL.life = prefs.getInteger("life");
		}
		else {
			try {
				FileHandle progressfile = Gdx.files.internal("data/progress.lp");
				String data = progressfile.readString();
				int pos1 = data.indexOf(';');
				int pos2 = data.indexOf(';', pos1 + 1);
				GLOBAL.current_level = Integer.parseInt(data.substring(0, pos1));
				GLOBAL.coins = Integer.parseInt(data.substring(pos1 + 1, pos2));
				GLOBAL.life = Integer.parseInt(data.substring(pos2 + 1, data.length()));
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		if(GLOBAL.current_level == 0)
			GLOBAL.current_level = 1;
		if(GLOBAL.life == 0)
			GLOBAL.life = 1;
	}
	
	/**
	* <b>initGame() - </b> Load textures and other stuff needed for the game.
	* @return true/false
	*/
	public Boolean initGame() {

		return draw.initDraw();
	}
	
	/**
	* <b>dispose()</b>
	*/
	public void dispose() {
		draw.dispose();
		GLOBAL.dispose();
	}

	public void logic() {

		// Send events
		level.level_event_manager.sendEvents();
		
		// Run logic for these objects
		level.player.logic();
		level.logic();
		
		// Spell logic
		int spelllogic = level.spells.size();
		for(int i = 0; i < spelllogic; i++) {
			if(level.spells.get(i).alive) {
				level.spells.get(i).logic();
				level.spells.get(i).updateParticles();
			}
		}
		
		// back logic
		int backlogic = level.backObjects.size();
		for(int i = 0; i < backlogic; i++) {
			level.backObjects.get(i).logic();
		}
		
		// Powerup logic
		int poweruplogic = level.powerups.size();
		for(int i = 0; i < poweruplogic; i++) {
			level.powerups.get(i).logic();
		}

		// If player ends up outside the world for some buggy reason, move it to a safer spot.
		if(outside_world(level.player)) {
			level.player.coordinates.x = level.player_start_x;
			level.player.coordinates.y = level.player_start_y;
			level.player.hitbox.updateHitbox(level.player_start_x, level.player_start_y);
			System.out.println("ERROR: Player outside world! Moving to starting position...");
		}

		// player vs world collision
		player_world_collision = collision.collision_world(level.player);

		// Objects
		int objsize = level.objects.size();
		for(int i = 0; i < objsize; i++) {
			
			// If object outside screen, do nothing.
			if(!collision.is_within(level.objects.elementAt(i).hitbox, camera_rect, 384))
				continue;
			
			// If this object ended up outside the world for some buggy reason... remove it.
			if(outside_world(level.objects.elementAt(i))) {
				Event ev = new Event();
				ev.action = GLOBAL.ACTION_REMOVEME;
				ev.objects.add(level.objects.elementAt(i));
				ev.ids.add("GENERATOR");
				level.level_event_manager.addEvent(ev);
				System.out.println("ERROR: Object outside world! Removing ID: " + level.objects.elementAt(i).ID + " TYPE: " + level.objects.elementAt(i).TYPE);
				continue;
			}
			
			// Normal collisions for player against objects
			if(level.objects.elementAt(i).collision) {
				
				
				// Object VS Object
				

				// Object VS Spells
				int spellsizes = level.spells.size();
				for(int c = 0; c < spellsizes; c++) {
					if(level.spells.elementAt(c).alive)
						if(level.spells.elementAt(c).collision && 
								level.objects.elementAt(i).collision && 
								level.spells.elementAt(c).from_object != level.objects.elementAt(i).TYPE)
							if(collision.box_collision(level.objects.elementAt(i), level.spells.get(c))) {
								level.objects.elementAt(i).collision(level.spells.get(c), null);
								level.spells.get(c).collision(null, null);
							}
				}

				// Object vs World
				CollisionInformation c = collision.collision_world(level.objects.elementAt(i));
				if(c.left || c.right || c.up || c.down) {
					level.objects.elementAt(i).collision(null, c);
				}
				
				// Player vs object
				if(GLOBAL.immortal == 0)
					if(collision.box_collision(level.player, level.objects.elementAt(i))) {
						
						// Physical collisions
						collision.collision_object(level.player, level.objects.elementAt(i));
						level.objects.elementAt(i).collision(level.player, null);
					}
				
			}
			
			// For detecting collisions that are not "physical"
			if(level.objects.elementAt(i).collision_event)
				if(collision.box_collision(level.player, level.objects.elementAt(i)))
					level.objects.elementAt(i).collision(level.player, null);

			
			// Update logic for each object
			level.objects.elementAt(i).logic();
		}
		
		// Spell collisions
		int spellsize = level.spells.size();
		for(int i = 0; i < spellsize; i++) {
			if(level.spells.elementAt(i).alive)
				if(!outside_world(level.spells.elementAt(i))) 
				{
					if(collision.collision_world_quick(level.spells.elementAt(i))) {
						level.spells.get(i).collision(null, null);
					}
					else if(collision.box_collision(level.player, level.spells.get(i))) {
						level.spells.get(i).collision(level.player, null);
					}
				}
				else
					level.spells.get(i).alive = false;
		}
		
		// Powerup collisions
		int powerupcollision = level.powerups.size();
		for(int i = 0; i < powerupcollision; i++)
			if(collision.box_collision(level.player, level.powerups.elementAt(i))) {
				level.powerups.elementAt(i).collision(level.player, null);
				level.player.collision(level.powerups.elementAt(i), null);
			}
				
		// The camera follows the player
		camera_rect.left = (int)level.player.coordinates.x - (draw.screen_width / 2);
		camera_rect.top = (int)level.player.coordinates.y - (draw.screen_height / 2);
		camera_rect.right = camera_rect.left + draw.screen_width;
		camera_rect.bottom = camera_rect.top + draw.screen_height;
		if(camera_rect.left <= 0)
			camera_rect.left = 0;
		if(camera_rect.top <= 0)
			camera_rect.top = 0;
	}

	public void draw() {
		draw.draw();
	}
	
	public void editor_draw(float alpha_normal, float alpha_background, float alpha_foreground, float alpha_object) {

		if(Gdx.input.isKeyPressed(Keys.TAB))
			draw.showTexture();
		else
			draw.editor_draw(alpha_normal, alpha_background, alpha_foreground, alpha_object);
	}
	
	public void mouse_input_editor(GameObject o, int xDown, int yDown, int layer, Vector<GameObject> event_object, Vector<Waypoint> event_waypoint) {
		
		// If no object, return
		if(o == null) {
			messages.add(GLOBAL.MSG_ERROR + "Trying to place NULL GameObject in mouse_input_editor()");
			return;
		}
		
		// Get the mouse coordinates
		mouse_x = Gdx.input.getX();
		mouse_y = Gdx.input.getY();
		
		// If we are choosing a texture
		if(Gdx.input.isKeyPressed(Keys.TAB)) {
			selected_editor_object.sprites.elementAt(0).texture_x = (((mouse_x) / GLOBAL.TILE_SIZE) * GLOBAL.TILE_SIZE);
			selected_editor_object.sprites.elementAt(0).texture_y = (((mouse_y) / GLOBAL.TILE_SIZE) * GLOBAL.TILE_SIZE);
		}

		// Get relative coordinates
		int mouse_relative_x_up = (camera_rect.left + mouse_x);
		int mouse_relative_y_up = (camera_rect.top + mouse_y);
		int mouse_relative_x_down = (camera_rect.left + xDown);
		int mouse_relative_y_down = (camera_rect.top + yDown);
		
		// Check if both UP and DOWN mouse coordinates are within the world
		if(mouse_relative_x_up / GLOBAL.TILE_SIZE >= 0 && 
				mouse_relative_x_up / GLOBAL.TILE_SIZE < level.world_size_x && 
				mouse_relative_y_up / GLOBAL.TILE_SIZE >= 0 && 
				mouse_relative_y_up  / GLOBAL.TILE_SIZE < level.world_size_y &&
				mouse_relative_x_down / GLOBAL.TILE_SIZE >= 0 && 
				mouse_relative_x_down / GLOBAL.TILE_SIZE < level.world_size_x && 
				mouse_relative_y_down / GLOBAL.TILE_SIZE >= 0 && 
				mouse_relative_y_down  / GLOBAL.TILE_SIZE < level.world_size_y
		) {

			// Exchange the X/Y values so they always remain the same regardless of direction of selection/dragging
			if(mouse_relative_x_up > mouse_relative_x_down) {
				int temp = mouse_relative_x_down;
				mouse_relative_x_down = mouse_relative_x_up;
				mouse_relative_x_up = temp;
			}
			
			if(mouse_relative_y_up > mouse_relative_y_down) {
				int temp = mouse_relative_y_down;
				mouse_relative_y_down = mouse_relative_y_up;
				mouse_relative_y_up = temp;
			}

			// Place items
			int xPos = 0;
			int yPos = 0;
			for (int y = mouse_relative_y_up; y <= mouse_relative_y_down; y++) {
				for (int x = mouse_relative_x_up; x <= mouse_relative_x_down; x++) {
					
					// Place a TILE 
					if(o.TYPE == GLOBAL.OBJECT_DEFAULT && layer != GLOBAL.STATE_EDITOR_OBJECT) {
						
						// If "Q" is pressed, we select the Tile here instead.
						if(Gdx.input.isKeyPressed(Keys.Q)) {
							selected_editor_object = level.world[x / GLOBAL.TILE_SIZE][y / GLOBAL.TILE_SIZE];
							draw.setSelected(selected_editor_object);
							return;
						}
						
						xPos = (x / GLOBAL.TILE_SIZE) * GLOBAL.TILE_SIZE;
						yPos = (y / GLOBAL.TILE_SIZE) * GLOBAL.TILE_SIZE;
						
						// Create a new GameObject here with the same sprite, and correct coordinates/hitbox
						Hitbox hitbox = new Hitbox(xPos, xPos + GLOBAL.TILE_SIZE, yPos, yPos + GLOBAL.TILE_SIZE);
						Coordinate coordinate = new Coordinate(xPos, yPos);
						Tile t = new Tile(coordinate, hitbox, o.sprites.elementAt(0), o.isCollidable());
						t.ID = o.ID;
						
						// Place in the correct layer
						if(layer == GLOBAL.STATE_EDITOR_NORMAL)
							level.world[x / GLOBAL.TILE_SIZE][y / GLOBAL.TILE_SIZE] = t;
						else if(layer == GLOBAL.STATE_EDITOR_FOREGROUND)
							level.foreground[x / GLOBAL.TILE_SIZE][y / GLOBAL.TILE_SIZE] = t;
						else if(layer == GLOBAL.STATE_EDITOR_BACKGROUND)
							level.background[x / GLOBAL.TILE_SIZE][y / GLOBAL.TILE_SIZE] = t;
						
					}// Place an OBJECT
					else if(layer == GLOBAL.STATE_EDITOR_OBJECT) {
						
						int xPos_obj = x;
						int yPos_obj = y;

						// If we are clicking on an already existing object, we select it
						GameObject temp = new Tile(new Coordinate(xPos_obj, yPos_obj), new Hitbox(xPos_obj, xPos_obj + 1, yPos_obj, yPos_obj + 1), null, true);
						
						// Trying to select the player?
						if(collision.is_within(temp, level.player)) {
							selected_editor_object = level.player;
							draw.setSelected(selected_editor_object);
							return;
						}
						
						// Trying to select an backobject?
						for(int i = 0; i < level.backObjects.size(); i++) {
							if(collision.is_within(temp, level.backObjects.elementAt(i))) {
								selected_editor_object = level.backObjects.elementAt(i);
								draw.setSelected(selected_editor_object);
								return;
							}
						}
						
						// Trying to select an object?
						for(int i = 0; i < level.objects.size(); i++) {
							if(collision.is_within(temp, level.objects.elementAt(i))) {
								selected_editor_object = level.objects.elementAt(i);
								draw.setSelected(selected_editor_object);
								return;
							}
						}
						
						// Trying to select a spell?
						for(int i = 0; i < level.spells.size(); i++) {
							if(collision.is_within(temp, level.spells.elementAt(i))) {
								selected_editor_object = level.spells.elementAt(i);
								draw.setSelected(selected_editor_object);
								return;
							}
						}
						
						// Trying to select a powerup?
						for(int i = 0; i < level.powerups.size(); i++) {
							if(collision.is_within(temp, level.powerups.elementAt(i))) {
								selected_editor_object = level.powerups.elementAt(i);
								draw.setSelected(selected_editor_object);
								return;
							}
						}
						
						// Will NOT place an object with the same ID...
						for(int i = 0; i < level.objects.size(); i++) {
							if(level.objects.elementAt(i).ID.equals(o.ID)) {
								messages.add(GLOBAL.MSG_WARNING + "OBJECT (" + o.ID + ") ALREADY EXISTS!");
								return;
							}
						}
						
						// Wrong object-type for this layer
						if(o.TYPE == GLOBAL.OBJECT_DEFAULT) {
							messages.add(GLOBAL.MSG_WARNING + "Trying to place a TILE in the OBJECT LAYER.");
							return;
						}
						
						// If the object should be snapped to the grid
						if(editor_snap) {
							xPos_obj = (x / GLOBAL.TILE_SIZE) * GLOBAL.TILE_SIZE;
							yPos_obj = (y / GLOBAL.TILE_SIZE) * GLOBAL.TILE_SIZE;
						}
						
						// Identify and create the correct object
						setGameObject(o, xPos_obj, yPos_obj, event_object, event_waypoint);
						
						// Only one object
						return;
					}
					else
						messages.add(GLOBAL.MSG_WARNING + "You cannot place that here.");
				}
			}
		}
	}
	
	private void setGameObject(GameObject o, int x, int y, Vector<GameObject> event_object, Vector<Waypoint> event_waypoint) {
		
		GameObject obj = null;
		Hitbox hitbox = new Hitbox(x, x + GLOBAL.TILE_SIZE, y, y + GLOBAL.TILE_SIZE);
		Coordinate coordinate = new Coordinate(x, y);
		Sprite sprite = new Sprite(o.sprites.elementAt(0).texture_x, o.sprites.elementAt(0).texture_y, 1, 0);

		switch(o.TYPE) {
		
			case GLOBAL.OBJECT_MOVING_BLOCK: {
				obj = new MovingBlock(coordinate, hitbox, sprite, true); 
				obj.ID = o.ID;
				obj.TYPE = o.TYPE;
				obj.hitbox.updateHitbox(x, y);
				obj.getWaypoints().addAll(event_waypoint);
				level.objects.add(obj);
				break;
			}
			
			case GLOBAL.OBJECT_DARK_PIT: {
				obj = new DarkPit(coordinate, hitbox, sprite, false); 
				obj.ID = o.ID;
				obj.TYPE = o.TYPE;
				obj.setCollidableEvent(true);
				obj.setEventCapable(level.level_event_manager);		// This object should send events
				obj.init();
				obj.event_objects.addAll(event_object);
				level.objects.add(obj);
				break;
			}
			
			case GLOBAL.OBJECT_PORTAL: {
				obj = new Portal(coordinate, hitbox, sprite, false);
				obj.ID = o.ID;
				obj.TYPE = o.TYPE;
				obj.setCollidableEvent(true);
				obj.setEventCapable(level.level_event_manager);		// This object should send events
				
				// Set portal data
				obj.getEventObjects().addAll(event_object);
				obj.init();
				level.objects.add(obj);
				break;
			}
			
			case GLOBAL.OBJECT_TRIGGER: 
			case GLOBAL.OBJECT_TRIGGER_VISUAL: 
			case GLOBAL.OBJECT_TRIGGER_ADDER: 
			case GLOBAL.OBJECT_TRIGGER_REMOVER:
			case GLOBAL.OBJECT_TRIGGER_ACTIVATOR: 
			{
				obj = new Trigger(coordinate, hitbox, sprite, false); 
				obj.ID = o.ID;
				obj.TYPE = o.TYPE;
				obj.setCollidableEvent(true);
				obj.setEventCapable(level.level_event_manager);		// This object should send events
				
				// Set objects that the trigger should be sent to
				obj.getEventObjects().addAll(event_object);
				obj.init();
				level.objects.add(obj);
				break;
			}
			
			case GLOBAL.OBJECT_DOOR: {
				
				obj = new Door(coordinate, hitbox, sprite, true); 
				obj.ID = o.ID;
				obj.TYPE = o.TYPE;
				obj.setCollidableEvent(false);
				obj.setEventCapable(level.level_event_manager);		// This object should recieve events
				
				// Add ONE waypoint for the door (unless the door should move more...)
				obj.getWaypoints().addAll(event_waypoint);
				level.objects.add(obj);
				break;
			}
			
			case GLOBAL.OBJECT_ROTATING_SPIKE:
			case GLOBAL.OBJECT_ROTATING_SPIKE_VER:
			case GLOBAL.OBJECT_ROTATING_SPIKE_HOR:
			case GLOBAL.OBJECT_ROTATING_SPIKE_RANDOM:
			{
				
				obj = new RotatingSpike(coordinate, hitbox, sprite, true); 
				obj.ID = o.ID;
				obj.TYPE = o.TYPE;
				obj.setCollidableEvent(true);
				obj.setEventCapable(level.level_event_manager);		// This object should recieve events
				((RotatingSpike) obj).setRotatingSpikeMovement(o.TYPE, true, 1);
				level.objects.add(obj);
				break;
			}
			
			case GLOBAL.OBJECT_MINE:
			{
				obj = new Mine(coordinate, hitbox, sprite, false); 
				obj.ID = o.ID;
				obj.TYPE = o.TYPE;
				obj.setCollidableEvent(true);
				obj.setEventCapable(level.level_event_manager);		// This object should send events
				obj.init();
				level.objects.add(obj);
				break;
			}
			
			case GLOBAL.OBJECT_CANNON_LEFT:
			case GLOBAL.OBJECT_CANNON_RIGHT:
			case GLOBAL.OBJECT_CANNON_UP:
			case GLOBAL.OBJECT_CANNON_DOWN:
			case GLOBAL.OBJECT_CANNON_AIMBOT:
			{
				obj = new Cannon(coordinate, hitbox, sprite, false); 
				obj.ID = o.ID;
				obj.TYPE = o.TYPE;
				obj.setEventCapable(level.level_event_manager);		// This object should send events
				((Cannon) obj).setPlayerPos(level.player);
				level.objects.add(obj);
				break;
			}
			
			case GLOBAL.OBJECT_FOUR_CANNON:
			case GLOBAL.OBJECT_FOUR_CANNON_ROTATING:
			case GLOBAL.OBJECT_FOUR_CANNON_BOUNCE:
			{
				obj = new FourCannon(coordinate, hitbox, sprite, false); 
				obj.ID = o.ID;
				obj.TYPE = o.TYPE;
				obj.setEventCapable(level.level_event_manager);		// This object should send events
				obj.init();
				level.objects.add(obj);
				break;
			}
			
			case GLOBAL.OBJECT_CIRCULAR_STATIC:
			case GLOBAL.OBJECT_CIRCULAR_MOVING:
			{
				obj = new Circular(coordinate, hitbox, sprite, false); 
				obj.ID = o.ID;
				obj.TYPE = o.TYPE;
				obj.setCollidableEvent(true);
				obj.setEventCapable(level.level_event_manager);		// This object should send events
				obj.getEventObjects().addAll(event_object);
				obj.init();
				level.objects.add(obj);
				break;
			}
			
			case GLOBAL.OBJECT_LAVA:
			{
				obj = new Lava(coordinate, hitbox, sprite, false); 
				obj.ID = o.ID;
				obj.TYPE = o.TYPE;
				obj.setEventCapable(level.level_event_manager);		// This object should send events
				obj.init();
				level.objects.add(obj);
				break;
			}
			
			case GLOBAL.OBJECT_WHIRLWIND:
			{
				obj = new Whirlwind(coordinate, hitbox, sprite, true); 
				obj.ID = o.ID;
				obj.TYPE = o.TYPE;
				obj.setCollidableEvent(true);
				obj.setEventCapable(level.level_event_manager);		// This object should send events
				obj.init();
				level.objects.add(obj);
				break;
			}
			
			case GLOBAL.OBJECT_SLIMEBALL:
			{
				obj = new SlimeBall(coordinate, hitbox, sprite, true); 
				obj.ID = o.ID;
				obj.TYPE = o.TYPE;
				obj.setCollidableEvent(true);
				obj.setEventCapable(level.level_event_manager);		// This object should send events
				((SlimeBall) obj).setSlimeData(level.player.coordinates, level);
				obj.init();
				level.objects.add(obj);
				break;
			}
			
			case GLOBAL.OBJECT_SPIKE_LEFT:
			case GLOBAL.OBJECT_SPIKE_RIGHT:
			case GLOBAL.OBJECT_SPIKE_UP:
			case GLOBAL.OBJECT_SPIKE_DOWN:
			{
				obj = new Spike(coordinate, hitbox, sprite, true); 
				obj.ID = o.ID;
				obj.TYPE = o.TYPE;
				obj.setCollidableEvent(true);
				obj.setEventCapable(level.level_event_manager);		// This object should send events
				((Spike) obj).setPlayerCoord(level.player.coordinates);
				obj.init();
				level.spells.add((Spell) obj);
				break;
			}
			
			case GLOBAL.OBJECT_SKULLGHOST:
			{
				obj = new SkullGhost(coordinate, hitbox, sprite, false); 
				obj.ID = o.ID;
				obj.TYPE = o.TYPE;
				obj.setCollidableEvent(true);
				obj.setEventCapable(level.level_event_manager);		// This object should send events
				((SkullGhost) obj).setPlayerCoord(level.player.coordinates);
				obj.init();
				level.objects.add(obj);
				break;
			}
			
			case GLOBAL.OBJECT_SNAKE:
			case GLOBAL.OBJECT_FIRESNAKE:
			{
				obj = new Snake(new Coordinate(x, y), new Hitbox(x, x + 60, y, y + 60), sprite, true);
				obj.ID = o.ID;
				obj.TYPE = o.TYPE;
				obj.setCollidableEvent(true);
				obj.setEventCapable(level.level_event_manager);	
				((Snake) obj).setPlayer(level.player);
				
				// Add 5 bodies
				for(int i = 0; i < 5; i++) {
					GameObject snakeBody = new Snake(new Coordinate(x, y), new Hitbox(x, x + 60, y, y + 60), sprite, true);
					((Snake) snakeBody).setPlayer(level.player);
					snakeBody.TYPE = o.TYPE;
					snakeBody.setCollidableEvent(true);
					snakeBody.setEventCapable(level.level_event_manager);	
					obj.event_objects.add(snakeBody);
					level.objects.add(snakeBody);
				}
				
				obj.init();
				level.objects.add(obj);
				break;
			}
			
			case GLOBAL.OBJECT_FLAMETHROWER:
			{
				obj = new Flamethrower(coordinate, hitbox, sprite, false); 
				obj.ID = o.ID;
				obj.TYPE = o.TYPE;
				obj.setCollidableEvent(true);
				obj.setEventCapable(level.level_event_manager);
				obj.init();
				level.objects.add(obj);
				break;
			}
			
			case GLOBAL.OBJECT_VIRUS:
			{
				obj = new Virus(coordinate, hitbox, sprite, false); 
				obj.ID = o.ID;
				obj.TYPE = o.TYPE;
				obj.setCollidableEvent(true);
				obj.setEventCapable(level.level_event_manager);
				((Virus) obj).setWorld(level);
				obj.init();
				level.objects.add(obj);
				break;
			}
			
			case GLOBAL.OBJECT_DARK_PIT_FADER:
			{
				obj = new DarkPit(coordinate, hitbox, sprite, false); 
				obj.ID = o.ID;
				obj.TYPE = o.TYPE;
				obj.setCollidableEvent(true);
				obj.setEventCapable(level.level_event_manager);
				obj.init();
				level.objects.add(obj);
				break;
			}
			
			case GLOBAL.OBJECT_FINISHLINE:
			{
				obj = new FinishLine(coordinate, hitbox, sprite, false); 
				obj.ID = o.ID;
				obj.TYPE = o.TYPE;
				obj.setCollidableEvent(true);
				obj.setEventCapable(level.level_event_manager);
				obj.init();
				level.objects.add(obj);
				break;
			}
			
			case GLOBAL.OBJECT_POWERUP_COIN:
			{
				obj = new Coin(coordinate, hitbox, sprite, false); 
				obj.ID = o.ID;
				obj.TYPE = o.TYPE;
				obj.setCollidableEvent(true);
				obj.setEventCapable(level.level_event_manager);
				obj.init();
				level.powerups.add((Powerup) obj);
				break;
			}
			
			case GLOBAL.OBJECT_POWERUP_TIME_10:
			case GLOBAL.OBJECT_POWERUP_TIME_20:
			case GLOBAL.OBJECT_POWERUP_TIME_30:
			case GLOBAL.OBJECT_POWERUP_TIME_40:
			case GLOBAL.OBJECT_POWERUP_TIME_50:
			case GLOBAL.OBJECT_POWERUP_TIME_60:
			case GLOBAL.OBJECT_POWERUP_TIME_120:
			case GLOBAL.OBJECT_POWERUP_TIME_180:
			{
				obj = new Time(coordinate, hitbox, sprite, false); 
				obj.ID = o.ID;
				obj.TYPE = o.TYPE;
				obj.setCollidableEvent(true);
				obj.setEventCapable(level.level_event_manager);
				obj.init();
				level.powerups.add((Powerup) obj);
				break;
			}
			
			case GLOBAL.OBJECT_POWERUP_LIFE:
			{
				obj = new Life(coordinate, hitbox, sprite, false); 
				obj.ID = o.ID;
				obj.TYPE = o.TYPE;
				obj.setCollidableEvent(true);
				obj.setEventCapable(level.level_event_manager);
				obj.init();
				level.powerups.add((Powerup) obj);
				break;
			}
			
			case GLOBAL.OBJECT_MOVINGFLOOR_LEFT:
			case GLOBAL.OBJECT_MOVINGFLOOR_RIGHT:
			case GLOBAL.OBJECT_MOVINGFLOOR_UP:
			case GLOBAL.OBJECT_MOVINGFLOOR_DOWN:
			{
				obj = new MovingFloor(coordinate, hitbox, sprite, false); 
				obj.ID = o.ID;
				obj.TYPE = o.TYPE;
				obj.setCollidableEvent(true);
				obj.setEventCapable(level.level_event_manager);
				obj.init();
				level.objects.add(obj);
				break;
			}
			
			case GLOBAL.OBJECT_MOVINGFLOOR_LEFT_A:
			case GLOBAL.OBJECT_MOVINGFLOOR_RIGHT_A:
			case GLOBAL.OBJECT_MOVINGFLOOR_UP_A:
			case GLOBAL.OBJECT_MOVINGFLOOR_DOWN_A:
			{
				obj = new MovingFloorAnimation(coordinate, hitbox, sprite, false); 
				obj.ID = o.ID;
				obj.TYPE = o.TYPE;
				obj.setCollidableEvent(true);
				obj.setEventCapable(level.level_event_manager);
				obj.init();
				level.backObjects.add(obj);
				break;
			}
			
			case GLOBAL.OBJECT_JUMPER:
			case GLOBAL.OBJECT_JUMPER_FOLLOW:
			{
				obj = new Jumper(coordinate, hitbox, sprite, true); 
				obj.ID = o.ID;
				obj.TYPE = o.TYPE;
				obj.setCollidableEvent(true);
				obj.setEventCapable(level.level_event_manager);
				obj.init();
				((Jumper) obj).player = level.player;
				((Jumper) obj).level = level;
				level.objects.add(obj);
				break;
			}
			
			case GLOBAL.OBJECT_DESTROYABLE:
			{
				obj = new DestroyableBlock(coordinate, hitbox, sprite, true); 
				obj.ID = o.ID;
				obj.TYPE = o.TYPE;
				obj.setCollidableEvent(true);
				obj.setEventCapable(level.level_event_manager);
				obj.init();
				level.objects.add(obj);
				break;
			}
			
			case GLOBAL.OBJECT_PUSHABLE:
			{
				obj = new PushableBlock(coordinate, hitbox, sprite, true); 
				obj.ID = o.ID;
				obj.TYPE = o.TYPE;
				obj.setCollidableEvent(true);
				obj.setEventCapable(level.level_event_manager);
				obj.init();
				level.objects.add(obj);
				break;
			}
			
			default: messages.add(GLOBAL.MSG_ERROR + "Unidentified object with type: " + o.TYPE);
		}
		
		
	}
	
	public void mouse_input(int mouse) {
		
		mouse_x = camera_rect.left + Gdx.input.getX();
		mouse_y = camera_rect.top + Gdx.input.getY();
		
		if(Gdx.input.getX() < 64 && Gdx.input.getY() > draw.screen_height - 64) 
		{
			if(mouse == GLOBAL.DOWN)
				if(GLOBAL.sound == 1) {
					GLOBAL.sound = 0;
					if(GLOBAL.soundActive)
						GLOBAL.music.pause();
				}
				else  {
					GLOBAL.sound = 1;
					if(GLOBAL.soundActive)
						GLOBAL.music.play();
				}
		}
		else
			level.player.setMoving(true, mouse_x, mouse_y);
	}
	
	public void mouse_input_menu() {
		
		mouse_x = Gdx.input.getX();
		mouse_y = Gdx.input.getY();
		click = true;
	}
	
	public void keyboard_input(int direction) {
		
		if(direction == GLOBAL.LEFT) 
			level.player.moveLeft();
		else if(direction == GLOBAL.RIGHT)
			level.player.moveRight();
		else if(direction == GLOBAL.UP)
			level.player.moveUp();
		else if(direction == GLOBAL.DOWN)
			level.player.moveDown();

		level.player.hitbox.updateHitbox(level.player.coordinates.x, level.player.coordinates.y);
	}
	
	public void keyboard_input_editor(int direction) {
		
	}
	
	public Boolean outside_world(GameObject o) {
		if(o.coordinates.x < 0 || o.coordinates.x + o.hitbox.width > level.world_size_x * GLOBAL.TILE_SIZE || o.coordinates.y < 0 || o.coordinates.y + o.hitbox.height > level.world_size_y * GLOBAL.TILE_SIZE)
			return true;
		return false;		
	}
	
	public void mainmenu() {
		
		if(click) 
		{
			click = false;
			int center_w = (draw.screen_width / 2) - 186;
			
			// PLAY
			if(mouse_x >= center_w && mouse_x <= center_w + 372 && mouse_y >= 240 && mouse_y <= 240 + 80) {
				menuposplay = 1816;
				prev_state = GLOBAL.current_state;
				GLOBAL.current_state = GLOBAL.STATE_FADEOUT;
				next_state = GLOBAL.STATE_PLAY;
			}	// EXIT
			else if(mouse_x >= center_w && mouse_x <= center_w + 372 && mouse_y >= 325 && mouse_y <= 325 + 80) {
				menuposexit = 1897;
				Gdx.app.exit();
			}
		}
		
		draw.draw_menu(global_alpha, 1484, menuposplay, 1484, menuposexit);
	}
	
	public void success() 
	{
		if(!adOn && handler != null) {
			adOn = true;
			handler.showAds(true);
		}
		
		if(click) 
		{
			click = false;
			int center_w = (draw.screen_width / 2) - 192;
			int center_h = (draw.screen_height / 2) - 195;
			
			// NEXT
			if(mouse_x >= center_w && mouse_x <= center_w + 372 && mouse_y >= center_h + 185 && mouse_y <= center_h + 285) {
				prev_state = GLOBAL.current_state;
				GLOBAL.current_state = GLOBAL.STATE_FADEOUT;
				next_state = GLOBAL.STATE_PLAY;
				adOn = false;
			}	// EXIT
			else if(mouse_x >= center_w && mouse_x <= center_w + 372 && mouse_y >= center_h + 286 && mouse_y <= center_h + 385) {
				prev_state = GLOBAL.current_state;
				GLOBAL.current_state = GLOBAL.STATE_FADEOUT;
				next_state = GLOBAL.STATE_MAINMENU;
				menuposplay = 1654;
				menuposexit = 1735;
				adOn = false;
			}
		}	
		draw.draw_success(global_alpha, 0, 0, 0, 0);
	}
	
	public void failure() 
	{
		if(click) 
		{
			click = false;
			int center_w = (draw.screen_width / 2) - 192;
			int center_h = (draw.screen_height / 2) - 195;
			
			// RETRY
			if(mouse_x >= center_w && mouse_x <= center_w + 372 && mouse_y >= center_h + 185 && mouse_y <= center_h + 285) {
				prev_state = GLOBAL.current_state;
				GLOBAL.current_state = GLOBAL.STATE_FADEOUT;
				next_state = GLOBAL.STATE_PLAY;
				GLOBAL.immortal = 0;
			}
			// EXIT
			if(mouse_x >= center_w && mouse_x <= center_w + 372 && mouse_y >= center_h + 286 && mouse_y <= center_h + 385) {
				prev_state = GLOBAL.current_state;
				GLOBAL.current_state = GLOBAL.STATE_FADEOUT;
				next_state = GLOBAL.STATE_MAINMENU;
				menuposplay = 1654;
				menuposexit = 1735;
				GLOBAL.immortal = 0;
			}
		}	
		draw.draw_failure(global_alpha, 0, 0, 0, 0);
	}
	
	public void fadein() {
		
		// Draw
		if(next_state == GLOBAL.STATE_PLAY) {
			draw.draw(global_alpha);
		}
		else {
			menuposplay = 1654;
			menuposexit = 1735;
			draw.draw_menu(global_alpha, 1484, menuposplay, 1484, menuposexit);
		}
		
		// Increase global alpha from 0 to 1.
		if(global_alpha < 0.99) {
			global_alpha += 0.01;
		}
		else {
			global_alpha = 1;
			prev_state = GLOBAL.current_state;
			GLOBAL.current_state = next_state;
			click = false;
		}
	}
	
	public void fadeout() {
		
		// Draw
		if(prev_state == GLOBAL.STATE_MAINMENU) {
			draw.draw_menu(global_alpha, 1484, menuposplay, 1484, menuposexit);
		}
		else if(prev_state == GLOBAL.STATE_SUCCESS){
			draw.draw_success(global_alpha, 0, 0, 0, 0);
		}
		else if(prev_state == GLOBAL.STATE_FAILURE){
			draw.draw_failure(global_alpha, 0, 0, 0, 0);
		}
		
		// Lower global alpha from 1 to 0.
		if(global_alpha > 0.01) {
			global_alpha -= 0.01;
		}
		else {
			global_alpha = 0;
			prev_state = GLOBAL.current_state;
			GLOBAL.current_state = GLOBAL.STATE_FADEIN;
			
			// If we are playing, load level with the current progress
			if(next_state == GLOBAL.STATE_PLAY) {
				getCurrentProgress();
				loadLevel(GLOBAL.current_level);
				if(GLOBAL.soundActive && GLOBAL.sound == 1)
					GLOBAL.music.play();
			}
			click = false;
		}
	}
	
	public void pause() {
		
		if(click) 
		{
			click = false;
			int center_w = (draw.screen_width / 2) - 192;
			int center_h = (draw.screen_height / 2) - 103;
			
			// RESUME
			if(mouse_x >= center_w && mouse_x <= center_w + 372 && mouse_y >= center_h - 103 && mouse_y <= center_h) {
				GLOBAL.current_state = GLOBAL.STATE_PLAY;
			}	// EXIT
			else if(mouse_x >= center_w && mouse_x <= center_w + 372 && mouse_y >= center_h + 1 && mouse_y <= center_h + 103) {
				prev_state = GLOBAL.STATE_PLAY;
				GLOBAL.current_state = GLOBAL.STATE_FADEOUT;
				next_state = GLOBAL.STATE_MAINMENU;
				
				// Trigga ad
				if(handler != null)
					handler.showAds(true);
				if(GLOBAL.soundActive) {
					GLOBAL.music.stop();
				}
			}
		}	
		
		draw.draw_pause(global_alpha, 0, 0, 0, 0);
	}
	
	// Set & Get
	public EventManager getLevelEventManager() { return level.level_event_manager; }
	public Draw getDraw() { return draw; }
	public Level getLevel() { return level; }
	public Player getPlayer() { return level.player; }
	public int getCurrentState() { return GLOBAL.current_state; }
	public void setEditorSnap(Boolean onoff) { editor_snap = onoff; }
	public void setCurrentState(int new_state) { GLOBAL.current_state = new_state; }
	public Hitbox getCameraRect() { return camera_rect; }
	public void updateCameraRect(Hitbox new_rect) { camera_rect = new_rect; }
	public void updateCameraRect(int left, int right, int top, int bottom) {
		camera_rect.left = left;
		camera_rect.right = right;
		camera_rect.top = top;
		camera_rect.bottom = bottom;
	}

	// CAMERA
	private Hitbox camera_rect;
	private int camera_size_x;
	private int camera_size_y;
	
	private int mouse_x;
	private int mouse_y;
	private Boolean click;
	private int menuposplay;
	private int menuposexit;

	public Draw draw;
	public Level level;
	public Collision collision;
	private float global_alpha;
	public int prev_state;
	public int next_state;
	private Boolean editor_snap;
	
	// CollisionInformation about the players collision vs various things
	CollisionInformation player_world_collision;
	
	// Currently selected object... start with an "empty" object
	public GameObject selected_editor_object;
	public GameObject selected_editor_object_default;
	public Vector<String> messages;
	
	// ADS
	public IActivityRequestHandler handler;
	public Boolean adOn;
	


}


