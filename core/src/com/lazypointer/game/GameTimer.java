package com.lazypointer.game;

import java.io.Serializable;

import com.badlogic.gdx.utils.TimeUtils;

public class GameTimer implements Serializable {

	private static final long serialVersionUID = 4906950987370780667L;
	
	GameTimer() {
		once = true;
	}
	
	// Set the time in milliseconds
	public void setMilliSeconds(long time_in_milliseconds) {
		secsToWait = time_in_milliseconds;
	}

	// Starts the timer, and runs until the end, repeated calls to this metod will not restart the timer
	public void startTimer() {
		
		if(once) {
			start = TimeUtils.millis();
			once = false;
		}
	}
	
	// Forced restart of the timer, regardless if its done or not
	public void forceRestartTimer() {
		start = TimeUtils.millis();
		once = false;
	}

	// If the timer is done
	public Boolean isDone() {
		
		if((TimeUtils.millis() - start) >= secsToWait) {
			once = true;
			return true;
		}
		return false;
	}
	
	// DATAMEMBERS
    private long start;
    private long secsToWait;
    private Boolean once;

}
