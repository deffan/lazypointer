package com.lazypointer.game;

import java.io.Serializable;

public class Hitbox implements Serializable {
	
	private static final long serialVersionUID = 6427236135826583093L;
	
	/**
	* Hitbox
	* @param <b>left_x:</b> LEFT side of the rectangle (X).
	* @param <b>right_x_:</b> RIGHT side of the rectangle (X).
	* @param <b>top_y:</b> TOP side of the rectangle (Y).
	* @param <b>bottom_y:</b> BOTTOM side of the rectangle (Y).
	*/
	public Hitbox(int left_x, int right_x, int top_y, int bottom_y) {
		left = left_x;
		right = right_x;
		top = top_y;
		bottom = bottom_y;
		width = right - left;
		height = bottom - top;
		relative_top = 0;
		relative_left = 0;
	}
	
	/**
	* updateHitbox
	* @param <b>x:</b> New x (left)
	* @param <b>y:</b> New y (top)
	*/
	public void updateHitbox(int x, int y) {
		left = x + relative_left; 
		right = left + width; 
		top = y + relative_top;
		bottom = top + height;
	}
	
	public void updateHitbox(double x, double y) {
		left = (int)x + relative_left; 
		right = left + width; 
		top = (int)y + relative_top;
		bottom = top + height;
	}
	
	public int left;
	public int top;
	public int right;
	public int bottom;
	public int width;
	public int height;
	public int relative_left;		// Normally 0, negative makes it go towards the LEFT, positive towards RIGHT
	public int relative_top;		// Normally 0, negative makes it go towards the TOP, positive towards BOTTOM


}
