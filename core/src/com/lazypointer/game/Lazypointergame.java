package com.lazypointer.game;

import java.util.Vector;

import javax.swing.JLabel;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;



public class Lazypointergame extends ApplicationAdapter implements InputProcessor {

	private Game game;
	private int state;
	private GameObject current_editor_object;
	private Vector<GameObject> event_object;
	private Vector<Waypoint> event_waypoint;
	private float OTHER_LAYER_ALPHA;
	private Boolean right_click;
	private int touch_x;
	private int touch_y;
	private int touch_down_x;
	private int touch_down_y;
	private Boolean is_editor;
	private JLabel world_label;
	private JLabel relative_label;
	private JLabel element_label;
	private int frames_to_run;
	private IActivityRequestHandler myRequestHandler;
	private int platform;

	// Konstruktor
	public Lazypointergame(Boolean editor, IActivityRequestHandler h, int current_platform) {
		is_editor = editor;
		myRequestHandler = h;
		platform = current_platform;
	}
	
	// GET
	public Game getGame() { return game; }
	
	@Override
	public void create () {
		Gdx.input.setInputProcessor(this);
		Gdx.input.setCatchBackKey(true);
		game = new Game(platform);
		game.handler = myRequestHandler;
		state = GLOBAL.STATE_PLAY;
		current_editor_object = null;
		OTHER_LAYER_ALPHA = 1;
		right_click = false;
		touch_x = 0;
		touch_y = 0;
		touch_down_x = 0;
		touch_down_y = 0;
		frames_to_run = 0;
		
		if(!game.initGame())
			Gdx.app.exit();
		
	}

	@Override
	public void resize(int h, int w) {
		game.getDraw().resize(h, w);
	}
	
	@Override
	public void render () {
		
		if(is_editor) 
		{
			if(state == GLOBAL.STATE_PLAY) {
				check_keyboard();
				game.logic();
				game.editor_draw(1, 1, 1, 1);
			}
			else {

				if(state == GLOBAL.STATE_EDITOR_NORMAL)
					game.editor_draw(1, OTHER_LAYER_ALPHA, OTHER_LAYER_ALPHA, OTHER_LAYER_ALPHA);
				else if(state == GLOBAL.STATE_EDITOR_BACKGROUND)
					game.editor_draw(OTHER_LAYER_ALPHA, 1, OTHER_LAYER_ALPHA, OTHER_LAYER_ALPHA);
				else if(state == GLOBAL.STATE_EDITOR_FOREGROUND)
					game.editor_draw(OTHER_LAYER_ALPHA, OTHER_LAYER_ALPHA, 1, OTHER_LAYER_ALPHA);
				else if(state == GLOBAL.STATE_EDITOR_OBJECT) 
				{
					check_keyboard_editor();
					game.editor_draw(OTHER_LAYER_ALPHA, OTHER_LAYER_ALPHA, OTHER_LAYER_ALPHA, 1);
				}
				
				// If you want to run X amount of frames then stop...
				if(frames_to_run > 0) {
					game.messages.add(GLOBAL.MSG_INFORMATION + "RUNNING FRAME [" + frames_to_run + "]");
					game.logic();
					game.editor_draw(1, 1, 1, 1);
					frames_to_run--;
				}
			}
		}
		else 
		{
			if(GLOBAL.current_state == GLOBAL.STATE_PLAY) {
				game.logic();
				game.draw();
			}
			else if(GLOBAL.current_state == GLOBAL.STATE_MAINMENU) {
				game.mainmenu();
			}
			else if(GLOBAL.current_state == GLOBAL.STATE_SUCCESS) {
				game.success();
			}
			else if(GLOBAL.current_state == GLOBAL.STATE_FAILURE) {
				game.failure();
			}
			else if(GLOBAL.current_state == GLOBAL.STATE_FADEOUT) {
				game.fadeout();
			}
			else if(GLOBAL.current_state == GLOBAL.STATE_FADEIN) {
				game.fadein();
			}
			else if(GLOBAL.current_state == GLOBAL.STATE_PAUSE) {
				game.pause();
			}
		}

	}
	
	public void setEditorState(int new_state) {
		state = new_state;
	}
	
	public void setFramesToRun(int frames) {
		frames_to_run = frames;
	}
	
	public int getEditorState() { return state; }

	
	public void setEditorObject(GameObject o, Vector<GameObject> event_o, Vector<Waypoint> event_w) {
		current_editor_object = o;
		event_object = event_o;
		event_waypoint = event_w;
	}
	
	public void setEditorAlpha(float a) {
		OTHER_LAYER_ALPHA = a;
	}
	
	@Override
	public void dispose() {
		game.dispose();
	}
	
	void check_keyboard() {
		
		if(Gdx.input.isKeyPressed(Keys.A))
			game.keyboard_input(GLOBAL.LEFT);
		
		if(Gdx.input.isKeyPressed(Keys.D))
			game.keyboard_input(GLOBAL.RIGHT);
		
		if(Gdx.input.isKeyPressed(Keys.W))
			game.keyboard_input(GLOBAL.UP);
		
		if(Gdx.input.isKeyPressed(Keys.S))
			game.keyboard_input(GLOBAL.DOWN);
	}
	
	void check_keyboard_editor() {
		
		if(Gdx.input.isKeyPressed(Keys.A))
			game.keyboard_input_editor(GLOBAL.LEFT);
		
		if(Gdx.input.isKeyPressed(Keys.D))
			game.keyboard_input_editor(GLOBAL.RIGHT);
		
		if(Gdx.input.isKeyPressed(Keys.W))
			game.keyboard_input_editor(GLOBAL.UP);
		
		if(Gdx.input.isKeyPressed(Keys.S))
			game.keyboard_input_editor(GLOBAL.DOWN);
	}
	
	public boolean keyDown(int keycode) {
		
		// If "BACK" button is pressed on Android or ESC on Desktop
		if (Gdx.input.isKeyPressed(Keys.BACK) || Gdx.input.isKeyPressed(Keys.ESCAPE)){
			if(GLOBAL.current_state == GLOBAL.STATE_PLAY) 			// If playing, pause
				GLOBAL.current_state = GLOBAL.STATE_PAUSE;
			else if(GLOBAL.current_state == GLOBAL.STATE_PAUSE)		// If paused, play
				GLOBAL.current_state = GLOBAL.STATE_PLAY;
			else if(GLOBAL.current_state == GLOBAL.STATE_MAINMENU)
				Gdx.app.exit();										// If in main-menu, exit
		}
		
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		
		// Record the X/Y for the "down" event
		touch_down_x = screenX;
		touch_down_y = screenY;
		
		if(is_editor) {
	        if(state == GLOBAL.STATE_PLAY && button == Buttons.LEFT) {
	        	game.mouse_input(GLOBAL.DOWN);
	        }
	        else if(state != GLOBAL.STATE_PLAY && button == Buttons.RIGHT) {
	        	touch_x = screenX;
	        	touch_y = screenY;
	        	right_click = true;
	        }
		}
		else
		{
	        if(GLOBAL.current_state == GLOBAL.STATE_PLAY) {
	        	game.mouse_input(GLOBAL.DOWN);
	        }
	        else
	        	game.mouse_input_menu();
		}
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {

		// Move the camera_rect (editor-only)
		if(is_editor) {
			if(right_click) {
				
				if(touch_x > screenX) {
					game.getCameraRect().left += touch_x - screenX;
					game.getCameraRect().right += touch_x - screenX;
				}
				else {
					game.getCameraRect().left -= screenX - touch_x;
					game.getCameraRect().right -= screenX - touch_x;
				}
				
				if(touch_y > screenY) {
					game.getCameraRect().top += touch_y - screenY;
					game.getCameraRect().bottom += touch_y - screenY;
				}
				else {
					game.getCameraRect().top -= screenY - touch_y;
					game.getCameraRect().bottom -= screenY - touch_y;
				}
				
	        	right_click = false;
	        }
			else {
				if(state != GLOBAL.STATE_PLAY) {
					game.mouse_input_editor(current_editor_object, touch_down_x, touch_down_y, state, event_object, event_waypoint);
				}
				else if(state == GLOBAL.STATE_PLAY && button == Buttons.LEFT) {
			        	game.mouse_input(GLOBAL.UP);
			        }
			}
		}
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		game.mouse_input(0);
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {

		// Update the text for the editors status-bar, to show the world, relative and element coordinates
		if(is_editor) {

			world_label.setText(String.format("<html>%s<font color='blue'>%s</font>%s</html>", "WORLD: [", screenX + "," + screenY, "]"));
			relative_label.setText(String.format("<html>%s<font color='blue'>%s</font>%s</html>", "RELATIVE: [", (game.getCameraRect().left + screenX) + "," + (game.getCameraRect().top + screenY), "]"));
			element_label.setText(String.format("<html>%s<font color='blue'>%s</font></html>", "ELEMENT: ", "[" + (game.getCameraRect().left + screenX) / GLOBAL.TILE_SIZE + "][" + (game.getCameraRect().top +  screenY) / GLOBAL.TILE_SIZE + "]"));
		}
		
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public void setEditorStatusBar(JLabel w, JLabel r, JLabel e) {
		world_label = w;
		relative_label = r;
		element_label = e;
	}
	

}
