package com.lazypointer.game;

import java.io.Serializable;
import java.util.Vector;

public class Level implements Serializable {

	private static final long serialVersionUID = -7048780914082827114L;
	
	Level() 
	{
		objects = new Vector<GameObject>();
		spells = new Vector<Spell>();
		powerups = new Vector<Powerup>();
		backObjects = new Vector<GameObject>();
		world = null;
		background = null;
		foreground = null;
		world_size_x = 20;
		world_size_y = 20;
		
		level_event_manager = new EventManager(spells);
		
		player_start_x = 100;
		player_start_y = 100;
		
		null_sprite = new Sprite(1024, 128, 1, 0);
		
		// timer for time...
		timer = new GameTimer();
		timer.setMilliSeconds(1000);
		start_time = 0;
		timestring = "00:00";
		flashing = 1;
		flashOnOff = true;
		
		// Player
		int pxPos = 100;
		int pyPos = 100;
		int playerSize = GLOBAL.TILE_SIZE - 24;
		Hitbox phitbox = new Hitbox(pxPos, pxPos + playerSize, pyPos, pyPos + playerSize);
		Sprite psprite = new Sprite(5, 450, 1, GLOBAL.NORMAL_LAYER);
		Coordinate pc = new Coordinate(pxPos, pyPos);
		player = new Player(pc, phitbox, psprite, true);
		player.speed = 2;
		player.TYPE = GLOBAL.PLAYER;
		player.ID = "PLAYER";
		player.setEventCapable(level_event_manager);
		
		// ObjectGenerator
		object_generator = new ObjectGenerator(null, null, null, false);
		object_generator.ID = "GENERATOR";
		object_generator.setEventCapable(level_event_manager);
		((ObjectGenerator) object_generator).setObjectGeneratorData(this);
		
		currlev = "Level ?";
	}
	
	//------------------------------------------------------------------------------
	// logic() - For the level
	//------------------------------------------------------------------------------
	public void logic() {
		
		object_generator.logic();
		
		if(timer.isDone()) {
			
			GLOBAL.time--;
			if(GLOBAL.time < 0) {
				GLOBAL.current_state = GLOBAL.STATE_FAILURE;
			}
			else {
				int min = (GLOBAL.time / 60) % 60;
				int sec = GLOBAL.time % 60;
				
				if(min <= 9)
					timestring = "0" + min + ":";
				else
					timestring = min + ":";
				
				if(sec <= 9)
					timestring += "0" + sec;
				else
					timestring += sec;
				
				timer.startTimer();
			}
		}
		
		// Make the UI and text "flash" red when time is low!
		if(GLOBAL.time < 30) {
			if(flashOnOff) {
				flashing -= 0.02;
				if(flashing <= 0.02) {
					flashing = 0;
					flashOnOff = false;
				}
			}
			else {
				flashing += 0.02;
				if(flashing >= 0.98) {
					flashing = 1;
					flashOnOff = true;
				}
				
			}
		}
	}
	
	//------------------------------------------------------------------------------
	// newEmptyLevel() - Constructs a new empty level, used by the editor
	//------------------------------------------------------------------------------
	public void newEmptyLevel(int width, int height) {
		
		world = new GameObject[width][height];
		background = new GameObject[width][height];
		foreground = new GameObject[width][height];
		
		world_size_y = height;
		world_size_x = width;

		// create test-world
		int xPos = 0;
		int yPos = 0;
		
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				
				Coordinate tile_c = new Coordinate(xPos, yPos);
				Hitbox hitbox = new Hitbox(xPos, xPos + GLOBAL.TILE_SIZE, yPos, yPos + GLOBAL.TILE_SIZE);
				Sprite sprite = new Sprite(0, 0, 1, GLOBAL.NORMAL_LAYER);
				world[x][y] = new Tile(tile_c, hitbox, sprite, false);	
				background[x][y] = new Tile(tile_c, hitbox, sprite, false);
				foreground[x][y] = new Tile(tile_c, hitbox, sprite, false);
				xPos += GLOBAL.TILE_SIZE;
			}
			xPos = 0;
			yPos += GLOBAL.TILE_SIZE;
		}		
	}
	
	//------------------------------------------------------------------------------
	// newEmptyLevel() - Constructs a new empty level, with all Tiles being tile t.
	//------------------------------------------------------------------------------
	public void newEmptyNullSpriteLevel(int width, int height) {
		
		world = new GameObject[width][height];
		background = new GameObject[width][height];
		foreground = new GameObject[width][height];
		
		world_size_y = height;
		world_size_x = width;

		// create test-world
		int xPos = 0;
		int yPos = 0;
		
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				
				Coordinate tile_c = new Coordinate(xPos, yPos);
				Hitbox hitbox = new Hitbox(xPos, xPos + GLOBAL.TILE_SIZE, yPos, yPos + GLOBAL.TILE_SIZE);
				world[x][y] = new Tile(tile_c, hitbox, null, false);	
				background[x][y] = new Tile(tile_c, hitbox, null, false);
				foreground[x][y] = new Tile(tile_c, hitbox, null, false);
				xPos += GLOBAL.TILE_SIZE;
			}
			xPos = 0;
			yPos += GLOBAL.TILE_SIZE;
		}		
	}
	
	public void reset() {
		currlev = "Level " + GLOBAL.current_level;
		GLOBAL.time = start_time;
		timestring = "03:00";
		player_start_x = (int) player.coordinates.x;
		player_start_y = (int) player.coordinates.y;
	}
	
	// DATAMEMBERS
	public GameObject world[][];
	public Vector<GameObject> objects;
	public Vector<Spell> spells;
	public Vector<Powerup> powerups;
	public Vector<GameObject> backObjects;
	public GameObject background[][];
	public GameObject foreground[][];
	public int world_size_x;
	public int world_size_y;
	public Sprite null_sprite;
	public int camera_speed;
	public int player_start_x;
	public int player_start_y;
	public EventManager level_event_manager;
	public Player player;
	public float flashing;
	private Boolean flashOnOff;
	
	// Object Generator
	public GameObject object_generator;
	
	// Time related
	public GameTimer timer;
	public String timestring;
	public String currlev;
	public int start_time;
	

	
}

