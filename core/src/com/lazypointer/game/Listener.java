package com.lazypointer.game;

import java.io.Serializable;
import java.util.Vector;

public class Listener implements Serializable {
	
	private static final long serialVersionUID = -1690387807476707957L;

	Listener() {
		event_queue = new Vector<Event>();
	}

	public Vector<Event> event_queue;
}
