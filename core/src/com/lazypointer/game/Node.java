package com.lazypointer.game;

/**
* Node class, containing a Coordinate object, a Node object and int values F, G, H.
* Used in A* path-finding algorithm.
*/
public class Node {

	Coordinate coordinate = null;
	Node parent = null;
	int F = 0;
	int G = 0;
	int H = 0;

}
