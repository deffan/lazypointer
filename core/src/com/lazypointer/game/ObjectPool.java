package com.lazypointer.game;

import java.io.Serializable;
import java.util.Vector;

public class ObjectPool implements Serializable {
	
	
	private static final long serialVersionUID = 643607988055429513L;
	
	ObjectPool(EventManager e, Vector<Spell> s) {
		event_manager = e;
		spells = s;
		generated = 0;
		
		// Create 10x of each
		for(int i = 0; i < 10; i++) {
			
			// Fireball
			Sprite sprite = new Sprite(0, 768, 1, GLOBAL.NORMAL_LAYER);
			sprite.relative_x = (-16);
			sprite.relative_y = (-16);
			Spell f = new Fireball(new Coordinate(0, 0), new Hitbox(0, 0, 0, 0), sprite, true);
			f.ID = "FIREBALL_" + generated;
			f.TYPE = GLOBAL.SPELL_FIREBALL;
			f.setEventCapable(event_manager);
			f.alive = false;
			generated++;
			spells.add(f);
			
			// Fireball Explosion
			Sprite sprite2 = new Sprite(0, 0, 1, 0);
			Spell f1 = new FireExplosion(new Coordinate(0, 0), new Hitbox(0, 0, 0, 0), sprite2, false);
			f1.ID = "FIREEXPLOSION_" + generated;
			f1.TYPE = GLOBAL.SPELL_FIRE_EXPLOSION;
			f1.alive = false;
			f1.init();
			spells.add(f1);
			generated++;
			
			// Slime
			Sprite sprite3 = new Sprite(0, 0, 1, 0);
			sprite3.relative_x = (-16);
			sprite3.relative_y = (-16);
			Spell s1 = new Slime(new Coordinate(0, 0), new Hitbox(0, 0, 0, 0), sprite3, false);
			s1.ID = "SLIME" + generated;
			s1.TYPE = GLOBAL.SPELL_SLIME;
			s1.alive = false;
			s1.init();
			spells.add(s1);
			generated++;
		}
		
		// Create "+1" and "-1" objects
		Coordinate coord = new Coordinate(0, 0);
		Sprite spr = new Sprite(1986, 982, 0, 0);
		Hitbox hitb = new Hitbox(0, 0, 0, 0);
		lostlife = new LifeOrDeath(coord, hitb, spr, false);
		lostlife.TYPE = GLOBAL.OBJECT_LIFELOSS;
		lostlife.init();
		spells.add(lostlife);
		
		Coordinate coord1 = new Coordinate(0, 0);
		Sprite spr1 = new Sprite(1986, 706, 0, 0);
		Hitbox hitb1 = new Hitbox(0, 0, 0, 0);
		extralife = new LifeOrDeath(coord1, hitb1, spr1, false);
		extralife.TYPE = GLOBAL.OBJECT_LIFEGAIN;
		extralife.init();
		spells.add(extralife);
	}
	
	public void addObject(int what, double x, double y, double angle, int from) 
	{
		switch(what) 
		{
			case GLOBAL.SPELL_FIREBALL: makeSpell(findSpell(GLOBAL.SPELL_FIREBALL), x, y, angle, from); break;
			case GLOBAL.SPELL_FIRE_EXPLOSION: makeSpell(findSpell(GLOBAL.SPELL_FIRE_EXPLOSION), x, y, angle, 0); break;
			case GLOBAL.SPELL_SLIME: makeSpell(findSpell(GLOBAL.SPELL_SLIME), x, y, angle, 0); break;
		}
	}
	
	private int findSpell(int what) 
	{
		// Find first free spell
		for(int i = 0; i < spells.size(); i++) {
			if(!spells.elementAt(i).is_alive() && spells.elementAt(i).TYPE == what)
				return i;
		}
	
		// If all spells are currently taken/active, make a new one
		switch(what) 
		{
			case GLOBAL.SPELL_FIREBALL: 
			{
				Sprite sprite = new Sprite(0, 768, 1, GLOBAL.NORMAL_LAYER);
				sprite.relative_x = (-16);
				sprite.relative_y = (-16);
				Spell f = new Fireball(new Coordinate(0, 0), new Hitbox(0, 0, 0, 0), sprite, true);
				f.ID = "FIREBALL_" + generated;
				f.TYPE = GLOBAL.SPELL_FIREBALL;
				f.setEventCapable(event_manager);
				spells.add(f);
				generated++;
				System.out.println("Had to make a new fireball");
				break;
			}
			case GLOBAL.SPELL_FIRE_EXPLOSION:
			{
				Sprite sprite = new Sprite(0, 0, 1, 0);
				Spell f = new FireExplosion(new Coordinate(0, 0), new Hitbox(0, 0, 0, 0), sprite, false);
				f.ID = "FIREEXPLOSION_" + generated;
				f.TYPE = GLOBAL.SPELL_FIRE_EXPLOSION;
				spells.add(f);
				generated++;
				System.out.println("Had to make a new fireexplosion");
				break;
			}
			case GLOBAL.SPELL_SLIME:
			{
				Sprite sprite = new Sprite(0, 0, 1, 0);
				sprite.relative_x = (-16);
				sprite.relative_y = (-16);
				Spell s = new Slime(new Coordinate(0, 0), new Hitbox(0, 0, 0, 0), sprite, true);
				s.ID = "SLIME_" + generated;
				s.TYPE = GLOBAL.SPELL_SLIME;
				s.init();
				s.setEventCapable(event_manager);
				spells.add(s);
				generated++;
				System.out.println("Had to make a new slime");
				break;
			}
		}
		return spells.size() - 1;
	}
	
	private void makeSpell(int i, double x, double y, double angle, int from) {
		
		spells.elementAt(i).from_object = from;
		spells.elementAt(i).coordinates.x = x - 12;
		spells.elementAt(i).coordinates.y = y - 12;
		spells.elementAt(i).point_x = x - 12;
		spells.elementAt(i).point_y = y - 12;
		spells.elementAt(i).hitbox.left = (int)spells.elementAt(i).coordinates.x;
		spells.elementAt(i).hitbox.top = (int)spells.elementAt(i).coordinates.y;
		spells.elementAt(i).hitbox.width = 25;
		spells.elementAt(i).hitbox.height = 25;
		spells.elementAt(i).hitbox.updateHitbox(spells.elementAt(i).coordinates.x, spells.elementAt(i).coordinates.y);
		spells.elementAt(i).setAngle(angle);
		spells.elementAt(i).reset();

	}
	
	private Vector<Spell> spells;
	public EventManager event_manager;
	private int generated;
	public LifeOrDeath lostlife;
	public LifeOrDeath extralife;
}
