package com.lazypointer.game;

import java.io.Serializable;

public class Sprite implements Serializable {

	private static final long serialVersionUID = -8176005746317240596L;
	
	/**
	* Sprite constructor
	* @param <b>texture_x_coordinate:</b> The X coordinate on the texture.
	* @param <b>texture_y_coordinate:</b> The Y coordinate on the texture.
	* @param <b>alpha_value:</b> Transparency value (1.0 - 0.0).
	* @param <b>layer_position:</b> GLOBAL.BACKGROUND_LAYER | GLOBAL.FOREGROUND_LAYER | GLOBAL.NORMAL_LAYER
	*/
	public Sprite(int texture_x_coordinate, int texture_y_coordinate, float alpha_value, int layer_position) {
		texture_x = texture_x_coordinate;
		texture_y = texture_y_coordinate;
		alpha = alpha_value;
		layer = layer_position;
		rotation = 0;
		size_x = GLOBAL.TILE_SIZE;
		size_y = GLOBAL.TILE_SIZE;
		relative_x = 0;
		relative_y = 0;
		scale_x = 1;
		scale_y = 1;
	}

	// DATAMEMBERS
	public int texture_x;
	public int texture_y;
	public int size_x;
	public int size_y;
	public float alpha;
	public int layer;
	public double rotation;
	public int relative_x;
	public int relative_y;
	public float scale_x;
	public float scale_y;

}
