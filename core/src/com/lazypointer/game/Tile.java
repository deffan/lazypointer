package com.lazypointer.game;

public class Tile extends GameObject {

	private static final long serialVersionUID = -5405315512150642261L;

	public Tile(Coordinate new_coordinates, Hitbox new_hitbox, Sprite new_sprite, Boolean is_collidable) {
		super(new_coordinates, new_hitbox, new_sprite, is_collidable);
	}
	
	@Override
	public void init() {
	}
	
	@Override
	public void logic() {}

	@Override
	public void collision(GameObject what, CollisionInformation info) {}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		
	}

}
