package com.lazypointer.game;

public abstract class UI {
	
	UI() {
		rect = new Hitbox(0, 0, 0, 0);
		position_x = GLOBAL.UI_LEFT;
		position_y = GLOBAL.UI_TOP;
		style = 0;
	}
	
	// SET
	public void setPositionX(int pos) { position_x = pos; }
	public void setPositionY(int pos) { position_y = pos; }
	public void setStyle(int s) { style = s; }
	
	// GET
	public Hitbox getRect() { return rect; }

	// DATAMEMBERS
	protected Hitbox rect;
	protected int position_x;
	protected int position_y;
	protected int style;
}
