package com.lazypointer.game;

import java.util.Vector;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class UIBox extends UI {
	
	UIBox() {
		v_padding = 0;
		h_padding = 0;
		elements = new Vector<UI>();
	}
	
	// check what UI-element was clicked on.
	// left is x:0
	// top is y:0
	public int click(int x, int y) {
		
		return 0;
	}
	
	public void draw(SpriteBatch batch, TextureRegion region) {
		
		region.setRegion(0, 0, 0, 0);
		region.flip(false, true);
		batch.setColor(1, 1, 1, 1);
		batch.draw(region, 0, 0);
		
		// print out, depending on STYLE
		// and paddings.
		// also, wrap if the box is too small?
		// or just overlap and disregard borders?
		// what about having BORDERS to the box?
		// what about having a half-transparent-grayish background in the box?
		// draw text if element is UIText
	}
	
	public Vector<UI> getBoxElements() { return elements; }

	private Vector<UI> elements;
	private int v_padding;
	private int h_padding;
}
