package com.lazypointer.game;

public class UIText extends UI {

	public void setText(String txt) { text = txt; }
	public String getText() { return text; }
	
	private String text;
}
