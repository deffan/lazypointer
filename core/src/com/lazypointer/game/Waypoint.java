package com.lazypointer.game;

import java.io.Serializable;

public class Waypoint implements Serializable {
	
	private static final long serialVersionUID = 6767242115605347760L;
	
	public Waypoint(int x, int y, int wspeed, int time_in_seconds) {
		speed = wspeed;
		coordinates = new Coordinate(x, y);
		timer = new GameTimer();
		time = time_in_seconds;
		timer.setMilliSeconds(time_in_seconds * 1000);
	}

	public int getTime() { return time; }
	public GameTimer getTimer() { return timer; }
	
	public Coordinate coordinates;
	public int speed;
	private int time;
	private GameTimer timer;
}
