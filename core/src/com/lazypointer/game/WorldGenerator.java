package com.lazypointer.game;

import java.util.Random;

public class WorldGenerator {

	// should be used in the level-class
	// the level class should probably have different types too.
	// like: EndlessLevelRight, EndlessLevelDown, EndlessLevelUp, FixedLevelRight, FixedLevelDown, FixedLevelUp
	Zone top_left;
	Zone top_middle;
	Zone top_right;
	Zone middle_left;
	Zone middle_middle;
	Zone middle_right;
	Zone bottom_left;
	Zone bottom_middle;
	Zone bottom_right;
	Random rand;
	
	// Texture-Coords for the edges
	int edge_type = 640;
	int top_edge = 256;
	int bottom_edge = 448;
	int right_edge = 384;
	int left_edge = 320;
	int top_left_edge = 0;
	int top_right_edge = 64;
	int bottom_left_edge = 128;
	int bottom_right_edge = 192;
	
	int template_default_tile = 1337;
	
	WorldGenerator() {

		rand = new Random();
		
		top_left = new Zone();
		top_left.stuff = "top_left";
		
		top_middle = new Zone();
		top_middle.stuff = "top_middle";
		
		top_right = new Zone();
		top_right.stuff = "top_right";
		
		middle_left = new Zone();
		middle_left.stuff = "middle_left";
		
		middle_middle = new Zone();
		middle_middle.stuff = "middle_middle";
		
		middle_right = new Zone();
		middle_right.stuff = "middle_right";
		
		bottom_left = new Zone();
		bottom_left.stuff = "bottom_left";
		
		bottom_middle = new Zone();
		bottom_middle.stuff = "bottom_middle";
		
		bottom_right = new Zone();
		bottom_right.stuff = "bottom_right";
		
	}
	
	
	/**
	 * Generates an outside Zone. The Level object will be rebuilt.
	 * @param level object
	 */
	public void generateOutsideZone(Level level) {
		
		level.newEmptyLevel(50, 50);	// 1024x768
		
		int xPos = 0;
		int yPos = 0;
		for (int y = 0; y < level.world_size_y; y++) {
			for (int x = 0; x < level.world_size_x; x++) {
				Coordinate tile_c = new Coordinate(xPos, yPos);
				Hitbox hitbox = new Hitbox(xPos, xPos + GLOBAL.TILE_SIZE, yPos, yPos + GLOBAL.TILE_SIZE);
				level.world[x][y] = getRandomOutsideForestTile(x, y);
				level.background[x][y] = new Tile(tile_c, hitbox, null, false);
				level.foreground[x][y] = new Tile(tile_c, hitbox, null, false);
				xPos += GLOBAL.TILE_SIZE;
			}
			xPos = 0;
			yPos += GLOBAL.TILE_SIZE;
		}
		
		// Create the "null sprite" which renders everything outside the world
		level.null_sprite = new Sprite(512, edge_type, 1, GLOBAL.NORMAL_LAYER);
		
		// Now, create the edges of the map, corners first, then the lines
		level.world[0][0] = new Tile(null, null, new Sprite(0, edge_type, 1, GLOBAL.NORMAL_LAYER), true);
		level.world[0][level.world_size_y - 1] = new Tile(null, null, new Sprite(128, edge_type, 1, GLOBAL.NORMAL_LAYER), true);
		level.world[level.world_size_x - 1][0] = new Tile(null, null, new Sprite(64, edge_type, 1, GLOBAL.NORMAL_LAYER), true);
		level.world[level.world_size_x - 1][level.world_size_y - 1] = new Tile(null, null, new Sprite(192, edge_type, 1, GLOBAL.NORMAL_LAYER), true);
		
		// Top
		for(int i = 1; i < level.world_size_x - 1; i++)
			level.world[i][0] = new Tile(null, null, new Sprite(top_edge, edge_type, 1, GLOBAL.NORMAL_LAYER), true);
		
		// Bottom
		for(int i = 1; i < level.world_size_x - 1; i++)
			level.world[i][level.world_size_y - 1] = new Tile(null, null, new Sprite(bottom_edge, edge_type, 1, GLOBAL.NORMAL_LAYER), true);
		
		// Right
		for(int i = 1; i < level.world_size_y - 1; i++)
			level.world[0][i] = new Tile(null, null, new Sprite(right_edge, edge_type, 1, GLOBAL.NORMAL_LAYER), true);
		
		// Left
		for(int i = 1; i < level.world_size_y - 1; i++)
			level.world[level.world_size_x - 1][i] = new Tile(null, null, new Sprite(left_edge, edge_type, 1, GLOBAL.NORMAL_LAYER), true);
	
		
		
		
		// First place 2 large templates
		// Then place 10 smaller templates
		// Then place 1 river (if the river cant find a path from start-exit, try another start-exit, but only 3 times.
		
		// Place 10 template tiles, 3x3 big.
		for(int i = 0; i < 10; i++) {
			
			// Place templates randomly
			int template_x = rand.nextInt(10);
			int template_y = rand.nextInt(2);
			int templatex = template_x * 192;
			int templatey = 384 + (template_y * 192);
			
			generateOutsideTemplate(level, templatex, templatey, 3);
		}

	}
	
	
	
	
	
	
	private void generateOutsideTemplate(Level level, int template_x, int template_y, int size) {
		
		// Randomize starting location
		int randx = level.world_size_x - size + 1;
		int randy = level.world_size_y - size + 1;
		int rx = rand.nextInt(randx) + 1;
		int ry = rand.nextInt(randy) + 1;
		int attempt = 0;
		Boolean ok = false;

		// First check if anything collidable is already here. If so: Abort
		// Try placing 3 times before giving up.
		while(attempt < 3) {
			
			// Check
			for (int y = ry; y < ry+size; y++) {
				for (int x = rx; x < rx+size; x++) {
					if(!level.world[x][y].isCollidable()) {
						ok = true;
					}
					else {
						ok = false;
						attempt++;
						if(attempt == 4)
							System.out.println("INFO: Unable to place template tile here!");
						break;
					}
				}
				if(!ok)
					break;
			}
			
			// Everyhing seems ok, now actually place the tiles
			if(ok) {

				// Coordinates
				int xPos = rx;
				int yPos = ry;
				int tx = template_x;
				int ty = template_y;
				
				// Place template tiles
				for (int y = ry; y < ry+size; y++) {
					for (int x = rx; x < rx+size; x++) {
						level.world[x][y] = new Tile(new Coordinate(xPos, yPos), new Hitbox(xPos, xPos + GLOBAL.TILE_SIZE, yPos, yPos + GLOBAL.TILE_SIZE), new Sprite(tx, ty, 1, GLOBAL.NORMAL_LAYER), true);
						xPos += GLOBAL.TILE_SIZE;
						tx += GLOBAL.TILE_SIZE;
					}
					xPos = rx;
					tx = template_x;
					yPos += GLOBAL.TILE_SIZE;
					ty += GLOBAL.TILE_SIZE;
				}
				// Breaks the while loop
				break;
			}

		}	// END OF WHILE

	}	// END OF METHOD
			
	
	private Tile getEdgeTile(int what) { return new Tile(null, null, new Sprite(what, edge_type, 1, GLOBAL.NORMAL_LAYER), false); }

	private Tile getRandomOutsideForestTile(int x, int y) {
		
		Tile t;
		Boolean collidable = false;
		
		// 50% chance of grass, 50% rest
		int mainroll = rand.nextInt(100);
		if(mainroll < 50) {
			t = new Tile(new Coordinate(x * GLOBAL.TILE_SIZE, y * GLOBAL.TILE_SIZE), new Hitbox(x, x + GLOBAL.TILE_SIZE, y, y + GLOBAL.TILE_SIZE), new Sprite(0, 0, 1, GLOBAL.NORMAL_LAYER), collidable);
			return t;
		}
		
		// Randomize which texture to get, from 0-2048px on X, and 0-384px on Y.
		int rollx = rand.nextInt(32) * GLOBAL.TILE_SIZE;
		int rolly = rand.nextInt(6) * GLOBAL.TILE_SIZE;
		
		// The row (5) are to be collidable tiles, this is only for the pathfinding to become more random later on
		if(rolly == 320)
			collidable = true;

		t = new Tile(new Coordinate(x * GLOBAL.TILE_SIZE, y * GLOBAL.TILE_SIZE), new Hitbox(x, x + GLOBAL.TILE_SIZE, y, y + GLOBAL.TILE_SIZE), new Sprite(rollx, rolly, 1, GLOBAL.NORMAL_LAYER), collidable);
		return t;
	}

	public void switchZone() {
		

		// Move RIGHT - move all 3 right zones to the left, and create a new (EMPTY) zone to the right.
		moveRight();
		
		// For example, in a FOREST-FIXED level it can look like this.
		
		// TOP ZONES can be "air" zones, meaning a world[][] with NULL tiles or possibly background clouds and such.
		// MIDDLE ZONES have a START-ZONE and END-ZONE with X "normal" zones in between.
		// BOTTOM ZONES can be filled with just dirt with a "solid rock layer" that cant be passed through
	
	}
	
	private void moveRight() {
		
		System.out.print("moveRight()");
		// Always save the ZONE that disappears from screen to file.
		// Should be named logically "tlz_n.zone" (topleftzone_number)
		// Always load or generate the zone to the new places.
		
		// The zones should be big enough to cover the entire screen.
		// 50x50 zone with 64-pixelsize gives 3200 pixels in size.
		// 50x50 world is 2500 tiles, x9 = 22500 tiles loaded at all times. Only counting TILES in the normal layer.
		
		// 100x100 should be perfectly fine, which means 10000 tiles x9 = 90000 tiles.
		// That means 24 Megabytes in memory for just the tiles. Which is not much.
		// However, it means 2,6 Megabytes will be saved each time you walk to a new zone.
		// And, lets say you have played for a long long time... and saved 1000 zones, thats 2,6 Gigabytes in diskspace.
		// I guess its smarter to just RESET previously saved worlds once you travel to a new zone.
		// So if you go back, they are all reset. OR we simply remove all non-endless worlds.
		
		// For fixed maps: Load in the entire 
		
		// MOVING RIGHT
		save(top_left);
		top_left = top_middle;
		top_middle = top_right;
		top_right = new Zone();		// Create a method that first checks if this already exists, and load that one in. OR generates a new one.

		save(middle_left);
		middle_left = middle_middle;
		middle_middle = middle_right;
		middle_right = new Zone();
		
		save(bottom_left);
		bottom_left = bottom_middle;
		bottom_middle = bottom_right;
		bottom_right = new Zone();
		
		System.out.print(top_left.stuff + " | ");
		System.out.print(top_middle.stuff + " | ");
		System.out.print(top_right.stuff);
		System.out.println();
		
		System.out.print(middle_left.stuff + " | ");
		System.out.print(middle_middle.stuff + " | ");
		System.out.print(middle_right.stuff);
		System.out.println();
		
		System.out.print(bottom_left.stuff + " ");
		System.out.print(bottom_middle.stuff + " ");
		System.out.print(bottom_right.stuff);
		System.out.println();
		
		
	}
	
	private void moveLeft() {

	}
	
	private Boolean save(Zone z) {
		return true;
	}
	
	public Zone generateZone(int type) {
		
		// forest
		// forest_end
		// 
		
		
		// kan skapa serializerade "mallar" som laddas in h�r
		// sen �ndras, modifieras slumpm�ssigt
		
		return null;
	}
	
}
