0 - Tile (default)
1 - MovingBlock
2 - Dark Pit
3 - Portal
4 - Trigger
5 - Trigger (Visual)
6 - Door
7 - Rotating Spike (Static)
8 - Rotating Spike (Vertical)
9 - Rotating Spike (Horizontal)
10 - Rotating Spike (Random)
11 - Mine
12 - Cannon-Left
13 - Cannon-Right
14 - Cannon-Up
15 - Cannon-Down
16 - Cannon-Aimbot
17 - 4-Cannon (Static)
18 - 4-Cannon (Rotating)
19 - 4-Cannon (Bounce)
20 - Circular (Static)
21 - Circular (Moving)
22 - Lava
23 - Whirlwind
24 - SlimeBall
25 - Spike-Left
26 - Spike-Right
27 - Spike-Up
28 - Spike-Down
29 - SkullGhost
30 - Snake
31 - FireSnake
32 - Flamethrower
33 - Virus
34 - DarkPit (fader)
35 - Finish Line
36 - Trigger (adder)
37 - Trigger (remover)
38 - Trigger (activator)
39 - [POWERUP] Coin
40 - [POWERUP] Time_10
41 - [POWERUP] Time_20
42 - [POWERUP] Time_30
43 - [POWERUP] Time_40
44 - [POWERUP] Time_50
45 - [POWERUP] Time_60
46 - [POWERUP] Time_120
47 - [POWERUP] Time_180
48 - [POWERUP] Life
49 - MovingFloor (LEFT)
50 - MovingFloor (RIGHT)
51 - MovingFloor (UP)
52 - MovingFloor (DOWN)
53 - MovingFloor (LEFT ANIMATION)
54 - MovingFloor (RIGHT ANIMATION)
55 - MovingFloor (UP ANIMATION)
56 - MovingFloor (DOWN ANIMATION)
57 - Jumper
58 - DestroyableBlock
59 - Pushable
60 - Jumper Follow