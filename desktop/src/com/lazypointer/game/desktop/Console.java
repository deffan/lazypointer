package com.lazypointer.game.desktop;

import java.util.List;

import com.lazypointer.game.Camera;
import com.lazypointer.game.GLOBAL;
import com.lazypointer.game.GameObject;
import com.lazypointer.game.Level;
import com.lazypointer.game.Player;
import com.lazypointer.game.Waypoint;

public class Console {

	//------------------------------------------------------------------------------
	// Constructor
	//------------------------------------------------------------------------------
	Console(EditorObject current_obj, Level l, Camera c, Player p, List<String> list) {
		current = current_obj;
		level = l;
		player = p;
		camera = c;
		typelist = list;
		current_waypoint = null;
	}
	
	//------------------------------------------------------------------------------
	// parseCommand() - Determines what command this is
	//------------------------------------------------------------------------------	
	public String parseCommand(String cmd) {
		
		try {
			if(cmd.length() >= 3 && cmd.substring(0, 3).equals("add")) {
				return add(cmd);
			}
			else if(cmd.length() >= 3 && cmd.substring(0, 3).equals("new")) {
				return newcmd(cmd);
			}
			else if(cmd.length() >= 4 && cmd.substring(0, 4).equals("help")) {
				return help();
			}
			else if(cmd.length() >= 4 && cmd.substring(0, 4).equals("move")) {
				return move(cmd);
			}
			else if(cmd.length() >= 4 && cmd.substring(0, 4).equals("show")) {
				return show(cmd);
			}
			else if(cmd.length() >= 4 && cmd.substring(0, 4).equals("list")) {
				return listObjects();
			}
			else if(cmd.length() >= 6 && cmd.substring(0, 6).equals("remove")) {
				
			}
		}
		catch(Exception e) {
			String error_message = "EXCEPTION: '" + e.getMessage() + "'.\n";
			return error_message;
		}

		String error_message = "UNKNOWN COMMAND: '" + cmd + "'.\n";
		return error_message;
	}
	
	//------------------------------------------------------------------------------
	// help() - The "help" command, showing all other commands
	//------------------------------------------------------------------------------	
	private String help() {
		
		String cmds = "-------------------------------------------------------\n" +
					  "> help\n" +
					  "> move(ID,x,y)\n" +
					  "> show(ID)\n" +
					  "> list\n" +
					  "> add(ID-from,ID-to)\n" +
					  "> new waypoint(x,y,speed,time)\n" +
					  "-------------------------------------------------------\n";
		
		return cmds;
	}
	
	//------------------------------------------------------------------------------
	// show() - The "show" command
	//------------------------------------------------------------------------------
	private String show(String cmd) {
		
		int par = 0;
		if(cmd.charAt(4) == '(') {
			// Find the parenthesis
			par = cmd.indexOf(')');
			if(par == -1)
				return "ERROR: Missing ')' at the end of command.\n";
		}
		else
			return "ERROR: Missing '('.\n";
				
		// extract the ID
		String ID = cmd.substring(5, par);
		
		// Find this object
		GameObject o = linearSearch(ID);
		if(o == null)
			return "ERROR: Unable to find the object with ID: " + ID + ".\n";
		
		return gameObjectInfo(o);
	}
	
	//------------------------------------------------------------------------------
	// gameObjectInfo() - Prints information about a GameObject
	//------------------------------------------------------------------------------	
	private String gameObjectInfo(GameObject o) {
		
		String cmds = "-------------------------------------------------------\n";

	
		return cmds;
	}
	
	//------------------------------------------------------------------------------
	// move() - The "move" command
	//------------------------------------------------------------------------------	
	private String move(String cmd) {
		
		return "ERROR: Missing '('.\n";
	}
	
	//------------------------------------------------------------------------------
	// add() - The "add" command
	//------------------------------------------------------------------------------	
	private String add(String cmd) {
		
		return "ERROR: Missing '('.\n";

	}
	
	//------------------------------------------------------------------------------
	// newcmd() - The "new" command
	//------------------------------------------------------------------------------	
	private String newcmd(String cmd) {

		return "ERROR: Command not formatted correctly.\n";

	}
	
	//------------------------------------------------------------------------------
	// addWaypoint()
	//------------------------------------------------------------------------------	
	private String addWaypoint(String cmd) {
		
		return "";
	}
	
	//------------------------------------------------------------------------------
	// listObjects()
	//------------------------------------------------------------------------------
	private String listObjects() {
		
		String list = "";
		return list;
	}
	
	//------------------------------------------------------------------------------
	// linearSearch()
	//------------------------------------------------------------------------------	
	private String linearSearchAndMove(String ID, int x, int y) {

		return "ERROR: Unable to find the ob";
	}
	
	//------------------------------------------------------------------------------
	// linearSearch()
	//------------------------------------------------------------------------------	
	private GameObject linearSearch(String ID) {

		return null;
	}
	


	
	private EditorObject current;
	private Level level;
	private Player player;
	private Camera camera;
	private List<String> typelist;
	private Waypoint current_waypoint;
}
