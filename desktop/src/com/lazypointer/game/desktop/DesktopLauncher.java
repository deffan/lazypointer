package com.lazypointer.game.desktop;

import javax.swing.SwingUtilities;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import com.lazypointer.game.GLOBAL;
import com.lazypointer.game.Lazypointergame;


public class DesktopLauncher implements IActivityRequestHandler {
	
	public static void main (String[] arg) {

		final Boolean editor = true;
		
		// Game
		if(!editor) {
			LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
			config.title = "LP";
			config.width = 1024;
			config.height = 768;
			config.useGL30 = false;
			config.fullscreen = false;
			new LwjglApplication(new Lazypointergame(editor, null, GLOBAL.DESKTOP_PLATFORM), config);
		}
		else {	// Editor
		
			// The editor runs in a seperate thread
	        SwingUtilities.invokeLater(new Runnable() {
	            @Override
	            public void run() {
	                new WorldEditor(editor);
	            }
	        });
		}
	}

	@Override
	public void showAds(boolean show) {
	}
	

}

