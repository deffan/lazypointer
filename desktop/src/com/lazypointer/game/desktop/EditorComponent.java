package com.lazypointer.game.desktop;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.backends.lwjgl.LwjglCanvas;
import com.lazypointer.game.GLOBAL;

public class EditorComponent {
	
	//--------------------------------------------
	// Constructor - Sets upp the main window and canvas for OpenGL
	//--------------------------------------------
	EditorComponent(WorldEditor e) {

		// Editor
		editor = e;
		
		// Create the main window
		window = new JFrame("WorldEditor");

		window.setSize(1024, 768);
		
		// Add the panel to the window, and show
		panelen = new JPanel();
		window.getContentPane().add(panelen, BorderLayout.CENTER);
		
		// For closing
		window.addWindowListener(new MyWindowListener());
		
		// GridBagLayout for the tools
		GridBagLayout theLayout = new GridBagLayout();
		GridBagConstraints con = new GridBagConstraints();
		
		// Create the rest of the components
		createMenu();
		createToolBar(theLayout, con);
		createTabs(theLayout, con);
		createCanvas();
		createSidePanel();
		sidePanelSetup();
		createStatusBar();
		createCameraWindow();
		createPlayerWindow();
		createEventWindow();
		createObjectEventWindow();
		createConsoleWindow();
		createSelectedObjectWindow();
		createLogWindow();
		
		window.setExtendedState(JFrame.MAXIMIZED_BOTH);
		window.setVisible(true);
	}
	
	//--------------------------------------------
	// WindowListener for closing the application in order
	//--------------------------------------------
	class MyWindowListener extends WindowAdapter {
		public void windowClosing(WindowEvent e) { 
			canvas.stop();
			log_window.dispose();
			camera_window.dispose();
			event_window.dispose();
			object_event_window.dispose();
			player_window.dispose();
			console_window.dispose();
			sel_obj_window.dispose();
			window.dispose();
			Gdx.app.exit();
		}
	}
	
	//--------------------------------------------
	// createPlayerWindow()
	//--------------------------------------------		
	private void createPlayerWindow() {
		player_window = new JFrame("PLAYER SETTINGS");
		player_window.setAlwaysOnTop(true);
		player_window.setSize(400, 170);
		
		// Make so that the window just hides when closed
		player_window.addWindowListener(new WindowListener() {

			@Override
			public void windowClosing(WindowEvent arg0) {
				player_window.setVisible(false);
			}
	
			// Ignore these...
			public void windowActivated(WindowEvent arg0) {}
			public void windowClosed(WindowEvent arg0) {}
			public void windowDeactivated(WindowEvent arg0) {}
			public void windowDeiconified(WindowEvent arg0) {}
			public void windowIconified(WindowEvent arg0) {}
			public void windowOpened(WindowEvent arg0) {}
		});
		
		JButton move = new JButton("Move player here");
		JButton movestart = new JButton("Set starting coordinates");
        JLabel xLabel = new JLabel("X COORD");
        JLabel yLabel = new JLabel("Y COORD");
		JTextArea xText = new JTextArea("0");
		JTextArea yText = new JTextArea("0");
		
        JPanel pan = new JPanel();
        pan.setPreferredSize(new Dimension(200,50));
        pan.setMinimumSize(new Dimension(200,50));
        pan.setMaximumSize(new Dimension(200,50));
        pan.setLayout(new GridLayout(2, 2, 5, 5));
        
        pan.add(xLabel);
        pan.add(yLabel);
        pan.add(xText);
        pan.add(yText);
		
        JPanel infopanel = new JPanel();
        infopanel.add(move);
        infopanel.add(movestart);
        
        player_window.add(infopanel, BorderLayout.NORTH);
        player_window.add(pan, BorderLayout.CENTER);
        
        // send to editor-logic to add events and code.
        editor.getEditorLogic().initPlayerWindow(player_window, xText, yText, move, movestart);
	}
	
		//--------------------------------------------
		// createLogWindow()
		//--------------------------------------------		
		private void createLogWindow() {
			log_window = new JFrame("LOG");
			log_window.setAlwaysOnTop(true);
			log_window.setSize(350, 150);
			log_window.getRootPane().setWindowDecorationStyle(JRootPane.FRAME);
			log_window.setUndecorated(true);
			log_window.setLocation(Toolkit.getDefaultToolkit().getScreenSize().width - 350, 855);
			
			// Make so that the window just minimizes
			log_window.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
			log_window.addWindowListener(new WindowListener() {

				@Override
				public void windowClosing(WindowEvent arg0) {
					log_window.setState(Frame.ICONIFIED);
				}
		
				// Ignore these...
				public void windowActivated(WindowEvent arg0) {}
				public void windowClosed(WindowEvent arg0) {}
				public void windowDeactivated(WindowEvent arg0) {}
				public void windowDeiconified(WindowEvent arg0) {}
				public void windowIconified(WindowEvent arg0) {}
				public void windowOpened(WindowEvent arg0) {}
			});
			
			JTextPane textPane = new JTextPane();
			textPane.setEditable(false);
			
			JScrollPane scrollmainpane = new JScrollPane(textPane);
			scrollmainpane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
			scrollmainpane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			
			log_window.add(scrollmainpane);
			log_window.setVisible(true);
			
	        // send to editor-logic to add events and code.
	        editor.getEditorLogic().initLogWindow(log_window, textPane);
		}
		
	
	//--------------------------------------------
	// createCameraWindow()
	//--------------------------------------------		
	private void createCameraWindow() {
		
		camera_window = new JFrame("CAMERA SETTINGS");
		camera_window.setAlwaysOnTop(true);
		camera_window.setSize(400, 200);
		
		// Make so that the window just hides when closed
		camera_window.addWindowListener(new WindowListener() {

			@Override
			public void windowClosing(WindowEvent arg0) {
				camera_window.setVisible(false);
			}
	
			// Ignore these...
			public void windowActivated(WindowEvent arg0) {}
			public void windowClosed(WindowEvent arg0) {}
			public void windowDeactivated(WindowEvent arg0) {}
			public void windowDeiconified(WindowEvent arg0) {}
			public void windowIconified(WindowEvent arg0) {}
			public void windowOpened(WindowEvent arg0) {}
		});
        
		camera_window.setLayout(new BorderLayout());
		
        JButton add = new JButton("Add waypoint");
        JButton remove = new JButton("Remove");
        
        remove.setPreferredSize(new Dimension(100,25));
        remove.setMinimumSize(new Dimension(100,25));
        remove.setMaximumSize(new Dimension(100,25));
        
        add.setPreferredSize(new Dimension(130,25));
        add.setMinimumSize(new Dimension(130,25));
        add.setMaximumSize(new Dimension(130,25));

        JPanel pan = new JPanel();
        pan.setPreferredSize(new Dimension(200,50));
        pan.setMinimumSize(new Dimension(200,50));
        pan.setMaximumSize(new Dimension(200,50));
        pan.setLayout(new GridLayout(2, 4, 5, 5));
        
        JLabel xLabel = new JLabel("X COORD");
        JLabel yLabel = new JLabel("Y COORD");
        JLabel speedLabel = new JLabel("SPEED");
        JLabel waitLabel = new JLabel("TIME (seconds)");
        
        JTextArea xText = new JTextArea("0");
        JTextArea yText = new JTextArea("0");
        JTextArea speedText = new JTextArea("0");
        JTextArea waitText = new JTextArea("0");
        
        pan.add(xLabel);
        pan.add(yLabel);
        pan.add(speedLabel);
        pan.add(waitLabel);
        
        pan.add(xText);
        pan.add(yText);
        pan.add(speedText);
        pan.add(waitText);
        
        DefaultListModel<String> listModel = new DefaultListModel<String>();
        JList<String> waypoint_list = new JList<String>(listModel);
        waypoint_list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        waypoint_list.setVisibleRowCount(-1);
        waypoint_list.setBorder(new LineBorder(Color.black, 1));
        
        JPanel infopanel = new JPanel();
        infopanel.add(add);
        infopanel.add(remove);
        
        camera_window.add(infopanel, BorderLayout.NORTH);
        camera_window.add(waypoint_list, BorderLayout.SOUTH);
        camera_window.add(pan, BorderLayout.CENTER);
        
        // send to editor-logic to add events and code.
        editor.getEditorLogic().initCameraWindow(camera_window, xText, yText, speedText, waitText, add, remove, waypoint_list, listModel);
	}
	
	//--------------------------------------------
	// createEventWindow()
	//--------------------------------------------		
	private void createEventWindow() {
		
		event_window = new JFrame("EVENTS");
		event_window.setAlwaysOnTop(true);
		event_window.setSize(800, 600);
		
		// Make so that the window just hides when closed
		event_window.addWindowListener(new WindowListener() {

			@Override
			public void windowClosing(WindowEvent arg0) {
				event_window.setVisible(false);
			}
	
			// Ignore these...
			public void windowActivated(WindowEvent arg0) {}
			public void windowClosed(WindowEvent arg0) {}
			public void windowDeactivated(WindowEvent arg0) {}
			public void windowDeiconified(WindowEvent arg0) {}
			public void windowIconified(WindowEvent arg0) {}
			public void windowOpened(WindowEvent arg0) {}
		});
        
		
        JButton add = new JButton("Add waypoint");
        JButton remove = new JButton("Remove");
        
        remove.setPreferredSize(new Dimension(100,25));
        remove.setMinimumSize(new Dimension(100,25));
        remove.setMaximumSize(new Dimension(100,25));
        
        add.setPreferredSize(new Dimension(130,25));
        add.setMinimumSize(new Dimension(130,25));
        add.setMaximumSize(new Dimension(130,25));

        JPanel pan = new JPanel();
        pan.setPreferredSize(new Dimension(350,50));
        pan.setMinimumSize(new Dimension(350,50));
        pan.setMaximumSize(new Dimension(350,50));
        pan.setLayout(new GridLayout(2, 4, 5, 5));
        
        JLabel xLabel = new JLabel("X COORD");
        JLabel yLabel = new JLabel("Y COORD");
        JLabel speedLabel = new JLabel("SPEED");
        JLabel waitLabel = new JLabel("TIME (seconds)");
        
        JTextArea xText = new JTextArea("0");
        JTextArea yText = new JTextArea("0");
        JTextArea speedText = new JTextArea("0");
        JTextArea waitText = new JTextArea("0");
        
        pan.add(xLabel);
        pan.add(yLabel);
        pan.add(speedLabel);
        pan.add(waitLabel);
        
        pan.add(xText);
        pan.add(yText);
        pan.add(speedText);
        pan.add(waitText);
        
        DefaultListModel<String> listModel = new DefaultListModel<String>();
        JList<String> waypoint_list = new JList<String>(listModel);
        waypoint_list.setPreferredSize(new Dimension(600, 150));
        waypoint_list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        waypoint_list.setVisibleRowCount(-1);
        waypoint_list.setBorder(new LineBorder(Color.black, 1));
        
        JPanel infopanel = new JPanel();
        infopanel.add(add);
        infopanel.add(remove);
        
        // Create the waypoint panel and add all waypoint related components to it.
        JPanel wayp_panel = new JPanel();
        wayp_panel.add(infopanel, BorderLayout.NORTH);
        wayp_panel.add(pan, BorderLayout.CENTER);
        wayp_panel.add(waypoint_list, BorderLayout.SOUTH);
        
        
        // Create the EventObject buttons
        JButton addEvent = new JButton("Add to event");
        JButton removeEvent = new JButton("Remove from event");
        addEvent.setPreferredSize(new Dimension(130,25));
        addEvent.setMinimumSize(new Dimension(130,25));
        addEvent.setMaximumSize(new Dimension(130,25));
        removeEvent.setPreferredSize(new Dimension(100,25));
        removeEvent.setMinimumSize(new Dimension(100,25));
        removeEvent.setMaximumSize(new Dimension(100,25));
        
        // Create the EventObject labels
        JLabel eventLabel1 = new JLabel("game.level.vector<GameObject>:");
        JLabel eventLabel2 = new JLabel("this.vector<GameObject>:");
        
        // Create the 2 lists with current objects in (ON) the world and in this particular event
        DefaultListModel<String> listModelLevelObject = new DefaultListModel<String>();
        JList<String> levelObject_list = new JList<String>(listModelLevelObject);
        levelObject_list.setPreferredSize(new Dimension(600,250));
        levelObject_list.setMinimumSize(new Dimension(600,250));
        levelObject_list.setMaximumSize(new Dimension(600,250));
        levelObject_list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        levelObject_list.setVisibleRowCount(-1);
        levelObject_list.setBorder(new LineBorder(Color.black, 1));
        
        DefaultListModel<String> listModelEventObject = new DefaultListModel<String>();
        JList<String> eventObject_list = new JList<String>(listModelEventObject);
        eventObject_list.setPreferredSize(new Dimension(600,250));
        eventObject_list.setMinimumSize(new Dimension(600,250));
        eventObject_list.setMaximumSize(new Dimension(600,250));
        eventObject_list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        eventObject_list.setVisibleRowCount(-1);
        eventObject_list.setBorder(new LineBorder(Color.black, 1));
        
        // Add all event-related components onto this panel
        JPanel ev_panel = new JPanel();
        ev_panel.setPreferredSize(new Dimension(700,550));
        ev_panel.setMinimumSize(new Dimension(700,550));
        ev_panel.setMaximumSize(new Dimension(700,550));

        
		// GridBagLayout
		GridBagLayout theLayout = new GridBagLayout();
		GridBagConstraints con = new GridBagConstraints();
		con.gridx = 0;
		con.gridy = 0;
		con.gridheight = 1;
		ev_panel.setLayout(theLayout);
		

        // Scrollbar
		JScrollPane scpane = new JScrollPane(levelObject_list);
		scpane.setPreferredSize(new Dimension(600,250));
		scpane.setMinimumSize(new Dimension(600,250));
		scpane.setMaximumSize(new Dimension(600,250));
		scpane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		theLayout.setConstraints(scpane, con);
		ev_panel.add(scpane);
		
		con.gridx = 1;
		con.gridy = 0;
        
		theLayout.setConstraints(addEvent, con);
		ev_panel.add(addEvent);
		
		con.gridx = 0;
		con.gridy = 1;

		
		theLayout.setConstraints(eventObject_list, con);
		ev_panel.add(eventObject_list);
		
		con.gridx = 1;
		con.gridy = 1;
		
		theLayout.setConstraints(removeEvent, con);
		ev_panel.add(removeEvent);
		

        // Add to window
        event_window.setLayout(new GridLayout(2, 1));
        event_window.add(ev_panel);
        event_window.add(wayp_panel);
        
        // send to editor-logic to add events and code.
        editor.getEditorLogic().initEventWindow(event_window, xText, yText, speedText, waitText, add, remove, 
        		waypoint_list, listModel, eventObject_list, listModelEventObject, levelObject_list, 
        		listModelLevelObject, addEvent, removeEvent);
	}
	
		//--------------------------------------------
		// createObjectEventWindow()
		//--------------------------------------------		
		private void createObjectEventWindow() {
			
			object_event_window = new JFrame("OBJECT-EVENTS");
			object_event_window.setAlwaysOnTop(true);
			object_event_window.setSize(800, 600);
			
			// Make so that the window just hides when closed
			object_event_window.addWindowListener(new WindowListener() {

				@Override
				public void windowClosing(WindowEvent arg0) {
					object_event_window.setVisible(false);
				}
		
				// Ignore these...
				public void windowActivated(WindowEvent arg0) {}
				public void windowClosed(WindowEvent arg0) {}
				public void windowDeactivated(WindowEvent arg0) {}
				public void windowDeiconified(WindowEvent arg0) {}
				public void windowIconified(WindowEvent arg0) {}
				public void windowOpened(WindowEvent arg0) {}
			});
	        
			
	        JButton add = new JButton("Add waypoint");
	        JButton remove = new JButton("Remove");
	        
	        remove.setPreferredSize(new Dimension(100,25));
	        remove.setMinimumSize(new Dimension(100,25));
	        remove.setMaximumSize(new Dimension(100,25));
	        
	        add.setPreferredSize(new Dimension(130,25));
	        add.setMinimumSize(new Dimension(130,25));
	        add.setMaximumSize(new Dimension(130,25));

	        JPanel pan = new JPanel();
	        pan.setPreferredSize(new Dimension(350,50));
	        pan.setMinimumSize(new Dimension(350,50));
	        pan.setMaximumSize(new Dimension(350,50));
	        pan.setLayout(new GridLayout(2, 4, 5, 5));
	        
	        JLabel xLabel = new JLabel("X COORD");
	        JLabel yLabel = new JLabel("Y COORD");
	        JLabel speedLabel = new JLabel("SPEED");
	        JLabel waitLabel = new JLabel("TIME (seconds)");
	        
	        JTextArea xText = new JTextArea("0");
	        JTextArea yText = new JTextArea("0");
	        JTextArea speedText = new JTextArea("0");
	        JTextArea waitText = new JTextArea("0");
	        
	        pan.add(xLabel);
	        pan.add(yLabel);
	        pan.add(speedLabel);
	        pan.add(waitLabel);
	        
	        pan.add(xText);
	        pan.add(yText);
	        pan.add(speedText);
	        pan.add(waitText);
	        
	        DefaultListModel<String> listModel = new DefaultListModel<String>();
	        JList<String> waypoint_list = new JList<String>(listModel);
	        waypoint_list.setPreferredSize(new Dimension(600, 150));
	        waypoint_list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	        waypoint_list.setVisibleRowCount(-1);
	        waypoint_list.setBorder(new LineBorder(Color.black, 1));
	        
	        JPanel infopanel = new JPanel();
	        infopanel.add(add);
	        infopanel.add(remove);
	        
	        // Create the waypoint panel and add all waypoint related components to it.
	        JPanel wayp_panel = new JPanel();
	        wayp_panel.add(infopanel, BorderLayout.NORTH);
	        wayp_panel.add(pan, BorderLayout.CENTER);
	        wayp_panel.add(waypoint_list, BorderLayout.SOUTH);
	         
	        // Create the EventObject buttons
	        JButton addEvent = new JButton("Add to event");
	        JButton removeEvent = new JButton("Remove from event");
	        addEvent.setPreferredSize(new Dimension(130,25));
	        addEvent.setMinimumSize(new Dimension(130,25));
	        addEvent.setMaximumSize(new Dimension(130,25));
	        removeEvent.setPreferredSize(new Dimension(100,25));
	        removeEvent.setMinimumSize(new Dimension(100,25));
	        removeEvent.setMaximumSize(new Dimension(100,25));
	        
	        // Create the EventObject labels
	        JLabel eventLabel1 = new JLabel("Objects in game.level.vector<GameObject> objects:");
	        JLabel eventLabel2 = new JLabel("Objects in this event:");
	        
	        // Create the 2 lists with current objects in (ON) the world and in this particular event
	        DefaultListModel<String> listModelLevelObject = new DefaultListModel<String>();
	        JList<String> levelObject_list = new JList<String>(listModelLevelObject);
	        levelObject_list.setPreferredSize(new Dimension(600, 650));
	        levelObject_list.setMinimumSize(new Dimension(600,650));
	        levelObject_list.setMaximumSize(new Dimension(600,650));
	        levelObject_list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	        levelObject_list.setVisibleRowCount(-1);
	        levelObject_list.setBorder(new LineBorder(Color.black, 1));
	        
	        DefaultListModel<String> listModelEventObject = new DefaultListModel<String>();
	        JList<String> eventObject_list = new JList<String>(listModelEventObject);
	        eventObject_list.setPreferredSize(new Dimension(600, 650));
	        eventObject_list.setMinimumSize(new Dimension(600,650));
	        eventObject_list.setMaximumSize(new Dimension(600,650));
	        eventObject_list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	        eventObject_list.setVisibleRowCount(-1);
	        eventObject_list.setBorder(new LineBorder(Color.black, 1));
	        
	        // Add all event-related components onto this panel
	        JPanel ev_panel = new JPanel();
	        ev_panel.setPreferredSize(new Dimension(600,750));
	        ev_panel.setMinimumSize(new Dimension(600,750));
	        ev_panel.setMaximumSize(new Dimension(600,750));
	      
	        
	        ev_panel.setLayout(new GridLayout(6, 1, 5, 5));
	        
	        // Scrollbar
			JScrollPane scpane = new JScrollPane(levelObject_list);
			scpane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	        
	        ev_panel.add(eventLabel1);
	        ev_panel.add(scpane);
	        ev_panel.add(addEvent);
	        ev_panel.add(eventLabel2);
	        ev_panel.add(eventObject_list);
	        ev_panel.add(removeEvent);
	        
	        // Add to window
	        object_event_window.setLayout(new GridLayout(2, 1));
	        object_event_window.add(ev_panel);
	        object_event_window.add(wayp_panel);
	        
	        // send to editor-logic to add events and code.
	        editor.getEditorLogic().initEventGameObjectWindow(object_event_window, xText, yText, speedText, waitText, add, remove, 
	        		waypoint_list, listModel, eventObject_list, listModelEventObject, levelObject_list, 
	        		listModelLevelObject, addEvent, removeEvent);
		}
		
	
	//--------------------------------------------
	// createConsoleWindow()
	//--------------------------------------------		
	private void createConsoleWindow() {
		console_window = new JFrame("CONSOLE");
		console_window.setSize(550, 350);
		
		// Make so that the window just hides when closed
		console_window.addWindowListener(new WindowListener() {

			@Override
			public void windowClosing(WindowEvent arg0) {
				console_window.setVisible(false);
			}
	
			// Ignore these...
			public void windowActivated(WindowEvent arg0) {}
			public void windowClosed(WindowEvent arg0) {}
			public void windowDeactivated(WindowEvent arg0) {}
			public void windowDeiconified(WindowEvent arg0) {}
			public void windowIconified(WindowEvent arg0) {}
			public void windowOpened(WindowEvent arg0) {}
		});
		
		JTextField console_input = new JTextField("");
		console_input.setPreferredSize(new Dimension(500,25));
		console_input.setMaximumSize(new Dimension(500,25));
		console_input.setMinimumSize(new Dimension(500,25));
		
		JTextArea console_output = new JTextArea();
		console_output.setPreferredSize(new Dimension(500,270));
		console_output.setMaximumSize(new Dimension(500,270));
		console_output.setMinimumSize(new Dimension(500,270));
		console_output.setEditable(false);
		
		JScrollPane consolepane = new JScrollPane(console_output);
		consolepane.setPreferredSize(new Dimension(500,270));
		consolepane.setMaximumSize(new Dimension(500,270));
		consolepane.setMinimumSize(new Dimension(500,270));
		consolepane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		consolepane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		// GridBagLayout
		GridBagLayout theLayout = new GridBagLayout();
		GridBagConstraints con = new GridBagConstraints();
		con.gridx = 0;
		con.gridy = 0;
		con.gridheight = 3;
		console_window.setLayout(theLayout);
		
		theLayout.setConstraints(consolepane, con);
		console_window.add(consolepane);
		
		con.gridx = 0;
		con.gridy = 3;
		con.gridheight = 1;
		
		theLayout.setConstraints(console_input, con);
		console_window.add(console_input);

        // send to editor-logic to add events and code.
        editor.getEditorLogic().initConsoleWindow(console_window, console_input, console_output);
	}
	
	//--------------------------------------------
	// createSelectedObjectWindow()
	//--------------------------------------------		
	private void createSelectedObjectWindow() {
		sel_obj_window = new JFrame("SELECTED OBJECT");
		sel_obj_window.setAlwaysOnTop(true);
		sel_obj_window.setSize(260, 700);
		sel_obj_window.getRootPane().setWindowDecorationStyle(JRootPane.FRAME);
		sel_obj_window.setUndecorated(true);
		sel_obj_window.setLocation(Toolkit.getDefaultToolkit().getScreenSize().width - 270, 150);
		
		// Make so that the window just minimizes
		sel_obj_window.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		sel_obj_window.addWindowListener(new WindowListener() {

			@Override
			public void windowClosing(WindowEvent arg0) {
				sel_obj_window.setState(Frame.ICONIFIED);
			}
	
			// Ignore these...
			public void windowActivated(WindowEvent arg0) {}
			public void windowClosed(WindowEvent arg0) {}
			public void windowDeactivated(WindowEvent arg0) {}
			public void windowDeiconified(WindowEvent arg0) {}
			public void windowIconified(WindowEvent arg0) {}
			public void windowOpened(WindowEvent arg0) {}
		});
		
		// Main JPanel
		JPanel mainpanel = new JPanel();
		
		mainpanel.setLayout(new GridLayout(0, 2, 0, 0));

		// Scrollpane for the main JPanel
		JScrollPane scrollmainpane = new JScrollPane(mainpanel);
		scrollmainpane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scrollmainpane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		// Create all "header labels"
		Font boldfont = new Font("Arial", Font.BOLD, 14);
		JLabel ObjectInfoLabel = new JLabel("<html><u>Information");
		JLabel CoordinatesLabel = new JLabel("<html><u>Coordinates");
		JLabel HitboxLabel = new JLabel("<html><u>Hitbox");
		JLabel SpriteLabel = new JLabel("<html><u>Sprite");
		JLabel EventLabel = new JLabel("<html><u>Events");
		JLabel AILabel = new JLabel("<html><u>AI");
		
		// Fonts for headerlabels
		ObjectInfoLabel.setFont(boldfont);
		CoordinatesLabel.setFont(boldfont);
		HitboxLabel.setFont(boldfont);
		SpriteLabel.setFont(boldfont);
		EventLabel.setFont(boldfont);
		AILabel.setFont(boldfont);
		
		// Delete this object button
		JButton delete_object = new JButton("Delete Object");
		
		// Textfields
		// Info
		JLabel idLabel = new JLabel("ID:");
		JLabel typeLabel = new JLabel("Type:");
		JTextField id = new JTextField("");
		JTextField type = new JTextField("");
		type.setEditable(false);
		
		// Coordinates
		JLabel xLabel = new JLabel("x:");
		JLabel yLabel = new JLabel("y:");
		JTextField x = new JTextField("");
		JTextField y = new JTextField("");
		
		// Hitbox
		JLabel widthLabel = new JLabel("Width");
		JLabel heightLabel = new JLabel("Height:");
		JTextField width = new JTextField("");
		JTextField height = new JTextField("");
		JCheckBox collidable = new JCheckBox("Collidable");
	
		// Sprite
		JLabel txLabel = new JLabel("Texture x:");
		JLabel tyLabel = new JLabel("Texture y:");
		JLabel sxLabel = new JLabel("Size x:");
		JLabel syLabel = new JLabel("Size y:");
		JLabel scalexLabel = new JLabel("Scale x:");
		JLabel scaleyLabel = new JLabel("Scale y:");
		JLabel relativeLeftLabel = new JLabel("(relative) Left");
		JLabel relativeTopLabel = new JLabel("(relative) Top:");
		JLabel alphaLabel = new JLabel("Alpha:");
		JTextField tx = new JTextField("");
		JTextField ty = new JTextField("");
		JTextField sx = new JTextField("");
		JTextField sy = new JTextField("");
		JTextField relativeleft = new JTextField("");
		JTextField relativetop = new JTextField("");
		JTextField alpha = new JTextField("");
		JTextField scalex = new JTextField("");
		JTextField scaley = new JTextField("");
		
		// Events
		JCheckBox event_collidable = new JCheckBox("Event-Collidable");
		JButton addevent = new JButton("Add event(s)");
		
		// AI
		JLabel speedLabel = new JLabel("Speed:");
		JTextField speed = new JTextField("");
		
		// Code Modification
		JButton initButton = new JButton("Re-run init();");
		
		
		// ADD ALL COMPONENTS TO THE MAINPANEL
		mainpanel.add(delete_object);
		mainpanel.add(new JPanel());
		
		mainpanel.add(ObjectInfoLabel);
		mainpanel.add(new JPanel());
		
		mainpanel.add(idLabel);
		mainpanel.add(id);
		
		mainpanel.add(typeLabel);
		mainpanel.add(type);
		
		mainpanel.add(CoordinatesLabel);
		mainpanel.add(new JPanel());
		
		mainpanel.add(xLabel);
		mainpanel.add(x);
		
		mainpanel.add(yLabel);
		mainpanel.add(y);
		
		mainpanel.add(HitboxLabel);
		mainpanel.add(new JPanel());
		
		mainpanel.add(widthLabel);
		mainpanel.add(width);
		
		mainpanel.add(heightLabel);
		mainpanel.add(height);
		
		mainpanel.add(collidable);
		mainpanel.add(new JPanel());
		
		mainpanel.add(SpriteLabel);
		mainpanel.add(new JPanel());
		
		mainpanel.add(txLabel);
		mainpanel.add(tx);
		
		mainpanel.add(tyLabel);
		mainpanel.add(ty);
		
		mainpanel.add(sxLabel);
		mainpanel.add(sx);
		
		mainpanel.add(syLabel);
		mainpanel.add(sy);
		
		mainpanel.add(relativeLeftLabel);
		mainpanel.add(relativeleft);
		
		mainpanel.add(relativeTopLabel);
		mainpanel.add(relativetop);
		
		mainpanel.add(scalexLabel);
		mainpanel.add(scalex);
		
		mainpanel.add(scaleyLabel);
		mainpanel.add(scaley);
		
		mainpanel.add(alphaLabel);
		mainpanel.add(alpha);
		
		mainpanel.add(EventLabel);
		mainpanel.add(new JPanel());
		
		mainpanel.add(event_collidable);
		mainpanel.add(new JPanel());
		
		mainpanel.add(addevent);
		mainpanel.add(new JPanel());
		
		mainpanel.add(AILabel);
		mainpanel.add(new JPanel());
		
		mainpanel.add(speedLabel);
		mainpanel.add(speed);
		
		mainpanel.add(initButton);

		sel_obj_window.add(scrollmainpane);

        // send to editor-logic to add events and code.
       editor.getEditorLogic().initCurrentSelectedObjectWindow(sel_obj_window, delete_object, id, type, x, y, width, height, collidable, tx, ty, sx, sy, scalex, scaley, alpha, event_collidable, addevent, speed, relativeleft, relativetop, initButton);
	}
	
	//--------------------------------------------
	// createMenu()
	//--------------------------------------------	
	private void createMenu() {

		JMenuBar theMenu = new JMenuBar();
		JMenu menuFile = new JMenu("File");
		JMenu menuSettings = new JMenu("Settings");
		JMenu menuHelp = new JMenu("Help");
		theMenu.add(menuFile);
		theMenu.add(menuSettings);
		theMenu.add(menuHelp);
		
		// Add the new option
		JMenuItem neww = new JMenuItem("New");
		neww.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	// logic object
            }
        });
		menuFile.add(neww);
		
		// Add separator
		menuFile.add(new JSeparator());
		
		// Add the open file
		JMenuItem open = new JMenuItem("Open");
		open.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	editor.getEditorLogic().loadLevel();
            }
        });
		menuFile.add(open);
		
		// Add the save file
		JMenuItem save = new JMenuItem("Save");
		save.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	editor.getEditorLogic().saveLevel();
            }
        });      
		menuFile.add(save);
		
		menuFile.add(new JSeparator());
		menuFile.add(new JMenuItem("Exit"));
		
		// SETTINGS
		// Add the reset
		JMenuItem reset = new JMenuItem("Reset level");
		reset.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	editor.getEditorLogic().resetLevel();
            }
        });      
		menuSettings.add(reset);
		
		window.setJMenuBar(theMenu);
	}
	
	//--------------------------------------------
	// createToolBar()
	//--------------------------------------------	
	private void createToolBar(GridBagLayout theLayout, GridBagConstraints con) {
		
		JToolBar toolbar = new JToolBar();
		toolbar.setFloatable(false);
			
		JButton mButton = createButton("LEVEL SETTINGS", new Font("Arial", Font.BOLD, 14));
		mButton.setIcon(new ImageIcon("editordata/level.png"));
		mButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	editor.getEditorLogic().showPlayerSettings();
            }
        });
		toolbar.add(mButton);
		
		JButton conButton = createButton("CONSOLE", new Font("Arial", Font.BOLD, 14));
		conButton.setIcon(new ImageIcon("editordata/console.png"));
		conButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	editor.getEditorLogic().showConsoleWindow();
            }
        });
		toolbar.add(conButton);
		
		final JButton switchButton = createButton("TURN GRID ON", new Font("Arial", Font.BOLD, 14));
		switchButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	switchButton.setText(editor.getEditorLogic().gridSwitch());
            }
        });
		
		switchButton.setIcon(new ImageIcon("editordata/grid.png"));
		toolbar.add(switchButton);
		
		
		final JButton hitboxSwitchButton = createButton("TURN HITBOXES ON", new Font("Arial", Font.BOLD, 14));
		hitboxSwitchButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	hitboxSwitchButton.setText(editor.getEditorLogic().hitboxSwitch());
            }
        });
		
		hitboxSwitchButton.setIcon(new ImageIcon("editordata/hitbox.png"));
		toolbar.add(hitboxSwitchButton);
		toolbar.addSeparator(new Dimension(15, 0));

		

		
		final JButton colorButton = createButton("Grid/Hitbox Color", new Font("Arial", Font.BOLD, 12));
		colorButton.setBackground(Color.green);
		colorButton.setPreferredSize(new Dimension(130, 32));

		colorButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	Color c = JColorChooser.showDialog(null, "Choose a Color", colorButton.getBackground());
                if (c != null) {
                	colorButton.setBackground(c);
                	editor.getEditorLogic().changeGridColor(c.getRed(), c.getGreen(), c.getBlue());
                }
            }
        });
		
		toolbar.add(colorButton);
		toolbar.addSeparator(new Dimension(40, 35));

		String[] lStrings = { "0% transparency ", "50% transparency", "75% transparency", "100% transparency" };
		JComboBox<String> transList = new JComboBox<String>(lStrings);
		transList.setSelectedIndex(0);
		transList.setPreferredSize(new Dimension(200, 25));
		transList.setMinimumSize(new Dimension(200, 25));
		transList.setMaximumSize(new Dimension(200, 25));
		toolbar.add(transList);
		toolbar.addSeparator(new Dimension(15, 0));
		
		JCheckBox snapToGrid = new JCheckBox("Snap-to-grid when placing objects", false);
		snapToGrid.setPreferredSize(new Dimension(380, 25));
		snapToGrid.setMinimumSize(new Dimension(320, 25));
		snapToGrid.setMaximumSize(new Dimension(320, 25));
		snapToGrid.setAlignmentX(Component.LEFT_ALIGNMENT);
		toolbar.add(snapToGrid);
		
		// Send to editorlogic
		editor.getEditorLogic().layerTransparency(transList);
		editor.getEditorLogic().snapToGrid(snapToGrid);
		
		// Create the toolgrid panel with a GridBagLayout
		toolgrid = new JPanel();
		toolgrid.setLayout(theLayout);
		window.add(toolgrid, BorderLayout.NORTH);
		
		con.gridx = 0;
		con.gridy = 0;
		con.weightx = 1;
		con.anchor = GridBagConstraints.WEST;
		
		
		
		// Create the toolpanel, add it to the toolgrid, and add the toolbar to the toolpanel
		toolpanel = new JPanel();
		toolpanel.add(toolbar);
		theLayout.setConstraints(toolpanel, con);
		toolgrid.add(toolpanel);
	}
	
	//--------------------------------------------
	// createPanel() - The panel on the left
	//--------------------------------------------
	private void createSidePanel() {
		
		side = new JPanel();
		side.setPreferredSize(new Dimension(400,700));
		window.getContentPane().add(side, BorderLayout.WEST);
	}
	
	//--------------------------------------------
	// createStatusBar() - The panel on the bottom
	//--------------------------------------------
	private void createStatusBar() {
		
		status = new JPanel();
		status.setPreferredSize(new Dimension(700,30));
		
		JLabel world = new JLabel("World: 0,0");
		JLabel relative = new JLabel("Relative: 0,0");
		JLabel element = new JLabel("Element: 0");
		
		world.setFont(new Font("Arial", Font.BOLD, 14));
		relative.setFont(new Font("Arial", Font.BOLD, 14));
		element.setFont(new Font("Arial", Font.BOLD, 14));
		
		status.add(world);
		status.add(relative);
		status.add(element);
		
		editor.getEditorLogic().statusBarLogic(world, relative, element);
		
		window.getContentPane().add(status, BorderLayout.SOUTH);
	}
	
	//--------------------------------------------
	// createCanvas()
	//--------------------------------------------
	private void createCanvas() {
		
		// Config
		final LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Lazypointer";
		config.width = 800;
		config.height = 600;
		
		// Use a LwjglCanvas to draw the game on
		canvas = new LwjglCanvas(editor.getGame(), config);
		panelen.setBackground(new Color(11,11,11));
		panelen.add(canvas.getCanvas(), BorderLayout.LINE_START);
		canvas.getCanvas().setSize(1500, 1000);
		canvas.getCanvas().requestFocusInWindow();
	}
	
	//--------------------------------------------
	// createTabs()
	//--------------------------------------------
	private void createTabs(GridBagLayout theLayout, GridBagConstraints con) {

		JToolBar toolbar = new JToolBar();
		toolbar.setFloatable(false);
	
		JButton pButton = createButton("PLAY MODE", new Font("Arial", Font.BOLD, 14));
		toolbar.add(pButton);
		toolbar.addSeparator();
		
		JButton runframebutton = createButton("RUN FRAME(S):", new Font("Arial", Font.BOLD, 14));
		JTextField frames = new JTextField("1");
		frames.setPreferredSize(new Dimension(25,15));
		toolbar.add(runframebutton);
		toolbar.add(frames);
		toolbar.addSeparator();
		
		JButton nButton = createButton("NORMAL LAYER", new Font("Arial", Font.BOLD, 14));
		toolbar.add(nButton);
		toolbar.addSeparator();
		
		JButton oButton = createButton("OBJECT LAYER", new Font("Arial", Font.BOLD, 14));
		toolbar.add(oButton);
		toolbar.addSeparator();
		
		JButton bButton = createButton("BACKGROUND LAYER", new Font("Arial", Font.BOLD, 14));
		toolbar.add(bButton);
		toolbar.addSeparator();
		
		JButton fButton = createButton("FOREGROUND LAYER", new Font("Arial", Font.BOLD, 14));
		toolbar.add(fButton);
		toolbar.addSeparator();
		
		
		con.gridx = 0;
		con.gridy = 2;
		con.anchor = GridBagConstraints.WEST;

		JPanel layerpanel = new JPanel();

		layerpanel.add(toolbar);

		theLayout.setConstraints(layerpanel, con);
		toolgrid.add(layerpanel);
		
		editor.getEditorLogic().setupLayerPanel(pButton, nButton, oButton, bButton, fButton, runframebutton ,frames);
		
	}
	
	//------------------------------------------------------------------------------
	// sidePanelSetup()
	//------------------------------------------------------------------------------
	private void sidePanelSetup() {
		
		// Current selected object panel
		JPanel currentObjectPanel = new JPanel();
		currentObjectPanel.setBorder(new TitledBorder(null, "Current Object", 0, 0, new Font("Arial", Font.BOLD, 14)));
		currentObjectPanel.setPreferredSize(new Dimension(390, 220));
		currentObjectPanel.setMinimumSize(new Dimension(390, 220));
		currentObjectPanel.setMaximumSize(new Dimension(390, 220));

		// Create the components
		JLabel n_label = new JLabel("Object ID (must be unique for non-tiles):");
		JTextField name = new JTextField("Empty Object");
		name.setFont(new Font("Arial", Font.BOLD, 12));
		name.setPreferredSize(new Dimension(380, 25));
		name.setMinimumSize(new Dimension(380, 25));
		name.setMaximumSize(new Dimension(380, 25));
		name.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		ImageIcon texture = new ImageIcon();
		JButton texturebutton = new JButton(texture);

		texturebutton.setFocusPainted(false);
		texturebutton.setContentAreaFilled(false);
		texturebutton.setPreferredSize(new Dimension(GLOBAL.TILE_SIZE, GLOBAL.TILE_SIZE));
		texturebutton.setMinimumSize(new Dimension(GLOBAL.TILE_SIZE, GLOBAL.TILE_SIZE));
		texturebutton.setMaximumSize(new Dimension(GLOBAL.TILE_SIZE, GLOBAL.TILE_SIZE));
		texturebutton.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		JCheckBox collidable = new JCheckBox("Collidable", false);
		collidable.setPreferredSize(new Dimension(380, 25));
		collidable.setMinimumSize(new Dimension(320, 25));
		collidable.setMaximumSize(new Dimension(320, 25));
		collidable.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		JLabel id_label = new JLabel("Object Type:");
		
		List<String> idStrings = editor.getEditorLogic().loadObjectIDs();		// Load in text-values for the ID's
		String[] ids = idStrings.toArray(new String[idStrings.size()]);
		JComboBox<String>  object_id = new JComboBox<String>(ids);
		object_id.setFont(new Font("Arial", Font.BOLD, 12));
		object_id.setPreferredSize(new Dimension(380, 25));
		object_id.setMinimumSize(new Dimension(380, 25));
		object_id.setMaximumSize(new Dimension(380, 25));
		object_id.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		JLabel addfunc_label = new JLabel("Add/Edit functionality:");
		String[] lStrings = { "Events", "-", "-" };
		JComboBox<String> funcList = new JComboBox<String>(lStrings);
		funcList.setSelectedIndex(0);
		funcList.setPreferredSize(new Dimension(380, 25));
		funcList.setMinimumSize(new Dimension(380, 25));
		funcList.setMaximumSize(new Dimension(380, 25));
		funcList.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		// Add to the panel
		currentObjectPanel.setLayout(new BoxLayout(currentObjectPanel, BoxLayout.PAGE_AXIS));
		currentObjectPanel.add(n_label);
		currentObjectPanel.add(name);
		currentObjectPanel.add(texturebutton);
		currentObjectPanel.add(collidable);
		currentObjectPanel.add(id_label);
		currentObjectPanel.add(object_id);
		currentObjectPanel.add(addfunc_label);
		currentObjectPanel.add(funcList);

		// Send this to editorlogic
		editor.getEditorLogic().initCurrentObject(name, texturebutton, collidable, object_id, funcList);

		
		
		
		
		
		// List of all objects panel
		JPanel objectsPanel = new JPanel();
		objectsPanel.setBorder(new TitledBorder(null, "Objects", 0, 0, new Font("Arial", Font.BOLD, 14)));
		objectsPanel.setPreferredSize(new Dimension(390, 320));
		objectsPanel.setMinimumSize(new Dimension(390, 320));
		objectsPanel.setMaximumSize(new Dimension(390, 320));	
		
		JButton createNewObject = new JButton("New object");
		createNewObject.setPreferredSize(new Dimension(110, 25));
		createNewObject.setMinimumSize(new Dimension(110, 25));
		createNewObject.setMaximumSize(new Dimension(110, 25));
		createNewObject.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		JButton deleteObject = new JButton("Delete object");
		deleteObject.setPreferredSize(new Dimension(110, 25));
		deleteObject.setMinimumSize(new Dimension(110, 25));
		deleteObject.setMaximumSize(new Dimension(110, 25));
		deleteObject.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		JButton clearObjects = new JButton("Clear list");
		clearObjects.setPreferredSize(new Dimension(110, 25));
		clearObjects.setMinimumSize(new Dimension(110, 25));
		clearObjects.setMaximumSize(new Dimension(110, 25));
		clearObjects.setAlignmentX(Component.LEFT_ALIGNMENT);

		JPanel objectList = new JPanel();
		objectList.setLayout(new BoxLayout(objectList, BoxLayout.PAGE_AXIS));
		objectList.setPreferredSize(new Dimension(370, 420));
		objectList.setMinimumSize(new Dimension(370, 420));
		objectList.setMaximumSize(new Dimension(370, 420));
		objectList.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		JScrollPane scrl = new JScrollPane(objectList);
		scrl.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrl.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrl.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		JPanel btpanel = new JPanel();
		btpanel.setPreferredSize(new Dimension(370, 40));
		btpanel.setMinimumSize(new Dimension(370, 40));
		btpanel.setMaximumSize(new Dimension(370, 40));
		btpanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		btpanel.add(createNewObject);
		btpanel.add(deleteObject);
		btpanel.add(clearObjects);
		
		// Add to the panel
		objectsPanel.setLayout(new BoxLayout(objectsPanel, BoxLayout.PAGE_AXIS));
		objectsPanel.add(btpanel);	
		objectsPanel.add(scrl);	
		
		// Send this to editorlogic
		editor.getEditorLogic().initObjectPanel(createNewObject, deleteObject, clearObjects, objectList);
		
		
		
		


		// List of all textures panel
		JPanel texturePanel = new JPanel();
		texturePanel.setBorder(new TitledBorder(null, "Textures", 0, 0, new Font("Arial", Font.BOLD, 14)));
		texturePanel.setPreferredSize(new Dimension(390, 300));
		texturePanel.setMinimumSize(new Dimension(390, 300));
		texturePanel.setMaximumSize(new Dimension(390, 300));
		
		JButton loadTexture = new JButton("Load texture");
		loadTexture.setPreferredSize(new Dimension(110, 25));
		loadTexture.setMinimumSize(new Dimension(110, 25));
		loadTexture.setMaximumSize(new Dimension(110, 25));
		loadTexture.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		JPanel textureList = new JPanel();
		textureList.setPreferredSize(new Dimension(370, 520));
		textureList.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		JScrollPane scrltex = new JScrollPane(textureList);
		scrltex.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrltex.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrltex.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		// Add to the panel
		texturePanel.setLayout(new BoxLayout(texturePanel, BoxLayout.PAGE_AXIS));
		texturePanel.add(loadTexture);	
		texturePanel.add(scrltex);	
		
		editor.getEditorLogic().loadNewTexture(GLOBAL.main_texture, textureList);
		
		// Add all "big" panels
		side.add(currentObjectPanel);
		side.add(objectsPanel);
		side.add(texturePanel);
		
		editor.getEditorLogic().setSide(side);
	}
	
	//--------------------------------------------
	// createButton()
	//--------------------------------------------
	private JButton createButton(String text, Font f) {
		JButton button = new JButton(text);
		button.setFont(f);
		return button;
	}
	

	
	
	private WorldEditor editor;
	private JFrame window;
	private LwjglCanvas canvas;		// the canvas where the game/world-grid is drawn
	private final JPanel panelen;	// contains the canvas
	private JPanel toolpanel;		// contains the tools
	private JPanel toolgrid;		// contains both toolpanel and layerpanel
	private JPanel side;			// the side-panel
	private JPanel status;			// status-bar-panel
	private JFrame camera_window;	// camera-window-settings
	private JFrame player_window;	// player-window-settings
	private JFrame event_window;		// event-window
	private JFrame object_event_window;	// object event-window
	private JFrame console_window;		// console-window
	private JFrame sel_obj_window;	// selected-object-window
	private JFrame log_window;		// log window
}
