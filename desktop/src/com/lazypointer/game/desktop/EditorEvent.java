package com.lazypointer.game.desktop;

import java.util.Vector;

import com.lazypointer.game.GameObject;
import com.lazypointer.game.Waypoint;

public class EditorEvent {
	
	 EditorEvent() {
		 event_objects = new Vector<GameObject>();
		 event_waypoints = new Vector<Waypoint>();
	 }
	
	public Vector<GameObject> event_objects;
	public Vector<Waypoint> event_waypoints;
}
