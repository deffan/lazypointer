package com.lazypointer.game.desktop;


import java.awt.Color;

import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import com.lazypointer.game.GLOBAL;

public class EditorLog {

	public EditorLog(JTextPane plogtextPane) {
		logtextPane = plogtextPane;
		doc = (StyledDocument) logtextPane.getDocument();
		
		// Default style
	    def_style = doc.addStyle("defstyle", null);
	    
	    // Bold style
	    def_bold = doc.addStyle("boldstyle", null);
	    StyleConstants.setBold(def_bold, true);
	    
	    // Error style (red+bold)
	    err_style = doc.addStyle("errstyle", null);
	    StyleConstants.setBold(err_style, true);
	    StyleConstants.setForeground(err_style, Color.red);
	   
	    // Info style (blue)
	    info_style = doc.addStyle("infostyle", null);
	    StyleConstants.setBold(info_style, true);
	    StyleConstants.setForeground(info_style, Color.blue);
	    
	    // Warning style (magenta)
	    warn_style = doc.addStyle("warnstyle", null);
	    StyleConstants.setBold(warn_style, true);
	    StyleConstants.setForeground(warn_style, Color.magenta);
	    
	    // Event style (green)
	    event_style = doc.addStyle("eventstyle", null);
	    StyleConstants.setBold(event_style, true);
	    StyleConstants.setForeground(event_style, new Color(10,100,0));
	}
	
	public void addMessage(int msg_type, String msg) {

		String pre_msg = "";
		switch(msg_type) {
			case GLOBAL.MSG_DEFAULT: current_style_pre = def_style; current_style_msg = def_style; break;
			case GLOBAL.MSG_DEFAULT_BOLD: current_style_pre = def_bold; current_style_msg = def_bold; break;
			case GLOBAL.MSG_ERROR: pre_msg = "[ERROR] "; current_style_pre = err_style; current_style_msg = def_bold; break;
			case GLOBAL.MSG_WARNING: pre_msg = "[WARNING] "; current_style_pre = warn_style; current_style_msg = def_style; break;
			case GLOBAL.MSG_INFORMATION: pre_msg = "[INFO] "; current_style_pre = info_style; current_style_msg = def_style; break;
			case GLOBAL.MSG_EVENT: pre_msg = "[EVENT] "; current_style_pre = event_style; current_style_msg = def_style; break;
			default: current_style_pre = def_style; current_style_msg = def_style; break;
		}

	    try {
			doc.insertString(doc.getLength(), pre_msg, current_style_pre);
			doc.insertString(doc.getLength(), msg + "\n", current_style_msg);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}
	
	private JTextPane logtextPane;
	private StyledDocument doc;
	private Style current_style_pre;
	private Style current_style_msg;
	private Style def_style;
	private Style def_bold;
	private Style err_style;
	private Style info_style;
	private Style warn_style;
	private Style event_style;
}
