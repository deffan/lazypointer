package com.lazypointer.game.desktop;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.Timer;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.badlogic.gdx.Gdx;
import com.lazypointer.game.Coordinate;
import com.lazypointer.game.GLOBAL;
import com.lazypointer.game.Hitbox;
import com.lazypointer.game.Level;
import com.lazypointer.game.Sprite;
import com.lazypointer.game.Tile;
import com.lazypointer.game.Waypoint;

public class EditorLogic {
	
	//------------------------------------------------------------------------------
	// constructor
	//------------------------------------------------------------------------------
	EditorLogic(WorldEditor e) {
		editor = e;
		current_object = null;
		editor_objects = new Vector<EditorObject>();
		console = null;
		objectCounter = 0;
		nowSelected = "";
	}
	
	//------------------------------------------------------------------------------
	// loadNewTexture
	//------------------------------------------------------------------------------	
	public void loadNewTexture(String filename, JPanel texturel) {

		// Set the texturelist jpanel
		texturelist = texturel;

		// Try reading in this texture from file
		BufferedImage bigImg = null;
		try {
			bigImg = ImageIO.read(new File(filename));
		} catch (IOException e) {
			e.printStackTrace();
			log.addMessage(GLOBAL.MSG_ERROR, "IOFoundException in Editor.loadNewTexture()");
			return;
		}

		// Rows and cols in the image.
		final int rows = bigImg.getHeight() / GLOBAL.TILE_SIZE;
		final int cols = bigImg.getWidth() / GLOBAL.TILE_SIZE;
		
		// GridBagLayout for the panel
		GridBagLayout theLayout = new GridBagLayout();
		GridBagConstraints con = new GridBagConstraints();
		con.gridx = 0;
		con.gridy = 0;
		texturelist.setLayout(theLayout);
		
		// Cut out each texture to be the size of "GLOBAL.TILE_SIZE" and make new EditorTextures.
		int row = 0;
		int rowcount = 0;
		for (int i = 0; i < rows; i++) {
		    for (int j = 0; j < cols; j++) {
		    	
		    	// Cut out this piece of the large texture
		    	BufferedImage img = bigImg.getSubimage(j * GLOBAL.TILE_SIZE, i * GLOBAL.TILE_SIZE, GLOBAL.TILE_SIZE, GLOBAL.TILE_SIZE);
		    	
		    	// Create a new EditorTexture and add it to the "texturelist" panel
		    	EditorTexture texturepiece = newTexture();
		    	texturepiece.texture = img;
		    	texturepiece.setIcon(new ImageIcon(img));
		    	texturepiece.texture_x = j * GLOBAL.TILE_SIZE;
		    	texturepiece.texture_y = i * GLOBAL.TILE_SIZE;
		    	theLayout.setConstraints(texturepiece, con);
		    	texturelist.add(texturepiece);
				con.gridx++;
				row++;
				
				// Change row in the list
				if(row * GLOBAL.TILE_SIZE > 300) {
					con.gridx = 0;
					con.gridy++;
					row = 0;
					rowcount++;
				}
		    }
		}
		
		// Change the size of the texturelist so the JScrollPane works...
		texturelist.setPreferredSize(new Dimension(300, rowcount * GLOBAL.TILE_SIZE));
	}
	
	//------------------------------------------------------------------------------
	// changeGridColor() - Change the color of the grid on the world
	//------------------------------------------------------------------------------
	public void changeGridColor(int r, int g, int b) {
		editor.getGame().getGame().getDraw().changeGridColor(r, g, b);
	}
	
	//------------------------------------------------------------------------------
	// setSide()
	//------------------------------------------------------------------------------
	public void setSide(JPanel s) {
		side = s;
	}
	
	//------------------------------------------------------------------------------
	// layerTransparency() - So you can change the transparency of the layers
	//------------------------------------------------------------------------------	
	public void layerTransparency(final JComboBox<String> transList) {
		
		transList.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				if(transList.getSelectedIndex() == 0)
					editor.getGame().setEditorAlpha(1);
				else if(transList.getSelectedIndex() == 1)
					editor.getGame().setEditorAlpha(0.5f);
				else if(transList.getSelectedIndex() == 2)
					editor.getGame().setEditorAlpha(0.25f);
				else if(transList.getSelectedIndex() == 3)
					editor.getGame().setEditorAlpha(0);

			}	
		});
	}
	
	//------------------------------------------------------------------------------
	// statusBarLogic()
	//------------------------------------------------------------------------------
	public void statusBarLogic(final JLabel w, final JLabel r, final JLabel e) {
		editor.getGame().setEditorStatusBar(w, r, e);
	}	
	
	//------------------------------------------------------------------------------
	// showPlayerSettings()
	//------------------------------------------------------------------------------
	public void showPlayerSettings() {
		// Show window
		player_window.setVisible(true); 
	}	
	
	
	//------------------------------------------------------------------------------
	// showConsoleWindow()
	//------------------------------------------------------------------------------
	public void showConsoleWindow() {

	}	
	
	//------------------------------------------------------------------------------
	// showCameraSettings()
	//------------------------------------------------------------------------------
	public void showCameraSettings() {
		
		/*
		// Clear the list
		camera_listmodel.clear();
		
		// Repopulate the list
		for(int i = 0; i < editor.getGame().getGame().getLevel().camera_waypoints.size(); i++) {
			String wp = "X: ";
			wp += editor.getGame().getGame().getLevel().camera_waypoints.elementAt(i).coordinates.x;
			wp += "\t Y: " + editor.getGame().getGame().getLevel().camera_waypoints.elementAt(i).coordinates.y;
			wp += "\t SPEED: " + editor.getGame().getGame().getLevel().camera_waypoints.elementAt(i).speed;
			wp += "\t TIMER: " + editor.getGame().getGame().getLevel().camera_waypoints.elementAt(i).getTime();
			camera_listmodel.addElement(wp);
		}
		
		// Show window
		camera_window.setVisible(true); 
		*/
	}
	
	//------------------------------------------------------------------------------
	// showEventWindow()
	//------------------------------------------------------------------------------
	public void showEventWindow() {
		
		// If this is the first time this window opens, make it "event enabled"
		if(current_object.editor_event == null)
			current_object.editor_event = new EditorEvent();
		
		// Clear the list of waypoints
		event_waypoint_listmodel.clear();

		// Repopulate the list
		for(int i = 0; i < current_object.editor_event.event_waypoints.size(); i++) {
			String wp = "X: ";
			wp += current_object.editor_event.event_waypoints.elementAt(i).coordinates.x;
			wp += "\t Y: " + current_object.editor_event.event_waypoints.elementAt(i).coordinates.y;
			wp += "\t SPEED: " + current_object.editor_event.event_waypoints.elementAt(i).speed;
			wp += "\t TIMER: " + current_object.editor_event.event_waypoints.elementAt(i).getTime();
			event_waypoint_listmodel.addElement(wp);
		}
		
		// Clear the list of active game-objects in the object-vector
		event_object_listmodel.clear();
		event_level_listmodel.clear();
		current_object.editor_event.event_objects.clear();	// This must be redone each time (for now...)
		
		// Repopulate the list
		for(int i = 0; i < editor.getGame().getGame().getLevel().objects.size(); i++) {
			String objStr = "Object ID: " + editor.getGame().getGame().getLevel().objects.elementAt(i).ID + "\t TYPE: " + editor.getGame().getGame().getLevel().objects.elementAt(i).TYPE;
			event_level_listmodel.addElement(objStr);
		}
		
	
		// Show window
		event_window.setVisible(true); 
	}
	
	//------------------------------------------------------------------------------
	// showObjectEventWindow()
	//------------------------------------------------------------------------------
	public void showObjectEventWindow() {
		
		// Clear the list of waypoints
		gameobject_event_waypoint_listmodel.clear();

		// Repopulate the list
		for(int i = 0; i < editor.getGame().getGame().selected_editor_object.getWaypoints().size(); i++) {
			String wp = "X: ";
			wp += editor.getGame().getGame().selected_editor_object.getWaypoints().elementAt(i).coordinates.x;
			wp += "\t Y: " + editor.getGame().getGame().selected_editor_object.getWaypoints().elementAt(i).coordinates.y;
			wp += "\t SPEED: " + editor.getGame().getGame().selected_editor_object.getWaypoints().elementAt(i).speed;
			wp += "\t TIMER: " + editor.getGame().getGame().selected_editor_object.getWaypoints().elementAt(i).getTime();
			gameobject_event_waypoint_listmodel.addElement(wp);
		}
		
		// Clear the list of active game-objects in the object-vector
		gameobject_event_object_listmodel.clear();
		gameobject_event_level_listmodel.clear();
		
		// Show window
		gameobject_event_window.setVisible(true); 
	}
	
	//------------------------------------------------------------------------------
	// initPlayerWindow()
	//------------------------------------------------------------------------------
	public void initPlayerWindow(JFrame pWindow, final JTextArea xText, final JTextArea yText, JButton move, JButton movestart) {
		
		player_window = pWindow;
		
		// Move the player
		move.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				editor.getGame().getGame().getPlayer().coordinates.x = (Integer.parseInt(xText.getText()));
				editor.getGame().getGame().getPlayer().coordinates.y = (Integer.parseInt(yText.getText()));
			}	
		});
		
		// Set the start coordinates of the player for this level
		move.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				editor.getGame().getGame().getLevel().player_start_x = Integer.parseInt(xText.getText());
				editor.getGame().getGame().getLevel().player_start_y = Integer.parseInt(yText.getText()); 
			}	
		});
	}	
	
	//------------------------------------------------------------------------------
	// initCameraWindow()
	//------------------------------------------------------------------------------
	public void initCameraWindow(JFrame cWindow, final JTextArea xText, final JTextArea yText, final JTextArea speedText, final JTextArea waitText, JButton add, JButton remove, final JList<String> waypoint_list, final DefaultListModel<String> listmodel) {
		camera_window = cWindow;
		camera_listmodel = listmodel;

		// Remove a waypoint
		remove.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				// Remove a waypoint from the vector and from the visual list
				if(camera_listmodel.getSize() > 0) {
					int index = waypoint_list.getSelectedIndex();
					if(index != -1) {
						listmodel.remove(index);
						//editor.getGame().getGame().getLevel().camera_waypoints.remove(index);
					}
				}
			}
				
		});
		
		// Add a waypoint
		add.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				String wp = "X: ";
				wp += xText.getText();
				wp += "\t Y: " + yText.getText();
				wp += "\t SPEED: " + speedText.getText();
				wp += "\t TIMER: " + waitText.getText();
				camera_listmodel.addElement(wp);
				//editor.getGame().getGame().getLevel().camera_waypoints.add(new Waypoint(Integer.parseInt(xText.getText()), Integer.parseInt(yText.getText()), Integer.parseInt(speedText.getText()), Integer.parseInt(waitText.getText())));
			}
				
		});

	}
	
	//------------------------------------------------------------------------------
	// initEventWindow()
	//------------------------------------------------------------------------------
	public void initEventWindow(JFrame cWindow, final JTextArea xText, final JTextArea yText, final JTextArea speedText, final JTextArea waitText, JButton add, JButton remove, 
			final JList<String> waypoint_list, final DefaultListModel<String> listmodel, 
			final JList<String> event_object_list, final DefaultListModel<String> event_listmodel, 
			final JList<String> level_object_list, final DefaultListModel<String> level_listmodel, 
			JButton addEvent, JButton removeEvent) 
	{
		// Add references
		event_window = cWindow;
		event_waypoint_listmodel = listmodel;
		event_object_listmodel = event_listmodel;
		event_level_listmodel = level_listmodel;
		
		// Remove a waypoint
		remove.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				// Remove a waypoint from the vector and from the visual list
				if(event_waypoint_listmodel.getSize() > 0) {
					int index = waypoint_list.getSelectedIndex();
					if(index != -1) {
						
						event_waypoint_listmodel.remove(index);
						current_object.editor_event.event_waypoints.remove(index);
					}
				}
			}
		});
		
		// Add a waypoint
		add.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				String wp = "X: ";
				wp += xText.getText();
				wp += "\t Y: " + yText.getText();
				wp += "\t SPEED: " + speedText.getText();
				wp += "\t TIMER: " + waitText.getText();
				event_waypoint_listmodel.addElement(wp);
				
				// add to current_object's EditorEvent
				current_object.editor_event.event_waypoints.add(new Waypoint(Integer.parseInt(xText.getText()), Integer.parseInt(yText.getText()), Integer.parseInt(speedText.getText()), Integer.parseInt(waitText.getText())));
			}
		});
		
		// Remove an event
		removeEvent.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				// Remove a event object
				if(event_object_listmodel.getSize() > 0) {
					int index = event_object_list.getSelectedIndex();
					if(index != -1) {

						event_object_listmodel.remove(index);
						
						if(current_object.editor_event != null)
							current_object.editor_event.event_objects.remove(index);
					}
				}
			}
				
		});
		
		// Add a object to events
		addEvent.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				// SELECTED object from the LEVEL.objects<vector> TO the EVENT-list
				int index = level_object_list.getSelectedIndex();
				if(index != -1) {
					
					event_object_listmodel.addElement(level_listmodel.get(index));
					
					if(current_object.editor_event != null)
						current_object.editor_event.event_objects.add(editor.getGame().getGame().getLevel().objects.elementAt(index));
				}
			}	
		});

	}
	
		//------------------------------------------------------------------------------
		// initEventGameObjectWindow() - For "in-game" objects (not in the editor)
		//------------------------------------------------------------------------------
		public void initEventGameObjectWindow(JFrame cWindow, final JTextArea xText, final JTextArea yText, final JTextArea speedText, final JTextArea waitText, JButton add, JButton remove, 
				final JList<String> waypoint_list, final DefaultListModel<String> listmodel, 
				final JList<String> event_object_list, final DefaultListModel<String> event_listmodel, 
				final JList<String> level_object_list, final DefaultListModel<String> level_listmodel, 
				JButton addEvent, JButton removeEvent) 
		{
			// Add references
			gameobject_event_window = cWindow;
			gameobject_event_waypoint_listmodel = listmodel;
			gameobject_event_object_listmodel = event_listmodel;
			gameobject_event_level_listmodel = level_listmodel;
			
			// Remove a waypoint
			remove.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					
					// Remove a waypoint from the vector and from the visual list
					if(gameobject_event_waypoint_listmodel.getSize() > 0) {
						int index = waypoint_list.getSelectedIndex();
						if(index != -1) {
							
							gameobject_event_waypoint_listmodel.remove(index);
							
							switch(editor.getGame().getGame().selected_editor_object.TYPE) 
							{
								case GLOBAL.OBJECT_MOVING_BLOCK: 
								{
									editor.getGame().getGame().selected_editor_object.getWaypoints().remove(index);
									break;
								}
							}
						}
					}
				}
			});
			
			// Add a waypoint
			add.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					
					String wp = "X: ";
					wp += xText.getText();
					wp += "\t Y: " + yText.getText();
					wp += "\t SPEED: " + speedText.getText();
					wp += "\t TIMER: " + waitText.getText();
					gameobject_event_waypoint_listmodel.addElement(wp);
					
					// add to current_selected_object
					switch(editor.getGame().getGame().selected_editor_object.TYPE) 
					{
						case GLOBAL.OBJECT_MOVING_BLOCK: 
						{
							editor.getGame().getGame().selected_editor_object.getWaypoints().add(new Waypoint(Integer.parseInt(xText.getText()), Integer.parseInt(yText.getText()), Integer.parseInt(speedText.getText()), Integer.parseInt(waitText.getText())));
							break;
						}
					}
				}
			});
			
			// Remove an event
			removeEvent.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					// Remove a event object
					if(gameobject_event_object_listmodel.getSize() > 0) {
						int index = event_object_list.getSelectedIndex();
						if(index != -1) {

							gameobject_event_object_listmodel.remove(index);
							editor.getGame().getGame().selected_editor_object.getEventObjects().remove(index);
						}
					}
				}
	
			});
			
			// Add a object to events
			addEvent.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					
					// SELECTED object from the LEVEL.objects<vector> TO the EVENT-list
					int index = level_object_list.getSelectedIndex();
					if(index != -1) {
						
						gameobject_event_object_listmodel.addElement(level_listmodel.get(index));
						editor.getGame().getGame().selected_editor_object.getEventObjects().add(editor.getGame().getGame().getLevel().objects.elementAt(index));
					}
					
				}	
			});

		}
	
	//------------------------------------------------------------------------------
	// initConsoleWindow()
	//------------------------------------------------------------------------------
	public void initConsoleWindow(JFrame cWindow, final JTextField console_input, final JTextArea console_output) {
		console_window = cWindow;
		
		// When ENTER is pressed
		console_input.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

            	String get_input = console_input.getText();
            	
            	if(get_input.length() > 0) {
            		console_output.append(get_input + '\n');
            		console_output.append(console.parseCommand(get_input));
            	}
            	else
            		console_output.append("\n");
            	console_input.setText("");
            	console_output.setPreferredSize(new Dimension(500, 270 + (console_output.getLineCount() * 15)));
            }
        });
	}
	
	//------------------------------------------------------------------------------
	// initLogWindow()
	//------------------------------------------------------------------------------
	public void initLogWindow(JFrame cWindow, final JTextPane logtextPane) {
		
		log_window = cWindow;
		log = new EditorLog(logtextPane);

		// Timer that every 0,5 seconds checks for messages
		msgUpdateTimer = new Timer(500, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				try {
					if(editor.getGame().getGame().messages.size() > 0) {
						for(int i = 0; i < editor.getGame().getGame().messages.size(); i++) {
							int msgtype = Character.getNumericValue(editor.getGame().getGame().messages.elementAt(i).charAt(0));
							String msg = editor.getGame().getGame().messages.elementAt(i).substring(1, editor.getGame().getGame().messages.elementAt(i).length());
							log.addMessage(msgtype, msg);
						}
						editor.getGame().getGame().messages.clear();
					}
				}
				catch(Exception ex) {
					return;
				}
			}
		});
		msgUpdateTimer.start();
		
	}
	
	//------------------------------------------------------------------------------
	// initCurrentSelectedObjectWindow()
	//------------------------------------------------------------------------------
	public void initCurrentSelectedObjectWindow(JFrame cWindow, final JButton delete_object, final JTextField id, 
			final JTextField type, final JTextField x, final JTextField y, final JTextField width, final JTextField height, 
			final JCheckBox collidable, final JTextField tx, final JTextField ty, 
			final JTextField sx, final JTextField sy, final JTextField scalex, final JTextField scaley, final JTextField alpha, final JCheckBox event_collidable, 
			final JButton addevent, final JTextField speed, final JTextField relativeleft, final JTextField relativetop, final JButton initButton) 
	{
		current_selected_window = cWindow;
		current_selected_window.setVisible(true);
		
		// The select
		select = new EditorSelect(editor, id, type, x, y, width, height, collidable, tx, ty, sx, sy, scalex, scaley, alpha, event_collidable, speed, relativeleft, relativetop);
		
		// Timer that every 0,5 seconds checks wether the selected object has changed
		selectUpdateTimer = new Timer(500, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!nowSelected.equals(editor.getGame().getGame().selected_editor_object.ID)) {
					select.Select();
					nowSelected = editor.getGame().getGame().selected_editor_object.ID;
					log.addMessage(GLOBAL.MSG_INFORMATION, "Selected object: " + nowSelected);
				}
			}
		});
		selectUpdateTimer.start();

		// DELETE
		delete_object.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	
            	for(int i = 0; i < editor.getGame().getGame().getLevel().objects.size(); i++) {
            		if(editor.getGame().getGame().selected_editor_object.equals(editor.getGame().getGame().getLevel().objects.elementAt(i))) {
            			log.addMessage(GLOBAL.MSG_INFORMATION, "Removed object: " + editor.getGame().getGame().selected_editor_object.ID);
            			editor.getGame().getGame().getLevel().objects.remove(i);
            			editor.getGame().getGame().selected_editor_object = editor.getGame().getGame().selected_editor_object_default;
            		}
            	}
            	
            	for(int i = 0; i < editor.getGame().getGame().getLevel().backObjects.size(); i++) {
            		if(editor.getGame().getGame().selected_editor_object.equals(editor.getGame().getGame().getLevel().backObjects.elementAt(i))) {
            			log.addMessage(GLOBAL.MSG_INFORMATION, "Removed object: " + editor.getGame().getGame().selected_editor_object.ID);
            			editor.getGame().getGame().getLevel().backObjects.remove(i);
            			editor.getGame().getGame().selected_editor_object = editor.getGame().getGame().selected_editor_object_default;
            		}
            	}
            }
        });
		
		// ID
		id.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
			}
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				editor.getGame().getGame().selected_editor_object.ID = (id.getText());
			}
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				editor.getGame().getGame().selected_editor_object.ID = (id.getText());
			}
		});
		
		// X
		x.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
			}
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				try {
					editor.getGame().getGame().selected_editor_object.coordinates.x = (Integer.parseInt(x.getText()));
					editor.getGame().getGame().selected_editor_object.hitbox.updateHitbox(editor.getGame().getGame().selected_editor_object.coordinates.x, editor.getGame().getGame().selected_editor_object.coordinates.y);
				}
				catch(NumberFormatException e) {
					return;
				}
			}
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				try {
					editor.getGame().getGame().selected_editor_object.coordinates.x = (Integer.parseInt(x.getText()));
					editor.getGame().getGame().selected_editor_object.hitbox.updateHitbox(editor.getGame().getGame().selected_editor_object.coordinates.x, editor.getGame().getGame().selected_editor_object.coordinates.y);
				}
				catch(NumberFormatException e) {
					return;
				}
			}
		});
		
		// Y
		y.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
			}
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				try {
					editor.getGame().getGame().selected_editor_object.coordinates.y = (Integer.parseInt(y.getText()));
					editor.getGame().getGame().selected_editor_object.hitbox.updateHitbox(editor.getGame().getGame().selected_editor_object.coordinates.x, editor.getGame().getGame().selected_editor_object.coordinates.y);
				}
				catch(NumberFormatException e) {
					return;
				}
			}
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				try {
					editor.getGame().getGame().selected_editor_object.coordinates.y = (Integer.parseInt(y.getText()));
					editor.getGame().getGame().selected_editor_object.hitbox.updateHitbox(editor.getGame().getGame().selected_editor_object.coordinates.x, editor.getGame().getGame().selected_editor_object.coordinates.y);
				}
				catch(NumberFormatException e) {
					return;
				}
			}
		});
		
		// Hitbox WIDTH
		width.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
			}
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				try {
					editor.getGame().getGame().selected_editor_object.hitbox.width = Integer.parseInt(width.getText());
					editor.getGame().getGame().selected_editor_object.hitbox.updateHitbox(editor.getGame().getGame().selected_editor_object.coordinates.x, editor.getGame().getGame().selected_editor_object.coordinates.y);
				}
				catch(NumberFormatException e) {
					return;
				}
			}
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				try {
					editor.getGame().getGame().selected_editor_object.hitbox.width = Integer.parseInt(width.getText());
					editor.getGame().getGame().selected_editor_object.hitbox.updateHitbox(editor.getGame().getGame().selected_editor_object.coordinates.x, editor.getGame().getGame().selected_editor_object.coordinates.y);
				}
				catch(NumberFormatException e) {
					return;
				}
			}
		});
		
		// Hitbox HEIGHT
		height.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
			}
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				try {
					editor.getGame().getGame().selected_editor_object.hitbox.height = Integer.parseInt(height.getText());
					editor.getGame().getGame().selected_editor_object.hitbox.updateHitbox(editor.getGame().getGame().selected_editor_object.coordinates.x, editor.getGame().getGame().selected_editor_object.coordinates.y);
				}
				catch(NumberFormatException e) {
					return;
				}
			}
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				try {
					editor.getGame().getGame().selected_editor_object.hitbox.height = Integer.parseInt(height.getText());
					editor.getGame().getGame().selected_editor_object.hitbox.updateHitbox(editor.getGame().getGame().selected_editor_object.coordinates.x, editor.getGame().getGame().selected_editor_object.coordinates.y);
				}
				catch(NumberFormatException e) {
					return;
				}
			}
		});
		
		// The checkbox for setting this object collidable
		collidable.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

            	if(collidable.isSelected()) {
            		collidable.setSelected(true);
            		editor.getGame().getGame().selected_editor_object.setCollidable(true);
            	}
            	else {
            		collidable.setSelected(false);
            		editor.getGame().getGame().selected_editor_object.setCollidable(false);
            	}
            }
        });
		
		// Sprite's Texture X
		tx.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
			}
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				try {
					editor.getGame().getGame().selected_editor_object.sprites.elementAt(0).texture_x = (Integer.parseInt(tx.getText()));
				}
				catch(NumberFormatException e) {
					return;
				}
			}
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				try {
					editor.getGame().getGame().selected_editor_object.sprites.elementAt(0).texture_x = (Integer.parseInt(tx.getText()));
				}
				catch(NumberFormatException e) {
					return;
				}
			}
		});
		
		// Sprite's Texture Y
		ty.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
			}
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				try {
					editor.getGame().getGame().selected_editor_object.sprites.elementAt(0).texture_y = (Integer.parseInt(ty.getText()));
				}
				catch(NumberFormatException e) {
					return;
				}
			}
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				try {
					editor.getGame().getGame().selected_editor_object.sprites.elementAt(0).texture_y = (Integer.parseInt(ty.getText()));
				}
				catch(NumberFormatException e) {
					return;
				}
			}
		});

		// Sprite's texture size X
		sx.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
			}
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				try {
					editor.getGame().getGame().selected_editor_object.sprites.elementAt(0).size_x = (Integer.parseInt(sx.getText()));
				}
				catch(NumberFormatException e) {
					return;
				}
			}
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				try {
					editor.getGame().getGame().selected_editor_object.sprites.elementAt(0).size_x = (Integer.parseInt(sx.getText()));
				}
				catch(NumberFormatException e) {
					return;
				}
			}
		});

		// Sprite's texture size Y
		sy.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
			}
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				try {
					editor.getGame().getGame().selected_editor_object.sprites.elementAt(0).size_y = (Integer.parseInt(sy.getText()));
				}
				catch(NumberFormatException e) {
					return;
				}
			}
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				try {
					editor.getGame().getGame().selected_editor_object.sprites.elementAt(0).size_y = (Integer.parseInt(sy.getText()));
				}
				catch(NumberFormatException e) {
					return;
				}
			}
		});
		
		// SPRITE RELATIVE LEFT
		relativeleft.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
			}
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				try {
					editor.getGame().getGame().selected_editor_object.sprites.elementAt(0).relative_x = (Integer.parseInt(relativeleft.getText()));
				}
				catch(NumberFormatException e) {
					return;
				}
			}
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				try {
					editor.getGame().getGame().selected_editor_object.sprites.elementAt(0).relative_x = (Integer.parseInt(relativeleft.getText()));
				}
				catch(NumberFormatException e) {
					return;
				}
			}
		});
		
		// SPRITE RELATIVE TOP
		relativetop.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
			}
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				try {
					editor.getGame().getGame().selected_editor_object.sprites.elementAt(0).relative_y = (Integer.parseInt(relativetop.getText()));
				}
				catch(NumberFormatException e) {
					return;
				}
			}
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				try {
					editor.getGame().getGame().selected_editor_object.sprites.elementAt(0).relative_y = (Integer.parseInt(relativetop.getText()));
				}
				catch(NumberFormatException e) {
					return;
				}
			}
		});
		
		// SPRITE scale_X
		scalex.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
			}
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				try {
					editor.getGame().getGame().selected_editor_object.sprites.elementAt(0).scale_x = (Float.parseFloat(scalex.getText()));
				}
				catch(NumberFormatException e) {
					return;
				}
			}
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				try {
					editor.getGame().getGame().selected_editor_object.sprites.elementAt(0).scale_x = (Float.parseFloat(scalex.getText()));
				}
				catch(NumberFormatException e) {
					return;
				}
			}
		});
		
		// SPRITE scale_y
		scaley.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
			}
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				try {
					editor.getGame().getGame().selected_editor_object.sprites.elementAt(0).scale_y = (Float.parseFloat(scaley.getText()));
				}
				catch(NumberFormatException e) {
					return;
				}
			}
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				try {
					editor.getGame().getGame().selected_editor_object.sprites.elementAt(0).scale_y = (Float.parseFloat(scaley.getText()));
				}
				catch(NumberFormatException e) {
					return;
				}
			}
		});
		
		// Sprite's alpha
		alpha.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
			}
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				try {
					editor.getGame().getGame().selected_editor_object.sprites.elementAt(0).alpha = (Float.parseFloat(alpha.getText()));
				}
				catch(NumberFormatException e) {
					return;
				}
			}
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				try {
					editor.getGame().getGame().selected_editor_object.sprites.elementAt(0).alpha = (Float.parseFloat(alpha.getText()));
				}
				catch(NumberFormatException e) {
					return;
				}
			}
		});
		
		// The checkbox for setting this object event-collidable
		event_collidable.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

            	if(event_collidable.isSelected()) {
            		event_collidable.setSelected(true);
            		editor.getGame().getGame().selected_editor_object.setCollidableEvent(true);
            	}
            	else {
            		event_collidable.setSelected(false);
            		editor.getGame().getGame().selected_editor_object.setCollidableEvent(false);
            	}
            }
        });
		
		// AddEvent Button
		addevent.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	showObjectEventWindow();
       
            }
        });
		
		// Speed
		speed.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
			}
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				try {
					editor.getGame().getGame().selected_editor_object.speed = ((Integer.parseInt(speed.getText())));
				}
				catch(NumberFormatException e) {
					return;
				}
			}
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				try {
					editor.getGame().getGame().selected_editor_object.speed = ((Integer.parseInt(speed.getText())));
				}
				catch(NumberFormatException e) {
					return;
				}
			}
		});
		
		// Re-Run the init(); function for the selected object
		initButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            		editor.getGame().getGame().selected_editor_object.init();
            		log.addMessage(GLOBAL.MSG_INFORMATION, "init(); function re-run for object (" + editor.getGame().getGame().selected_editor_object.ID + ")");
            }
        });

	}

	//------------------------------------------------------------------------------
	// saveLevel()
	//------------------------------------------------------------------------------
	public void saveLevel() {
		
		// Create a ObjectOutputStream, and then write the level object to file
		ObjectOutputStream out = null;
		try {
			resetLevel();
			
			// Filename is current datetime-stamp
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date now = new Date();
		    String strDate = sdf.format(now) + ".lvl";
		    strDate = strDate.replaceAll("-", "");
		    strDate = strDate.replaceAll(":", "-");
		    strDate = strDate.replaceAll(" ", "");
		    
			out = new ObjectOutputStream(new FileOutputStream(strDate));
			out.writeObject(editor.getGame().getGame().getLevel());
			out.close();
			log.addMessage(GLOBAL.MSG_INFORMATION, "Level serialized to: " + strDate);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			log.addMessage(GLOBAL.MSG_ERROR, "FileNotFoundException in Editor.saveLevel()");
		} catch (IOException e) {
			log.addMessage(GLOBAL.MSG_ERROR, "IOException in Editor.saveLevel()");
			e.printStackTrace();
		}
		
	}
	
	//------------------------------------------------------------------------------
	// loadLevel()
	//------------------------------------------------------------------------------	
	public void loadLevel() {
		
		final JFileChooser fc = new JFileChooser();
		int returnVal = fc.showOpenDialog(null);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			try {
				InputStream is = new FileInputStream(fc.getSelectedFile());
				ObjectInputStream in = new ObjectInputStream(is);
				editor.getGame().getGame().level = (Level) in.readObject();
				editor.getGame().getGame().draw.level = editor.getGame().getGame().level;
				editor.getGame().getGame().collision.world = editor.getGame().getGame().level.world;
				in.close();
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	//------------------------------------------------------------------------------
	// resetLevel()
	//------------------------------------------------------------------------------	
	public void resetLevel() {
		editor.getGame().getGame().getLevel().reset();
		for(int i = 0; i < editor.getGame().getGame().getLevel().objects.size(); i++) {
			editor.getGame().getGame().getLevel().objects.elementAt(i).reset();
			log.addMessage(GLOBAL.MSG_INFORMATION, "Object (" + editor.getGame().getGame().getLevel().objects.elementAt(i).ID + ") has been reset.");
		}
		log.addMessage(GLOBAL.MSG_INFORMATION, "Level has been reset.");
	}
	
	//------------------------------------------------------------------------------
	// snapToGrid()
	//------------------------------------------------------------------------------
	public void snapToGrid(final JCheckBox snap) {

		snap.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

            	if(snap.isSelected()) {
            		snap.setSelected(true);
            		editor.getGame().getGame().setEditorSnap(true);
            	}
            	else {
            		snap.setSelected(false);
            		editor.getGame().getGame().setEditorSnap(false);
            	}
            }
        });
	}
	
	//------------------------------------------------------------------------------
	// gridSwitch() - Switches the Grid ON/OFF
	//------------------------------------------------------------------------------
	public String gridSwitch() {
		
		if(editor.getGame().getGame().getDraw().isGridActive()) {
			editor.getGame().getGame().getDraw().showGrid(false);
			return "TURN GRID ON";
		}
		editor.getGame().getGame().getDraw().showGrid(true);
		return "TURN GRID OFF";
	}

	//------------------------------------------------------------------------------
	// hitboxSwitch() - Switches the hitboxes ON/OFF
	//------------------------------------------------------------------------------
	public String hitboxSwitch() {
		
		if(!editor.getGame().getGame().getDraw().isHitboxesActive()) {
			editor.getGame().getGame().getDraw().showHitboxes(true);
			return "TURN HITBOXES OFF";
		}
		editor.getGame().getGame().getDraw().showHitboxes(false);
		return "TURN HITBOXES: ON";
	}
	
	//------------------------------------------------------------------------------
	// loadObjectIDs()
	//------------------------------------------------------------------------------
	public List<String> loadObjectIDs() {
		
		List<String> idStrings = new ArrayList<String>();
 		Scanner sc = null;
		try {
			sc = new Scanner(new File(GLOBAL.objectlist_file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			log.addMessage(GLOBAL.MSG_ERROR, "FileNotFoundException in Editor.loadObjectIDs()");
		}
		
		while(sc.hasNextLine()) {
			String temp = sc.nextLine();
			idStrings.add(temp);
		}
		sc.close();
		return idStrings;
	}
	
	//------------------------------------------------------------------------------
	// initCurrentObject() - Set references to the "current object" panel and update name with Listener
	//------------------------------------------------------------------------------	
	public void initCurrentObject(JTextField name, JButton texturebutton, JCheckBox collidable, final JComboBox<String> object_type, final JComboBox<String> funcList) {
		
		currentName = name;
		currentTexture = texturebutton;
		currentCollidable = collidable;
		currentTYPE = object_type;
		
		// When typing in the textbox "name" change the current_object value also
		currentName.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
			}
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				current_object.setText(currentName.getText());
				current_object.gameobject.ID = (currentName.getText());
			}
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				current_object.setText(currentName.getText());
				current_object.gameobject.ID = (currentName.getText());
			}
		});
		
		// Change the current TYPE
		currentTYPE.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				updateTYPE();
			}	
		});
		
		// The checkbox for setting this object collidable
		currentCollidable.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

            	if(currentCollidable.isSelected()) {
            		currentCollidable.setSelected(true);
            		current_object.gameobject.setCollidable(true);
            	}
            	else {
            		currentCollidable.setSelected(false);
            		current_object.gameobject.setCollidable(false);
            	}
            }
        });
		
		// Add functionality to the current_object, when clicking on ...
		funcList.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				if(funcList.getSelectedIndex() == 0) {
					showEventWindow();
				}
				else if(funcList.getSelectedIndex() == 1)
					;
				else if(funcList.getSelectedIndex() == 2)
					;
				else if(funcList.getSelectedIndex() == 3)
					;

			}	
		});
		
	}
	
	//------------------------------------------------------------------------------
	// updateTYPE() - Update the TYPE for the current object, to identify what this is exacly
	//------------------------------------------------------------------------------	
	private void updateTYPE() {
		
		int newTYPE = 0;
		try {
			// H�mta ID't fr�n 0 till och med mellanslaget.
			newTYPE = Integer.parseInt(currentTYPE.getSelectedItem().toString().substring(0, currentTYPE.getSelectedItem().toString().indexOf(' ')));
		}
		catch(NumberFormatException e) {
			return;
		}
		current_object.gameobject.TYPE = (newTYPE);
	}
	
	//------------------------------------------------------------------------------
	// initObjectPanel() - Setup the object panel with one simple empty object
	//------------------------------------------------------------------------------	
	public void initObjectPanel(JButton createNewObject, JButton deleteObject, JButton clearObjects, final JPanel objectList) {
		
		// On start, create a new "empty object" to be in the list by default, and set this to be the current_object
		current_object = newObject(getEmptyImage(), OBJECT_BUTTON_WIDTH, OBJECT_BUTTON_HEIGHT);
		current_object.editor_event = new EditorEvent();
		objectList.add(current_object);
		setCurrentObject(current_object);
		editor_objects.add(current_object);
		
		// When click on the "new object" button occurs, create a new empty object.
		createNewObject.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
        		EditorObject o = newObject(getEmptyImage(), OBJECT_BUTTON_WIDTH, OBJECT_BUTTON_HEIGHT);
        		objectList.add(o);
        		editor_objects.add(o);
        		
        		// Change the size of the objectList panel so the scrolling works correctly
        		objectList.setPreferredSize(new Dimension(300, OBJECT_BUTTON_HEIGHT * editor_objects.size()));
        		objectList.revalidate();
            }
        });
	}
	
	//------------------------------------------------------------------------------
	// getEmptyImage() - Returns "empty.png"
	//------------------------------------------------------------------------------
	private BufferedImage getEmptyImage() {
		
		// Load in an empty editor-texture
		BufferedImage img = null;
		try {
			img = ImageIO.read(new File("editordata/empty.png"));
		} catch (IOException e) {
			e.printStackTrace();
			log.addMessage(GLOBAL.MSG_ERROR, "IOException in Editor.getEmptyImage");
			return null;
		}
		return img;
	}
	
	//------------------------------------------------------------------------------
	// getNewEmptyTile() - Returns a new empty tile
	//------------------------------------------------------------------------------
	private Tile getNewEmptyTile() {
		
		Sprite s = new Sprite(0, 0, 1, GLOBAL.NORMAL_LAYER);
		Hitbox h = new Hitbox(0, 0, 0, 0);
		Coordinate c = new Coordinate(0, 0);
		Tile t = new Tile(c, h, s, false);
		t.ID = (Integer.toString(objectCounter));
		objectCounter++;
		return t;
	}
	
	//------------------------------------------------------------------------------
	// setupLayerPanel()
	//------------------------------------------------------------------------------	
	public void setupLayerPanel(final JButton pButton, final JButton nButton, final JButton oButton, final JButton bButton, final JButton fButton, final JButton frameButton, final JTextField frameText) {
		
		// Save buttons default color
		default_color = pButton.getBackground();
		
		// We always start in play-mode
		pButton.setBackground(Color.green);
		
		// PLAY_MODE - Set action listener behaviour
		pButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	
            	// Set color green
            	pButton.setBackground(Color.green);
            	
            	// Set state
            	editor.getGame().setEditorState(GLOBAL.STATE_PLAY);

            	// Set all other buttons to default color
            	nButton.setBackground(default_color);
            	oButton.setBackground(default_color);
            	bButton.setBackground(default_color);
            	fButton.setBackground(default_color);
            }
        });
		
		// RUN FRAME(S) - To run "x" frames of game logic then stop.
		frameButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	
        		int frames = 0;
        		try {
        			frames = Integer.parseInt(frameText.getText());
        		}
        		catch(NumberFormatException ex) {
        			return;
        		}
            	editor.getGame().setFramesToRun(frames);
            }
        });
		
		// NORMAL_MODE - Set action listener behaviour
		nButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	
            	// Set color orange
            	nButton.setBackground(new Color(255, 160, 0));
            	
            	// Set state
            	editor.getGame().setEditorState(GLOBAL.STATE_EDITOR_NORMAL);

            	// Set all other buttons to default color
            	pButton.setBackground(default_color);
            	oButton.setBackground(default_color);
            	bButton.setBackground(default_color);
            	fButton.setBackground(default_color);
            }
        });
		
		// OBJECT_MODE - Set action listener behaviour
		oButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	
            	// Set color orange
            	oButton.setBackground(new Color(255, 160, 0));
            	
            	// Set state
            	editor.getGame().setEditorState(GLOBAL.STATE_EDITOR_OBJECT);

            	// Set all other buttons to default color
            	pButton.setBackground(default_color);
            	nButton.setBackground(default_color);
            	bButton.setBackground(default_color);
            	fButton.setBackground(default_color);
            }
        });
		
		// BACKGROUND_MODE - Set action listener behaviour
		bButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	
            	// Set color orange
            	bButton.setBackground(new Color(255, 160, 0));
            	
            	// Set state
            	editor.getGame().setEditorState(GLOBAL.STATE_EDITOR_BACKGROUND);

            	// Set all other buttons to default color
            	pButton.setBackground(default_color);
            	nButton.setBackground(default_color);
            	oButton.setBackground(default_color);
            	fButton.setBackground(default_color);
            }
        });
		
		// FOREGROUND_MODE - Set action listener behaviour
		fButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	
            	// Set color orange
            	fButton.setBackground(new Color(255, 160, 0));
            	
            	// Set state
            	editor.getGame().setEditorState(GLOBAL.STATE_EDITOR_FOREGROUND);

            	// Set all other buttons to default color
            	pButton.setBackground(default_color);
            	nButton.setBackground(default_color);
            	oButton.setBackground(default_color);
            	bButton.setBackground(default_color);          	
            }
        });
		
	}

	//------------------------------------------------------------------------------
	// newObject() - Creates a new editorobject
	//------------------------------------------------------------------------------
	private EditorObject newObject(BufferedImage img, final int width, final int height) {
		
		final EditorObject eo = new EditorObject(new ImageIcon(img), getNewEmptyTile(), width, height);
		eo.editor_event = new EditorEvent();
		eo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	
            	// When clicked on: Change color on the currently selected object and set it to be the current one
            	if(current_object != null) {
            		current_object.setBackground(eo.getBackground());
	            	eo.setBackground(new Color(255, 160, 0));
	            	setCurrentObject(eo);
            	}
            }
        });
		
		return eo;
	}
	
	//------------------------------------------------------------------------------
	// newTexture() - Creates a new editortexture
	//------------------------------------------------------------------------------
	private EditorTexture newTexture() {
		
		final EditorTexture eo = new EditorTexture();
		eo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	
            	// Set the texture and icon for the editor
            	if(current_object == null) {
            		JOptionPane.showMessageDialog(null, "current_object == NULL", "Warning", JOptionPane.WARNING_MESSAGE);
            		return;
            	}
            	
        		current_object.texture = eo;
        		current_object.setIcon(new ImageIcon(eo.texture));
        		
        		// Set the Sprite values of the actual gameobject
        		current_object.gameobject.sprites.elementAt(0).texture_x = (eo.texture_x);
        		current_object.gameobject.sprites.elementAt(0).texture_y = (eo.texture_y);
        		
        		// Change the current texture on the "current object panel" to this one
        		if(currentTexture != null)
        			currentTexture.setIcon(current_object.getIcon());
            }
        });
		
		return eo;
	}
	
	//------------------------------------------------------------------------------
	// setCurrentObject() - Change the current object
	//------------------------------------------------------------------------------	
	private void setCurrentObject(EditorObject o) {
		
		current_object = o;
		currentName.setText(o.getText());
		
		currentTYPE.setSelectedIndex(current_object.gameobject.TYPE);
		
		if(currentTexture != null)
			currentTexture.setIcon(o.getIcon());
		
		if(o.gameobject != null) {

			// Set this to the world
			editor.getGame().setEditorObject(o.gameobject, o.editor_event.event_objects, o.editor_event.event_waypoints);
			
			if(o.gameobject.isCollidable())
				currentCollidable.setSelected(true);
			else
				currentCollidable.setSelected(false);
		}
	}
	


	// The editor itself
	private WorldEditor editor;
	
	// Current Object panel / info
	private JPanel side;
	private EditorObject current_object;
	private JTextField currentName;
	private JButton currentTexture;
	private JCheckBox currentCollidable;
	private JComboBox<String> currentTYPE;
	
	// Vector with all objects
	private Vector<EditorObject> editor_objects;
	
	// Texture panel (list of textures)
	private JPanel texturelist;
	
	// Layers
	private Color default_color;
	
	// Button sizes
	private static final int OBJECT_BUTTON_HEIGHT = GLOBAL.TILE_SIZE + 4;
	private static final int OBJECT_BUTTON_WIDTH = GLOBAL.TILE_SIZE + 350;
	
	// CAMERA & PLAYER WINDOWS
	private JFrame player_window;
	private DefaultListModel<String> camera_listmodel;
	private JFrame camera_window;
	
	// EVENT WINDOW
	private JFrame event_window;
	private DefaultListModel<String> event_waypoint_listmodel;
	private DefaultListModel<String> event_object_listmodel;
	private DefaultListModel<String> event_level_listmodel;
	
	// GAMEOBJECT EVENT WINDOW 
	private JFrame gameobject_event_window;
	private DefaultListModel<String> gameobject_event_waypoint_listmodel;
	private DefaultListModel<String> gameobject_event_object_listmodel;
	private DefaultListModel<String> gameobject_event_level_listmodel;
	
	// CONSOLE WINDOW
	private JFrame console_window;
	private Console console;
	
	// SELECTED OBJECT WINDOW
	private static JFrame current_selected_window;
	private static EditorSelect select;
	private int objectCounter;
	private Timer selectUpdateTimer;
	private String nowSelected;
	
	// LOG WINDOW
	private static JFrame log_window;;
	private EditorLog log;
	private Timer msgUpdateTimer;
}
