package com.lazypointer.game.desktop;

import java.awt.Dimension;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import com.lazypointer.game.GameObject;

@SuppressWarnings("serial")
public class EditorObject extends JButton {

	EditorObject(ImageIcon texture, GameObject o, int width, int height) {
		
		gameobject = o;
		editor_event = null;
		
		Dimension size = new Dimension(width, height);
		setPreferredSize(size);
		setMaximumSize(size);
		setMinimumSize(size);
		
		setBorder(null);

		setFont(new Font("Arial", Font.BOLD, 14));
		setIcon(texture);
		setText(o.ID);
		setHorizontalAlignment(SwingConstants.LEFT);
	}

	GameObject gameobject;
	EditorTexture texture;
	EditorEvent editor_event;

}
