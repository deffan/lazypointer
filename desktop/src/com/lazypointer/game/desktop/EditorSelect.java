package com.lazypointer.game.desktop;

import javax.swing.JCheckBox;
import javax.swing.JTextField;

public class EditorSelect {

	public EditorSelect(WorldEditor eeditor, JTextField eid, JTextField etype, JTextField ex, JTextField ey, 
			JTextField ewidth, JTextField eheight, JCheckBox ecollidable, JTextField etx, 
			JTextField ety, JTextField esx, JTextField esy, JTextField escalex , JTextField escaley, JTextField ealpha, 
			JCheckBox eevent_collidable, JTextField espeed, JTextField erelativeleft, JTextField erelativetop) 
	{
		editor = eeditor;
		id = eid;
		type = etype;
		x = ex;
		y = ey;
		width = ewidth;
		height = eheight;
		collidable = ecollidable;
		tx = etx;
		ty = ety;
		sx = esx;
		sy = esy;
		alpha = ealpha;
		event_collidable = eevent_collidable;
		speed = espeed;
		relativeleft = erelativeleft;
		relativetop = erelativetop;
		scalex = escalex;
		scaley = escaley;
	}
	
	public void Select() {
		try {
			id.setText(editor.getGame().getGame().selected_editor_object.ID);
			type.setText(Integer.toString(editor.getGame().getGame().selected_editor_object.TYPE));
			x.setText(Integer.toString((int) editor.getGame().getGame().selected_editor_object.coordinates.x));
			y.setText(Integer.toString((int) editor.getGame().getGame().selected_editor_object.coordinates.y));
			width.setText(Integer.toString(editor.getGame().getGame().selected_editor_object.hitbox.width));
			height.setText(Integer.toString(editor.getGame().getGame().selected_editor_object.hitbox.height));
			relativeleft.setText(Integer.toString(editor.getGame().getGame().selected_editor_object.sprites.elementAt(0).relative_x));
			relativetop.setText(Integer.toString(editor.getGame().getGame().selected_editor_object.sprites.elementAt(0).relative_y));
			collidable.setSelected(editor.getGame().getGame().selected_editor_object.isCollidable());
			tx.setText(Integer.toString(editor.getGame().getGame().selected_editor_object.sprites.elementAt(0).texture_x));
			ty.setText(Integer.toString(editor.getGame().getGame().selected_editor_object.sprites.elementAt(0).texture_y));
			sx.setText(Integer.toString(editor.getGame().getGame().selected_editor_object.sprites.elementAt(0).size_x));
			sy.setText(Integer.toString(editor.getGame().getGame().selected_editor_object.sprites.elementAt(0).size_y));
			scalex.setText(Float.toString(editor.getGame().getGame().selected_editor_object.sprites.elementAt(0).scale_x));
			scaley.setText(Float.toString(editor.getGame().getGame().selected_editor_object.sprites.elementAt(0).scale_y));
			alpha.setText(Float.toString(editor.getGame().getGame().selected_editor_object.sprites.elementAt(0).alpha));
			event_collidable.setSelected(editor.getGame().getGame().selected_editor_object.isCollidableEvent());
			speed.setText(Integer.toString(editor.getGame().getGame().selected_editor_object.speed));
		}
		catch(Exception e) {
			return;
		}
	}
	
	WorldEditor editor;
	final JTextField id;
	final JTextField type; 
	final JTextField x; 
	final JTextField y; 
	final JTextField width; 
	final JTextField height; 
	final JCheckBox collidable; 
	final JTextField tx; 
	final JTextField ty; 
	final JTextField sx; 
	final JTextField sy; 
	final JTextField scalex; 
	final JTextField scaley;
	final JTextField alpha;
	final JCheckBox event_collidable; 
	final JTextField speed;
	final JTextField relativeleft;
	final JTextField relativetop;
}
