package com.lazypointer.game.desktop;

import java.awt.Dimension;
import java.awt.image.BufferedImage;

import javax.swing.JButton;

import com.lazypointer.game.GLOBAL;

public class EditorTexture extends JButton {

	EditorTexture() {
		Dimension size = new Dimension(GLOBAL.TILE_SIZE, GLOBAL.TILE_SIZE);
		setPreferredSize(size);
		setMaximumSize(size);
		setMinimumSize(size);
	}
	
	int texture_x;
	int texture_y;
	BufferedImage texture;
}
