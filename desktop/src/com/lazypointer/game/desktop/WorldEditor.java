package com.lazypointer.game.desktop;
import com.lazypointer.game.GLOBAL;
import com.lazypointer.game.Lazypointergame;

class WorldEditor {
	
	//--------------------------------------------
	// Constructor
	//--------------------------------------------
	WorldEditor(Boolean editor) {

		// Create the game object or "engine"
		game = new Lazypointergame(editor, null, GLOBAL.DESKTOP_PLATFORM);

		// Most logic/code here
		logic = new EditorLogic(this);
		
		// Create all windows, buttons, panels, etc
		components = new EditorComponent(this);
		
	}
	
	// GET DATAMEMBERS
	public EditorLogic getEditorLogic() { return logic; }
	public EditorComponent getEditorComponent() { return components; }
	public Lazypointergame getGame() { return game; }
	
	// DATAMEMBERS
	private Lazypointergame game;
	private EditorComponent components;
	private EditorLogic logic;
}

